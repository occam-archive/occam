#################################
Occam Administrator Documentation
#################################
.. toctree::
   :maxdepth: 2

   Installing Occam <docs/administrator/installing.md>
   Configuring Occam <docs/administrator/configuring.md>
   Creating an Occam Slurm Cluster <docs/administrator/slurm-cluster.md>
   Firewall Rules <docs/administrator/firewall.md>
   Deploying to AWS <docs/administrator/AWS.md>
   Upgrading Occam <docs/administrator/upgrading.md>
   Creating / Joining a federation <docs/administrator/federating.md>
   Importing Existing Objects <docs/administrator/importing-objects.md>
   Establishing System Policies <docs/administrator/policies.md>

**********
References
**********
.. toctree::
   :maxdepth: 2

   IPFS <https://ipfs.io/>
   Postgres <https://www.postgresql.org/>
   SQLite <https://www.sqlite.org/index.html>
   Slurm <https://slurm.schedmd.com/>

