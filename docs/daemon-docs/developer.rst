#############################
Occam Developer Documentation
#############################
.. toctree::
   :maxdepth: 2

   Contributing <docs/contributing.md>
   Code of Conduct <docs/code-of-conduct.md>
   Occam's decorators <src/occam/decorators.md>
   Unit tests <tests/README.md>
   How to write migration scripts <scripts/migrations/README.md>
   Plugins <plugins/README.md>
   Performance testing <docs/performance.md>

***********************
Component Documentation
***********************
.. toctree::
   :maxdepth: 3

   Creating components <src/occam/README.md>


==========
Components
==========
.. toctree::
   :maxdepth: 2

   accounts <src/occam/accounts/README.md>
   analysis <src/occam/analysis/README.md>
   associations <src/occam/associations/README.md>
   backends <src/occam/backends/README.md>
   builds <src/occam/builds/README.md>
   caches <src/occam/caches/README.md>
   citations <src/occam/citations/README.md>
   commands <src/occam/commands/README.md>
   configurations <src/occam/configurations/README.md>
   daemon <src/occam/daemon/README.md>
   databases <src/occam/databases/README.md>
   discover <src/occam/discover/README.md>
   git <src/occam/git/README.md>
   ingest <src/occam/ingest/README.md>
   jobs <src/occam/jobs/README.md>
   keys <src/occam/keys/README.md>
   licenses <src/occam/licenses/README.md>
   links <src/occam/links/README.md>
   manifests <src/occam/manifests/README.md>
   messages <src/occam/messages/README.md>
   network <src/occam/network/README.md>
   nodes <src/occam/nodes/README.md>
   notes <src/occam/notes/README.md>
   objects <src/occam/objects/README.md>
   people <src/occam/people/README.md>
   permissions <src/occam/permissions/README.md>
   resources <src/occam/resources/README.md>
   services <src/occam/services/README.md>
   storage <src/occam/storage/README.md>
   system <src/occam/system/README.md>
   vendor <src/occam/vendor/README.md>
   versions <src/occam/versions/README.md>
   workflows <src/occam/workflows/README.md>

**********
References
**********
.. toctree::
   :maxdepth: 2

   Git handbook <https://git-scm.com/book/en/v2>
   Occam papers <https://occam.cs.pitt.edu/publications>
