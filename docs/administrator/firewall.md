# Firewall Requirements

Depending on your [configuration](configuring.md) you may need to modify your network firewall rules to properly deploy Occam. Firewall considerations are listed below by feature:

  * Occam daemon
    * By default the Occam daemon runs on port 32000. The front-end, as well as any Slurm nodes running jobs will need to reach this port.
  * Postgres
    * Typically Postgres runs on TCP port 5432. Occam will need to be able to reach Postgres on this port.
  * Slurm
    * By default, Slurm installations require opening TCP ports 6817-6819, 7321, 60001-63000 from machines launching Slurm jobs (which the Occam daemon does). This allows connections to the Slurm database, and allows the random port opening srun and sbatch do.
