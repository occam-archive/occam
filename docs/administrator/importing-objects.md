# Importing Existing Objects

By default when you install an Occam instance there will be no objects in the store. You will want your instance to have some pre-existing objects to make it quick and easy for users to get started using Occam.

See the user manual's [guide to pulling objects](../user/importing-objects.md) for how to pull from existing resources.

## Recommended Resources:

  * [The Ace Editor](https://occam.cs.pitt.edu/QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g)
    * This editor allows web users to edit files directly in Occam.

  * [Plotly.js](https://occam.cs.pitt.edu/QmbUbGBxwEp7UjgCgKmpFLYSotxYpVsjeK8GR8nzVM28bu)
    * Plotly is a widely used graphing library.

  * [pdf.js](https://occam.cs.pitt.edu/QmNooP5Lxn3jGbFvUtMzphJeTBUykVix9SDSSkT8rzeNt1)
    * This viewer allows web users to view PDFs without leaving the object page.

  * [Web Image Viewer](https://occam.cs.pitt.edu/QmeGqNVNWoR9XPSa33aZMXUrsADWYnM6ZTxKgjZxetgehk)
    * This viewer allows web users to view image files without leaving the object page.
