# Contribution Guide

The following are accepted and cherished forms of contribution:

* Filing a bug issue. (We like to see feature requests and just normal bugs)
* Fixing a bug. (It's obviously helpful!)
* Adding documentation. (Help us with our docs or send us a link to your blog post!)
* Adding features.
* Adding artwork. (Art is the true visual form of professionalism)

The following are a bit harder to really accept, in spite of the obvious effort that may go into them, so please avoid this:

* Changing all of the javascript to coffeescript because it is "better"
* Rewriting all of the sass to whatever is newer. (It's happened to me before)
* Porting everything to rails.
* Creating a pull request with a "better" software license.

The general process for contributing is as follows:

1. Submit an issue to the [issue tracker](https://gitlab.com/occam-archive/occam/-/issues) describing the feature / bug details. If you wish to contribute directly, write a design document in the issue body describing your proposed fix for the issue.
1. Occam developers will look at the issue as well as proposed fixes and determine if the solution would work with how the system is architected. You should then be free to craft your solution!
1. If you have not done so already, fork and clone the project.
1. Update the code on your end following your design document.
1. Push that code to a public service like GitHub or GitLab.
1. Create a merge request from your repository to ours and link the issue in the merge request.

The above is the most convenient process. You may create an issue with a link to a repository or tar/zip containing your code or patches as well, if git is not your thing.

Below you will find a wealth of documentation to help you get started in
contributing to this project.

## Coding guidelines

### Style
Our coding style is based very loosely on [Google's Python style guidelines](https://google.github.io/styleguide/pyguide.html)

* Use camelCase for variable and function names.

* Try to keep columns at or below 80 characters.

* Wrap lines as follows when needed(aligning variables): 
```
  functionCall(veryLongParameterName,
               aSignificantlyLongerParameterName)
```

* If nested function calls don't fit well on one line, be verbose and split the
	calls up.

* Function parameters should have no space between the `=` operator:
```
  def functionCall(parameter="defaultValue"):
```

* Function calls should have a space between the `=` operator:
```
  functionCall(parameter = "nonDefaultValue")
```

* Code comments should be fully punctuated English sentences.

### Quality

* Try to keep functions small (<= 50 lines) to make unit testing easier.

* Remove commented code unless there is a temporary problem with it that needs to be fixed.

* Code should not deviate from any design documents describing the expected functionality. Design documents should be kept up to date with code changes.

* Code comments should describe why something is being done. Comments should not simply restate what the code is doing. It is always encouraged to link to a component readme when the code context isn't sufficient for understanding why something is being done. For example:

```
    # (Bad - doesn't add any useful information.):
    # Change the run section.
    if command:
      buildingObject['run'] = buildingObject.get('run', {})
      buildingObject['run']['command'] = shlex.split(command)

    # (Good - succinctly describes the rationale.):
    # Update the task to force the given command.
    if command:
      buildingObject['run'] = buildingObject.get('run', {})
      buildingObject['run']['command'] = shlex.split(command)

    # (Better - describes the rationale and where to find more information.):
    # The command parameter overrides the existing build command in the task.
    # See the manifest manager README for more details on how the run section
    # is used for builds.
    if command:
      buildingObject['run'] = buildingObject.get('run', {})
      buildingObject['run']['command'] = shlex.split(command)
```

## Linting

Linting is a fantastic way to quickly get feedback on a patch prior to code review. Most linters will warn you if you have forgotten to add docstrings to a function, and catch syntax errors before the code goes through the continuous integration pipeline.

We recommend running [pylint](https://www.pylint.org/) prior to submitting changes to Occam. This will help you find both style violations as well as logical errors (unused includes, shadowed global variables etc.).

`pylint` is installed after running `install.sh`, so to check a file you have modified simply run `pylint path/to/modified/file`.

## Documentation

Documentation of this project is divided by target audience into three levels: user, administrator, and developer. User level documentation should tell people who want to archive software and maintain Occam objects how to use all of the features of Occam to accomplish their goals. Administrator level documentation should serve as a guide for people who are trying to run their own Occam instance and should describe the processes of installing, configuring, maintaining, and upgrading an Occam instance. Developer level documentation should be targeted towards both Occam developers as well as anyone who wants a deep understanding of how Occam itself works. Developer documentation describes design decisions and low level behavior in detail in order to help developers plan new features and understand standing design decisions.

We follow a single source of truth (SSOT) model where the documentation describes the design, expected behavior, and procedures and the code is what must keep up with the documentation. User documentation is the most critical because it sets the expectation of Occam for people who are not deeply familiar with the software. Occam's documentation should not repeat itself in our tiered structure, but rather expand in detail. User level documentation will describe how a feature works and the benefits of the feature. Administrator documentation will talk about how to configure the feature and help with user transparent details like firewall rules. Developer documentation should not reword the documentation of the first two tiers, but should explain the design of the feature at a low enough level that changes can be planned in detail using the documentation.

### Building documentation

In the `docs/` folder there is a Makefile. If you ran the project's `install.sh` script, you should be able to type `make html` and all of the documentation will be build.

The documentation will be generated as follows:
```
daemon-html   - User/Administrator/Developer Documentation
api-html      - Sphinx Generated Code Documentation
```

We use Sphinx to parse documentation from the code into a set of HTML pages. To only generate the code documentation, navigate to the `docs` directory and run:

```
make api-html
```

This will produce a website in the `api-html` directory. You may browse these files using your web browser, or alternatively run a small web server on your loopback address by typing:

```
cd docs/daemon-api-html
python3 -m http.server 8000
```

This will make the documentation site available at [http://localhost:8000](http://localhost:8000).

The same process can be used to generate the daemon documentation written by developers:

```
make daemon-html
```

### Adding documentation to Occam

All functional changes to the system should be accompanied with some level of documentation change. Implementation only changes should at a minimum result in changes in code comments, function docstrings, and developer documentation (such as a component's README.md). User facing changes should be accompanied by updated screenshots, updated user documentation, and new changelog entries.

See the [documentation README](./README.md) for more information.

The source of our documentation is markdown files spread throughout the project. Documentation should be kept close to the component to which it relates. Linking between documentation should use relative links. Occam components are documented in a README.md file in the component's folder. Any changes related to a component should be made there.

If you are modifying existing documentation, write your changes and make sure it displays well both on Gitlab as well as the HTML help generated in the `docs` folder.

If you are adding new documentation, you will need to modify the `index.rst` files in the relevent subfolder of `docs`.


## Commits
* Commit messages should follow the guidelines suggested by [Chris Beams](https://chris.beams.io/posts/git-commit/).

* Standalone changes being in separate commits is ideal. For example, if implementing a feature requires changes A and B, but A and B do not affect one another at all, it is preferrable to have A and B in separate commits. This makes debugging and review easier.

* Squash bug fixes for commits in the same merge request.

* Be sure to make note of any database or configuration changes required from a commit in both the merge request and the commit by which the changes are triggered. After a merge these changes will affect people consuming the latest version of Occam. This information should also be tracked in a changelog to easily find upgrading steps when upgrading an Occam instance.


## Issue templates
An issue describing a bug should generally have the following information:

* Steps to reproduce

* Expected behavior

* Actual behavior

* For visual bugs, screenshots are helpful

At a minimum a feature should contain a description of the desired behavior and an explanation of the benefit of the feature. An ideal feature description would also contain:

* A detailed description of how the feature would work

* A list of system components that would be affected

* A mockup / drawing / diagram of the feature

Our GitLab is setup with issue templates to provide issue writers with an outline for these details.


## Merge request checklist
- Manually tested

- Areas with potential side effects manually tested

- Database schema changes are accompanied with migration scripts

- Documented

- Unit tests made

- Passes CI

- Passes code review


## Code review
- Check for style consistency, typos, and grammar mistakes.

- Check that all new code has comments, and if significantly modified, old code should conform to the same standard.

- Look at individual commits to more easily find unintentional changes and to understand rationale.

- Look for database modifications that are missing relevant migration scripts.

- Evaluate if the patch is overly complex for what is being attempted.

- Consider potential performance impacts if the code is in a critical path.

- Make sure comments explain why not what.

- Ensure unit tests exist for everything and that the logic is easy to understand outside of the context of the changes you are reviewing. That is to say, if you discover this code a year from now, it should be clear what it is doing and why it is doing it.

- Ensure that any changes that could break assumptions on the front-end are handled in a paired patch on the front-end repository.
