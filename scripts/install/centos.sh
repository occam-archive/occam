#!/bin/bash
set -eu

# Package Dependencies
BINARY_PACKAGES=(make gcc pkg-config curl wget tcl)
BINARY_CHECK=(make gcc pkg-config curl wget tclsh)
LIBRARY_PACKAGES=()
LIBRARY_CHECK=()
INCLUDE_PACKAGES=(ncurses-devel openssl-devel bzip2-devel readline-devel libcurl-devel zlib-devel xz-devel)
INCLUDE_CHECK=(/usr/include/ncurses.h /usr/include/openssl/crypto.h /usr/include/bzlib.h /usr/include/readline/history.h /usr/include/curl/curl.h /usr/include/zlib.h /usr/include/lzma.h)
POTENTIALLY_OLD_PACKAGES=(sqlite-devel python3 python3-devel python3-pip)
POTENTIALLY_OLD_PACKAGES_CHECK=(/usr/include/sqlite3.h /usr/bin/python3 /usr/bin/python-config /usr/bin/pip)

# Stores the packages we need to install
PACKAGES=()

for i in "${!BINARY_CHECK[@]}"; do
  binary=${BINARY_CHECK[$i]}
  package=${BINARY_PACKAGES[$i]}
  if hash ${binary} 2>/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

for i in "${!LIBRARY_CHECK[@]}"; do
  library=${LIBRARY_CHECK[$i]}
  package=${LIBRARY_PACKAGES[$i]}
  if pkg-config ${library} --libs 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

for i in "${!INCLUDE_CHECK[@]}"; do
  include=${INCLUDE_CHECK[$i]}
  package=${INCLUDE_PACKAGES[$i]}
  if find ${include} 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

# Get version numbers
version=`cat /etc/os-release | grep -c "^VERSION=" | tr -d -c 0-9`
# Get first number
version=${version:0:1}

if [[ $version -ge 9 ]]
then
  # Should be fine with yum packages
  for i in "${!POTENTIALLY_OLD_PACKAGES_CHECK[@]}"; do
    include=${POTENTIALLY_OLD_PACKAGES_CHECK[$i]}
    package=${POTENTIALLY_OLD_PACKAGES[$i]}
    if find ${include} 2>/dev/null >/dev/null; then
      echo "${package} already installed"
    else
      PACKAGES+=(${package})
    fi
  done
fi


if [ ${#PACKAGES[@]} -ne 0 ]; then
  echo
  echo "Please run the following command as root to install the required packages and re-run the install script."
  echo

  echo yum install -y ${PACKAGES[@]}

  exit 1
fi



if [[ !$version -ge 9 ]]
then
  # Build SQLite3
  ./scripts/build/sqlite3.sh

  # Build Python
  ./scripts/build/python.sh
fi

# Setup from here
./scripts/install/common.sh
