#!/bin/bash
set -eu

# Install required packages
sudo apt install python3 python3-pip python3-virtualenv build-essential libssl-dev libffi-dev python3-dev python3-venv sqlite3 golang-go cryptsetup-bin

# Also install singularity
wget https://github.com/sylabs/singularity/releases/download/v3.9.9/singularity-ce_3.9.9-focal_amd64.deb
sudo dpkg -i singularity-ce_3.9.9-focal_amd64.deb

# Setup from here
./scripts/install/common.sh
