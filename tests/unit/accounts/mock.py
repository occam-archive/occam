from unittest.mock import patch, Mock, MagicMock, PropertyMock

from occam.accounts.manager import AccountManager

from tests.helper import multihash

class AccountManagerMock(Mock):
  """ A fake AccountManager that manages mocked records for you.
  """

  def __init__(self, people=[], *args, **kwargs):
    """ Initializes the account manager.

    Arguments:
      people (list): An array of ObjectMock representing the people to create accounts for.
    """

    super(Mock, self).__init__(*args, spec_set=AccountManager, **kwargs)

    def retrievePerson(identity):
      for person in people:
        if person.identity == identity:
          return person

      return None

    self.retrievePerson = MagicMock(side_effect=retrievePerson)
