import unittest

import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from occam.configurations.manager import ConfigurationManager
from occam.objects.manager import ObjectManager

from unittest.mock import patch, Mock, MagicMock
from unittest.mock import create_autospec

class TestConfigurationManager:

  class TestTypeCheck(unittest.TestCase): 

    configManager = ConfigurationManager()
    def test_should_return_true_and_valid_int(self):
      randNumber = random.randint(0, 10000000)
      array = self.configManager.typeCheck(randNumber, "int", None)
      self.assertEqual(array[0], True) 
      self.assertEqual(array[1], randNumber)

    def test_should_return_false_when_invalid_int_and_valid_valueType(self):
      ar = [-1.23,'test',1.32,['123',1233],{"elements":[1]}]
      for x in ar:
        array = self.configManager.typeCheck(x,"int",None)
        self.assertEqual(array[0],False)
        self.assertIsInstance(array[1],str)

    def test_should_return_false_when_invalid_float_and_valid_valueType(self):
      ar = ['test',['123',123],{"elements":[1]}]
      for x in ar:
        array = self.configManager.typeCheck(x,"float",None)
        self.assertEqual(array[0],False)
        self.assertIsInstance(array[1],str)

    def test_should_return_false_when_invalid_boolean_and_valid_valueType(self):
      ar = [3,2,4,1.42,'test',['123',123],{"elements":[1]}]
      for x in ar:
        array = self.configManager.typeCheck(x,"boolean",None)
        self.assertEqual(array[0],False)
        self.assertIsInstance(array[1],str)

    def test_should_return_false_when_invalid_tuple_and_valid_valueType(self):
      ar = ['test',['123',123],{"elements":[1]}]
      for x in ar:
        array = self.configManager.typeCheck(x,"tuple",{"elements":[1,2,4]})
        self.assertEqual(array[0],False)
        self.assertIsInstance(array[1],str)      

    def test_should_return_true_and_valid_float(self):
      randNumber = random.random()
      array = self.configManager.typeCheck(randNumber, "float", None)
      self.assertEqual(array[0], True)
      self.assertEqual(array[1], randNumber)

    def test_should_return_true_and_valid_boolean_True(self):
      #boolean = True
      array = self.configManager.typeCheck(True, "boolean", None)
      self.assertEqual(array[0], True)
      self.assertEqual(array[1], True)

    def test_should_return_true_and_valid_boolean_False(self):
      #boolean = False
      array = self.configManager.typeCheck(False, "boolean", None)
      self.assertEqual(array[0], True)
      self.assertEqual(array[1], False)

    def test_should_return_true_and_valid_boolean_True_when_boolean_isnot_instance_of_bool(self):
      ar = ['true','on','yes']
      for x in ar:
        array = self.configManager.typeCheck(x, "boolean", None)
        self.assertEqual(array[0], True)
        self.assertEqual(array[1], True)
      
    def test_should_return_true_and_valid_boolean_False_when_boolean_isnot_instance_of_bool(self):
      ar = ['false','no','off']
      for x in ar:
        array = self.configManager.typeCheck(x, "boolean", None)
        self.assertEqual(array[0], True)
        self.assertEqual(array[1], False)

    def test_should_return_False_and_error_message_when_value_does_not_match_with_list(self):
      array = self.configManager.typeCheck("Test", ["test","int"], None)
      self.assertEqual(array[0], False)
      self.assertIsInstance(array[1], str)

    def test_should_return_True_and_list_of_values(self):
      array = self.configManager.typeCheck([1,2,3,4,5],"tuple",{"elements":[1,2,3,4,5]})
      self.assertEqual(array[0], True)
      self.assertIsInstance(array[1], list)

  class TestDataFor(unittest.TestCase): #DONE

    configManager = ConfigurationManager()
    objManager = ObjectManager()

    def test_should_return_value(self):
      self.configManager.objects = Mock()
      self.configManager.objects.infoFor.return_value = {"file":"foo"}
      self.configManager.objects.retrieveJSONFrom.return_value = {}
      self.assertEqual(self.configManager.dataFor("t"),{})

  class TestisItem(unittest.TestCase): #DONE
    configManager = ConfigurationManager(
)
    def test_should_return_true_when_schema_contains_dict_and_type(self):
      self.assertEqual(self.configManager.isItem({"type":'t'}),True)

    def test_should_return_false_when_schema_contains_dict_and_does_not_contains_type(self):
      self.assertEqual(self.configManager.isItem({}),False)

    def test_should_return_false_when_schema_is_not_dict(self):
      self.assertEqual(self.configManager.isItem([1,2,3]),False)

    def test_should_return_false_when_schema_type_contains_dict(self):
      self.assertEqual(self.configManager.isItem({"type":{"test":[1,2,3]}}),False)

  class TestisGroup(unittest.TestCase): #DONE

    configManager = ConfigurationManager()

    def test_should_return_true_when_not_an_item(self):
      self.assertEqual(self.configManager.isGroup({"type":{"test":[1,2,3]}}),True)

    def test_should_return_false_when_is_an_item(self):
      self.assertEqual(self.configManager.isGroup({"type":[1,2,3]}),False)

    def test_should_return_false_when_neither_item_or_group(self):
      self.assertEqual(self.configManager.isGroup([1,2,3,4]),False)

  class TestParseRangedValue(unittest.TestCase): #DONE
    configManager = ConfigurationManager()

    def test_should_return_array_of_ranged_values(self):
      ar = self.configManager.parseRangedValue("123=123!32...64:x^2")
      self.assertIsInstance(ar,tuple)
      self.assertIsInstance(ar[0],str)
      self.assertIsInstance(ar[1],str)
      self.assertIsInstance(ar[2],list)
      self.assertIsInstance(ar[2][0][0],str)
      self.assertIsInstance(ar[2][0][1],str)
      self.assertIsInstance(ar[2][0][2],str)

    def test_should_return_array_of_ranged_values_with_None(self):
      ar = self.configManager.parseRangedValue(123)
      self.assertIsInstance(ar,tuple)
      self.assertEqual(ar[0],None)
      self.assertEqual(ar[1],None)
      self.assertIsInstance(ar[2],list)
      self.assertIsInstance(ar[2][0][0],str)
      self.assertIsInstance(ar[2][0][1],str)
      self.assertIsInstance(ar[2][0][2],str)

  class TestRangedValuesFor(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_empty_array_when_values_is_None_and_schema_nor_item_or_group(self):
      t = self.configManager.rangedValuesFor("123","x",None)
      self.assertEqual(t,[])

    def test_should_return_array_when_schema_type_array_and_schema_is_item(self):
      t = self.configManager.rangedValuesFor(("2048...4096:X^2"),{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "element":{"type":{"test":[1,2,3]}}
      })
      self.assertIsInstance(t,list)

    def test_should_return_array_when_schema_is_item_and_type_tuple(self):
      t = self.configManager.rangedValuesFor(("2048...4096:X^2", "MB"),{
        "type": "tuple",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [
          {
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }
            ]
          },
          {
            "default": "MB",
            "type": ["MB", "GB"]
          }
        ]
      })
      self.assertIsInstance(t,list)

    def test_should_return_array_when_schema_is_group(self):
      t = self.configManager.rangedValuesFor({"memory":"memory","size":2048,"banks":2},{"memory":{
        "label":"memory","size":"2048","type":"int"}})
      self.assertIsInstance(t,list)

    def test_should_return_array_when_schema_type_string_and_schema_is_item(self):
      t = self.configManager.rangedValuesFor(({"values":"2048...4096:X^2"}),{
        "type": "string",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "element":{"type":{"test":[1,2,3]}}
      })
      self.assertIsInstance(t,list)
      
    def test_should_return_array_when_schema_is_group_but_items_not_dict(self):
      t = self.configManager.rangedValuesFor({"memory":"memory","size":2048,"banks":2},{"memory":({"label":"memory"},{"size":"2048"},{"type":"int"}),"test":{"test":{"test":1}}})
      self.assertIsInstance(t,list)  

  class TestDefaultsFor(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_None_if_schema_nor_item_or_group(self):
      self.assertEqual(self.configManager.defaultsFor("123"),None)

    def test_should_return_list_when_schema_is_item_and_type_is_tuple(self):
      t = self.configManager.defaultsFor({
        "type": "tuple",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [
          {
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }
            ]
          },
          {
            "default": "MB",
            "type": ["MB", "GB"]
          },
          {
            "default":"25.2",
            "type":"float"
          },
          {
            "default":"test",
            "type":"string"
          },
          {
            "default":"invalid",
            "type":"int"
          }
        ]
      })
      self.assertIsInstance(t,list)

    def test_should_return_dict_when_schema_is_group(self):
      t = self.configManager.defaultsFor({"memory":{
        "label":"memory","default":2048,"type":{"memory1","memory2"}}})
      self.assertIsInstance(t,dict)

    def test_should_return_empty_dict_when_schema_is_group_and_not_instance_of_dict(self):
      t = self.configManager.defaultsFor({"memory":["0","1","2"]})
      self.assertIsInstance(t,dict)

  class TestVariablesIn(unittest.TestCase):

    configManager = ConfigurationManager()

    def test_should_return_array_when_schema_is_item_type_tuple_and_value_len_equal_elements_len(self):
      t = self.configManager.variablesIn(("i=1024...4096:x^2","MB=1"),{
        "type": "tuple",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      })
      self.assertCountEqual(t,["i","MB"])

    def test_should_return_error_when_schema_is_item_type_tuple_and_value_len_not_equal_elements_len(self):
      t = self.configManager.variablesIn(("i=1024...4096:x^2","MB=MB"),{
        "type": "tuple",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [
          {
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }
            ]
          }
        ]
      })
      self.assertIsInstance(t,str)

    def test_should_return_array_with_variables_when_schema_is_item_and_type_is_not_string(self):
      t = self.configManager.variablesIn("test=256...1024:x^2",{
        "type": "int",
        "elements":[{
          "default":2048,
          "type":"int"
        }]
      })
      self.assertCountEqual(t,["test"])

    def test_should_return_array_with_variables_when_schema_is_group(self):
      t = self.configManager.variablesIn({"memory": "i=1"},
        {"memory":
          {
            "label":"memory",
            "default":2048,
            "type": "int"
          }
        })
      self.assertCountEqual(t,["i"])

    def test_should_return_empty_array_when_schema_is_group_and_schemaItems_not_instance_dict(self):
      t = self.configManager.variablesIn({"memory": "i=1"},{"memory":"test"})
      self.assertCountEqual(t,[])

    def test_should_return_array_when_schema_is_item_type_tuple_and_value_not_tuple(self):
      t = self.configManager.variablesIn(("i=1024...4096:x^2"),{
        "type": "tuple",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          }
        ]
      })
      self.assertCountEqual(t,["i"])

  class TestSchemaItem(unittest.TestCase):

    configManager = ConfigurationManager()

    def test_should_return_dictionary_when_type_is_tuple(self):
      t = self.configManager.schemaItem({"xe":{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "element": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      },"abc":{"type": "tuple","elements":[{"default":245,"type": "int"}]}},"abc[0]")
      self.assertIsInstance(t,dict)

    def test_should_return_list_when_type_is_array(self):
      t = self.configManager.schemaItem({"xe":{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      },"abc":{"type": "array","element":[{"default":245,"type": "int"}]}},"abc[0]")
      self.assertIsInstance(t,list)

  class TestValidate(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_true_and_range_value(self):
      t = self.configManager.validate("abc[0]","i=1024...2048:x^2",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      self.assertEqual(t[0],True)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_string_error_when_range_is_invalid(self):
      t = self.configManager.validate("abc[0]","i=1024...23",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      # I Believe that the following if (Line 747) is never executed:
      #   if values is None:
      #     return [False, "Range is invalid."]
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_msg_when_range_does_not_include_valid_value(self):
      t = self.configManager.validate("abc[0]","i=1024...23",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_msg_when_final_range_is_invalid(self):
      t = self.configManager.validate("abc[0]","i=1024...",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_msg_when_start_range_is_invalid(self):
      t = self.configManager.validate("abc[0]","i=-1...2013",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_msg_when_valueType_is_invalid_with_value(self):
      t = self.configManager.validate("abc[0]","i=1024...2048:x^2",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"boolean"}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_msg_when_value_less_than_validation_min_and_value_int(self):
      t = self.configManager.validate("abc[0]",2048,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int","validations":[{"min":4096}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_when_number_greater_than_max_and_value_int(self):
      t = self.configManager.validate("abc[0]",2048,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int","validations":[{"max":1024}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_when_number_less_than_min_and_value_float(self):
      t = self.configManager.validate("abc[0]",4096,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"float","validations":[{"min":4096.2}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_when_number_greater_than_max_and_value_float(self):
      t = self.configManager.validate("abc[0]",1025,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"float","validations":[{"max":1024.52}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_True_and_value(self):
      t = self.configManager.validate("abc",2048,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}},None)
      self.assertEqual(t[0],True)
      self.assertIsInstance(t[1],list)

    def test_should_return_False_and_error_msg_when_type_does_not_match_on_elements(self):
      t = self.configManager.validate("abc",2048,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"tuple"}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_True_and_value_when_type_is_array(self):
      t = self.configManager.validate("abc[0]",2048,{"test":"teste"},{"abc":{"type":"array","element":{"type":"int"}}},None)
      self.assertEqual(t[0],True)
      self.assertIsInstance(t[1],int)

    def teste_should_return_False_when_test_validation_fails(self):
      t = self.configManager.validate("abc[0]",1025,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"float","validations":[{"test":"x<1024"}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_False_and_error_when_cannot_validate_inside_while(self):
      t = self.configManager.validate("abc[0]","i=1024...2048:x^2",{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"float","validations":[{"min":5012}]}]}},None)
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

  class TestResolveValue(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_False_and_error_when_variableName_not_in_variables(self):
      t = self.configManager.resolveValue("(13371920)identifier=42","int","123123132","teste")
      self.assertEqual(t[0],False)
      self.assertIsInstance(t[1],str)

    def test_should_return_True_and_array(self):
      t = self.configManager.resolveValue("2048","int",{"i":[0]},[{0:[{"default":2048,"type":"int"},{"2":2}]},{"1":[{"1":1}]}])
      self.assertEqual(t[0],True)
      self.assertIsInstance(t[1],list)

    def test_should_return_True_and_error_message(self):
      t = self.configManager.resolveValue("i=2048","int",{"i":[0]},[{0:[{"default":2048,"type":"int"},{"2":2}]},{"1":[{"1":1}]}])
      print(t)
      self.assertEqual(t[0],True)
      self.assertIsInstance(t[1],list)

  class TestIterate(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_array_containing_start_when_step_is_none(self):
      t = self.configManager.iterate(1024,2048,None,"int",{"teste": "teste"})
      self.assertCountEqual(t,[1024])

    def test_should_return_array_containing_start_and_step(self):
      t = self.configManager.iterate(1024,2048,"2","int",{"test":{"type":"tuple","elements":{"type":"int","default":1024}}})
      self.assertCountEqual(t,[1024,2])

    def test_should_return_array_containing_float_start_and_step(self):
      t = self.configManager.iterate(1024.16,2048.64,"2","float",{"test":{"type":"tuple","elements":{"type":"float","default":1024}}})
      self.assertCountEqual(t,[1024.16,2])

  class TestSet(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_array_with_first_parameter_equal_key(self):
      t = self.configManager.set("abc",2048,{"abc":2048},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}})
      self.assertCountEqual(t[0],[2048])
      self.assertEqual(t[1],None)

    def test_should_return_first_parameter_as_None_and_second_as_string(self):
      t = self.configManager.set("abc[0]",2048,{"test":"teste"},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int","validations":[{"min":4096}]}]}})
      self.assertEqual(t[0],None)
      self.assertIsInstance(t[1],str)

    @patch('occam.configurations.manager.KeyParser')
    def test_should_return_array_with_first_parameter_equal_key_and_chunk_isistance_list(self,mock_KeyParser):
      mock_KeyParser.return_value.getParent.return_value = (['teste'],1)
      print(mock_KeyParser.return_value.getParent)
      t = self.configManager.set("abc",2048,{"abc":2048},{"abc":{"type":"tuple","elements":[{"default":2048,"type":"int"}]}})
      mock_KeyParser.assert_any_call()
      self.assertEqual(t[0],2048)
      self.assertEqual(t[1],None)

  class TestDelete(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_data_as_dictionary(self):
      t = self.configManager.delete("abc",{"abc":123},{"xe":{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      },"abc":{"type": "tuple","elements":[{"default":245,"type": "int"}]}})
      self.assertIsInstance(t,dict)

    def test_should_return_data_as_dictionary_when_deleted(self):
      t = self.configManager.delete("abc",{"abc":123},{"xe":{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      },"abc":{"type": "test"}})
      self.assertIsInstance(t,dict)

  class TestGet(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_data_value(self):
      t = self.configManager.get("abc",{"abc":123},{"xe":{
        "type": "array",
        "label": "Memory Size",
        "description": "The amount of memory to simulate in total.",
        "elements": [{
            "default": 2048,
            "type": "int",
            "validations": [
              {
                "min":    "1"
              },
              {
                "test":    "log2(x) == floor(log2(x))",
                "message": "Must be a power of two"
              }]
          },{
            "default": 2048,
            "type": "int"
          }
        ]
      },"abc":{"type": "tuple","elements":[{"default":245,"type": "int"}]}})
      self.assertIsInstance(t,int)

  class TestSchemaFor(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_dictionary_when_passing_only_obj(self):
      self.configManager.objects.infoFor = Mock(return_value={"schema":{"default":2048,"type":"int"}})
      self.configManager.objects.retrieveJSONFrom = Mock(return_value={"file":"foo"})
      t = self.configManager.schemaFor({"tste":"test"})
      self.configManager.objects.infoFor.assert_called_once()
      self.configManager.objects.retrieveJSONFrom.assert_called_once()
      self.assertIsInstance(t,dict)

    def test_should_return_dictionary_when_passing_obj_and_index(self):
      self.configManager.objects.infoFor = Mock(return_value={"schema":{"default":2048,"type":"int"},"inputs":["1",{"type":"configuration","schema":{"test":"test"}},"3"]})
      self.configManager.objects.retrieveJSONFrom = Mock(return_value={"file":"foo"})
      t = self.configManager.schemaFor({"tste":"test"},1)
      self.configManager.objects.infoFor.assert_called_once()
      self.configManager.objects.retrieveJSONFrom.assert_called_once()
      self.assertIsInstance(t,dict)

    def test_should_return_dictionary_when_passing_obj_and_index_and_id_in_schemaFile(self):
      self.configManager.objects.infoFor = Mock(return_value={"schema":{"default":2048,"type":"int"},"inputs":["1",{"type":"configuration","schema":{"file":"anotherFile","test":"test","id":"231","revision":"revision"}},"3"]})
      self.configManager.objects.retrieveJSONFrom = Mock(return_value={"file":"foo"})
      self.configManager.objects.retrieve = Mock(return_value={"something":"another"})
      t = self.configManager.schemaFor({"tste":"test"},1)
      self.configManager.objects.infoFor.assert_called_once()
      self.configManager.objects.retrieveJSONFrom.assert_called_once()
      self.configManager.objects.retrieve.assert_called_once()
      self.assertIsInstance(t,dict)

    def test_should_return_dictionary_when_passing_obj_and_index_and_key_and_id_in_schemaFile(self):
      self.configManager.objects.infoFor = Mock(return_value={"schema":{"default":2048,"type":"int"},"inputs":["1",{"type":"configuration","schema":{"file":"anotherFile","test":"test","id":"231","revision":"revision"}},"3"]})
      self.configManager.objects.retrieveJSONFrom = Mock(return_value={"abc":{"type": "tuple","elements":[{"default":245,"type": "int"}]}})
      self.configManager.objects.retrieve = Mock(return_value={"something":"another"})
      t = self.configManager.schemaFor({"tste":"test"},1,"abc")
      self.configManager.objects.infoFor.assert_called_once()
      self.configManager.objects.retrieveJSONFrom.assert_called_once()
      self.configManager.objects.retrieve.assert_called_once()
      self.assertIsInstance(t,dict)

  class TestPermutor(unittest.TestCase):
    configManager = ConfigurationManager()

    @patch('occam.configurations.manager.Permutor')
    def test_should_assert_that_function_permute_was_called_once(self,Permutor):
      self.configManager.permute("abc","abc")
      Permutor.assert_called_once()

  class TestExpandRangedValue(unittest.TestCase):
    configManager = ConfigurationManager()

    def test_should_return_array_with_configurationSpace(self):
      t = self.configManager.expandRangedValue([("123",[("1024","2048","2")],"int",None,"i",{"type":"int","default":512})])
      self.assertIsInstance(t,list)

    def test_should_return_array_with_empty_configurationSpace(self):
      t = self.configManager.expandRangedValue([("123",[("1024","2048","2")],"int",True,"i",{"type":"int","default":512})])
      self.assertIsInstance(t,list)

    def test_should_return_array_with_empty_configurationSpace_due_to_wrong_start_in_ranges(self):
      t = self.configManager.expandRangedValue([("123",[("abc","2048","2")],"int",None,"i",{"type":"int","default":512})])
      self.assertIsInstance(t,list)

    def test_should_return_array_with_empty_configurationSpace_due_to_wrong_final_in_ranges(self):
      t = self.configManager.expandRangedValue([("123",[("1024","abc","2")],"int",None,"i",{"type":"int","default":512})])
      self.assertIsInstance(t,list)

    def test_should_return_array_(self):
      t = self.configManager.expandRangedValue([(("123","124"),[("1024","2048","2")],"int",0,"i",{"type":"int","default":512}),(("123",'124'),[("1024","2048","2")],"int","i","i",{"type":"int","default":512})])
      self.assertIsInstance(t,list)