# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log import Log

from occam.manager import uses

from occam.links.write_manager import LinkWriteManager
from occam.objects.manager     import ObjectManager

from occam.commands.manager import command, option, argument

@command('links', 'untrack',
  category      = 'Link Management',
  documentation = "Destroys a tracked link for a particular staged object")
@argument("source", type="object", help="the staged object to unstage")
@uses(LinkWriteManager)
@uses(ObjectManager)
class LinkUntrackCommand:
  def do(self):
    # The object must be staged
    if self.options.source.link is None:
      Log.error("Object must be staged (%s is not in the form id#link)" % (self.options.source.id))
      return 1

    # Resolve the object
    obj = self.objects.resolve(self.options.source, person = self.person)
    if obj is None:
      Log.error("Cannot find source object (%s#%s)" % (self.options.source.id, self.options.source.link))
      return 1

    # Destroy the stage
    self.links.write.destroyLocalLink(obj.link, obj.path)
    return 0
