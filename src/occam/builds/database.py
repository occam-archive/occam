# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("builds")
class BuildDatabase:
  """ Manages the database interactions for keeping track of builds.
  """

  def queryBuilds(self, id=None, revision=None, taskId=None, taskRevision=None, filterTokens=None, identity=None):
    builds = sql.Table("builds")

    query = builds.select()

    query.where = sql.Literal(True)
    if id is not None:
      query.where = (builds.id == id)

    if revision is not None:
      query.where = query.where & (builds.revision == revision)

    if taskId is not None:
      query.where = query.where & (builds.build_id == taskId)

    if taskRevision is not None:
      query.where = query.where & (builds.build_revision == taskRevision)

    if identity:
      query.where = query.where & (builds.identity_uri == identity)

    for filterToken in (filterTokens or []):
      query.where = query.where & (builds.filter_token.like("%;" + filterToken + "%"))

    # Order by newest (signature)
    query.order_by = sql.Desc(builds.signed)

    return query

  def retrieveBuilds(self, id=None, revision=None, taskId=None,
                                                   filterTokens=None,
                                                   identity=None):
    from occam.builds.records.build import BuildRecord

    session = self.database.session()

    query = self.queryBuilds(id, revision, taskId=taskId,
                                           filterTokens=filterTokens,
                                           identity=identity)

    self.database.execute(session, query)
    return [BuildRecord(x) for x in self.database.many(session, size=100)]

  def retrieveBuild(self, id, revision, taskId, taskRevision, identity):
    from occam.builds.records.build import BuildRecord

    session = self.database.session()

    query = self.queryBuilds(id, revision, taskId, taskRevision, identity=identity)

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret is not None:
      ret = BuildRecord(ret)

    return ret
