# The Builds Component

This component manages the build storage and retrieval.

## Commands

### `builds` `new`

This command will invoke a build process directly, if possible, on the local
machine.

### `builds` `queue`

This command will queue a build process which will occur when the job
scheduler pulls it from the queue.

### `builds` `view`

This command will view data from an existing build.

### `builds` `list`

This command will list the builds for an object or list the files within an
existing build.

### `builds` `status`

This command will reveal status of a particular build or the file metadata for
a file within an existing build.

### `builds` `verify`

This command will verify the given build against the signature information
known to the system.

### `builds` `sign`

This command will sign the given build with your signing key.

### `builds` `job-done`

This command is an internal command that is issued by the job scheduler when a
queued build has completed. This command is responsible for finalizing the
build.

## Philosophy

With any object, that object might have a source representation and a built
representation. In traditional software, this might comprise of a compilation
step where source code is transformed into machine code. It might also be a
processing step where data is transformed or decompressed in some specific way.
In either case, there is typically a drastically different process taken when
building software than running that software. There might even be a different
hardware requirement. This is the case for software that is built on one machine
which has the resources to compile code, but it runs on lower power hardware.

In this system, there is a desire to track the exact circumstances that created
any build of an object. This information can be used to determine which
libraries and software were used, in case any have a defect. The information
could be leveraged to understand the complexity of the software whereas the
built software might hide where the code or the dependencies in its built form.
And, of course, the information is useful in order to repeat the build a second
time.

A repeatable build is particularly useful not just in the more obvious aspect
of preserving that particular build, but also in updating and modifying the
software object. When the exact process is kept, a small edit to the software
object can be issued and the compilation step and environment recreated with the
same structure. All such dependencies are kept in the same manner as any
object in the system. That is, the build environment is not kept as an opaque
container image or binary blob. Rather, it is simply kept as metadata and relies
on the system itself to keep mirrors of the objects tracked in the build
manifest. This keeps everything rather lightweight and incentivizes the
retention of those objects.

Trust in the system is tracked via actor keys. As described in the Object
Component, objects in the system have identifiers that are partly hashed using
the public key of the actor that owns that object. The same key has an
associated signing key that is used by that actor to mark build information as
accurate. When a build is made, the resulting binary files that make up the
built package are hashed along with the build log (the output of the build
process) and the build metadata (information about the machine on which the
object was built.) This comprises the build hash. This is signed by the actor
that initiated the build. When one discovers an object, it pulls the built
package and verifies the integrity and authenticity of the build by re-hashing
the files, the build log, and the build metadata. This associates a build with
an object, and both with the particular actor that created the object. This
association is verifiable at any node without participation of the originating
node with just the public keys (the main public key and the verification key
associated with the signing key) of the actor.

Malicious actors would have to either forge a build that is verifiable with the
verification key, or somehow modify an existing build that would trigger a false
positive verification with an existing signature. Both of these situations are
very likely incredibly difficult tasks. On top of that, the resulting build
package would have to contain a build that runs in a way that would not be
suspicious, as any useless build could simply be flagged and thrown out on a
future sophisticated system.

## What is a Buildable Object?

Any object in the system that has a `build` section in the object's metadata is
a buildable object. This section has the same format as the `run` section. This
includes `environment` and `architecture` tags that describe the intended
runtime environment for the virtual machine in which the build will take place.
These are inherited from the root metadata if not specified, but never inherited
from the `run` section. There is also a `command` field that is either a string
forming the command or an array of strings comprising the arguments for the
command. See the [Objects Component](../objects/README.md) for more information
on these fields.

When such a section exists, the object cannot be invoked without the existence
of a built package. This package is the result of running the object's build
phase. The `builds` component is responsible for creating these packages by
either invoking the build phase directly or queuing the build to be invoked as
resources are available.

When a build phase has completed successfully, the build package is stored and
signed. This enables the invocation of the object where the `run` section will
be interpreted alongside the contents of the build package, as opposed to the
contents of the object itself. That is, if the `run` `command` field specifies
`/usr/bin/my-app`, then this path refers to a file copied from the built
package and not a file copied from the object artifact. If a file exists in the
object artifact that is meant to be seen from the running invocation, that file
must be copied to the build package as part of the build phase command or
script. For instance, this is true for C or C++ so-called "include files" which
are part of the build process, but they are also useful when using the resulting
built library. These files must then be copied from the object's source code,
which is visible and used during the build phase, into the resulting build
package alongside the normal compiled binaries. This is traditionally already
done by a build script's "install" step.

It will always remain useful to simply look at some existing object that is
most similar to any that one is attempting to build to see how those steps are
generally taken.

## Identifiers

Each build is identified by the object identifier of the task manifest that
describes the build environment. For instance, when building an object, that
object describes its `build` requirements. These are turned into the actual
task manifest via the [Manifests Component](../manifests/README.md). This task
manifest is used to create the virtual machine and, then, the built package.
Considering that task uniquely describes that build, the task's object
identifier is then used to uniquely identify that particular build. The task
itself is then stored and distributed as any other object would.

## Storage

The build is stored as a directory containing the files within the build
package. That path is currently within the object repository off a `builds`
directory and a subdirectory based on the object revision of the build.
The build itself is then within a subdirectory that is given the name of the
task identifier for that build. Then the files comprising the build package
are located inside.

These files are also compressed into at least one compressed file for
the ease of distribution. That file is likely located at the base of the
build package's path with an extension that matches the type of compression
used.

The build log is also stored alongside the build package with a name that is
the task identifier with the `.log` extension.

The build metadata is similarly stored as the task idenifier with the `.json`
extension. The data is structured as a JSON document.

As an example, the build files might look like this:

```
.../objects/Qm/RG/BZ/Xb/QmRGBZXbAtKTk3rqyS9NU8HYh9HiMsYL1s3vRD2snaSMK7/
  builds/
    5drP4QdLuhjMQ9JSFxHthDHWeWPig8/
      QmTE53bYFjoEgj4M8YQo9CKuqCKTk4xuwUz6U5jP58jKZE.log
      QmTE53bYFjoEgj4M8YQo9CKuqCKTk4xuwUz6U5jP58jKZE.json
      QmTE53bYFjoEgj4M8YQo9CKuqCKTk4xuwUz6U5jP58jKZE.tar.xz
      QmTE53bYFjoEgj4M8YQo9CKuqCKTk4xuwUz6U5jP58jKZE/
        usr/
          bin/
            make
          include/
            make.h
```

Here, the contents of the build, which were produced by the build process, are
within the nested `QmTE53bYFjoEgj4M8YQo9CKuqCKTk4xuwUz6U5jP58jKZE/` path. The
files that are part of the build package are `usr/bin/make` and
`usr/include/main.h`. These are also within the compressed package with the
`tar.xz` extension. The build process produced some text output which is
captured by the file with the `.log` extension. Finally, the machine metadata
and any other meta-information captured when the build was produced is captured
as part of the file ending in `.json`.

## Metadata

For each build, several pieces of metadata are preserved alongside the build
package. First, is the hash of the build. This hash is computed by
concatenating the hashes of every file, the build log, and the build metadata
file and then digesting the resulting data stream with a SHA256 hash.

The hashes of every file part of the data stream is computed using the hashdeep
1.0 algorithm. It specifically must hash the files in a sorted order to ensure
hashes are reproducible. The hashdeep algorithm is hashing each file
individually as a SHA256 hash and creating a text-based record of those hashes
which is what comprises that original data stream that is, itself, hashed to
form the build's hash with a SHA256 digest.

The "build log" part of the data stream is simply the byte stream that was the
text output of the build task. This would be the logged output of compiling,
for instance.

Finally, the "build metadata" aspect of the data stream that is preserved along
with the build is a collection of metadata supplied by the system on which the
build was invoked. This metadata describes the native environment of the build
task, not the virtual environment, which is already described by the task
manifest. The structure of this metadata is not necessarily well-formed.
However, there are some general fields which should be supplied when possible:

`built`: An ISO8601 formatted string that describes the date when the build
completed.

`elapsed`: The number of seconds that elapsed during the actual runtime of the
build. This does not include and preprocessing or finalization.

`platform`: A collection of text metadata describing the system details. This
contains `machine` which describes the architecture, `system` which describes
the operating system, `kernel` which describes the kernel version, and `version`
which is some lower-level description of the kernel build.

`processors`: A list of the processors which are present on the system. This
does not have to describe the processors that were actually used. Rather, this
describes the entire capability of the machine. For each of these, there are
several fields that go into detail about the capability of each individual core
in the list. These fields are dependent on the type and family of CPU.

`occam`: A description of the Occam system running on the node that generated
the build package. This includes a `version` subfield that describes the
version of Occam.

## Run Tasks

When a build is completed, the build finalization will produce a task manifest
to invoke the, now built, object. This makes it the object owner's
responsibility to produce the run task. The philosophy behind this is two-fold.
First, it is very likely that the owner can produce the appropriate run task
for the object. That is, the runtime dependencies were chosen by that actor and
are, naturally, present on the system where the build will take place.
And second, this applies the performance cost of producing the run task to the
job currently running the build. Therefore, the accounting for the runtime it
takes to build an object also accounts for the time it may take to generate a
run task. It is not likely to take a relative long time, but it is not trivial
either to perform, potentially arbitrary, dependency resolution.

Once the object owner has built its object, the run task produced at this
moment by that same actor will be the official way of running that object.
When an object is discovered, that partial task will then also be discovered and
pulled. This task is signed and therefore verified upon discovery before being
stored and propagated on any new node. The actor is, then, responsible for
ensuring that the object runs correctly using this task. The task can be
regenerated with different versions of objects satisfying dependencies at any
time. Upon discovery of a new run task, it will take the place of the existing
one if it is 1. by the same actor and 2. if the signature is newer than the
existing signature.

If a run task is not known when a build object is invoked, it will have to be
created when the job is queued. This might happen in the case where the run
task cannot be verified or the run task could not be generated after a build.
It is possible and reasonable to disallow generation of partial tasks by
arbitrary actors not during a build job. That is, the task generation might
only be possible during a queued job so that the time can be accounted and
applied against an actor.

## Discovery

To be described.
