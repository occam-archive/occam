from occam.commands.manager import command, option, argument

from occam.log import Log

from occam.manager import uses

from occam.network.manager  import NetworkManager

import sys

@command('network', 'post',
  category      = 'Networking',
  documentation = "Uses the Occam protocol to post to the given URL.")
@argument("url")
@uses(NetworkManager)
class DaemonCommand:
  """ This command posts to the URL argument. Optionally reading from stdin.

  This is very useful for testing the daemon by hand.
  """

  def do(self):
    """ Perform the command.
    """
    import codecs
    import select
    import sys

    # Send the command to the running daemon.
    socket, _, _ = self.network.postOccam(self.options.url,
                                          data = None,
                                          accept = 'application/octet-stream',
                                          promoted = True)

    # Failed to connect.
    if not socket:
      return 1

    # Balance between reading from stdin and writing to the socket.
    readers = [sys.stdin.fileno(), socket]
    while True:
      r, _, _ = select.select(readers, [], [])

      if len(r) <= 0:
        break

      if socket in r:
        d = socket.read(1024)
        if not d:
          break
        Log.output(codecs.decode(d, 'utf8'))
      else:
        socket.write(sys.stdin.read(1024))
    return 0
