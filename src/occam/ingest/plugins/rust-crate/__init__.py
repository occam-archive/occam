# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager

from occam.manager import uses

from occam.git_repository import GitRepository
from occam.semver import Semver

import uuid as UUID
import os, re, json

@package("rust-crate")
@uses(ObjectManager)
@uses(NetworkManager)
class RustCrate:
  """ This aids in importing rust crates into Occam for preservation.
  """

  RUST_IDENTITY = "6MutXyXE6JBndfmJYmWUpJisHY6FfRMzQn4JiLV8q6Vxk1L"

  INDEX_SOURCE = "https://github.com/rust-lang/crates.io-index"

  INDEX_URL = "https://raw.githubusercontent.com/rust-lang/crates.io-index/master"

  CANONICAL_SOURCE = "https://crates.io/api/v1/crates/%s"

  def __reformVersion(self, version):
    """ Converts a cargo version to an Occam version.

    Arguments:
      version (str): The cargo version.

    Returns:
      str: The corresponding Occam version.
    """

    return version.replace('+', '.')

  def __queryLockfile(self, identity, lockfile, tomlfile=None):
    """ Retrieves all referenced crates in the given lockfile.

    Optionally, can refer to a Cargo.toml file as well.

    Arguments:
      identity (str): The identity URI of the ingesting actor.
      lockfile (str): The path to the lockfile to parse.
      tomlfile (str): The path to the Cargo.toml to also parse.

    Returns:
      list: A set of metadata referencing the ingested objects.
    """

    import toml

    ret = []
    data = {}
    info = {}
    members = []

    with open(lockfile, 'r') as f:
      data = toml.load(f)

    if tomlfile:
      with open(tomlfile, 'r') as f:
        info = toml.load(f)

      members = info.get('workspace', {}).get('members', [])
      members.append(info.get('package', {}).get('name'))

    for package in data.get('package', []):
      if package['name'] in members:
        continue

      ret.append({
        'name': package['name'],
        'version': package['version']
      })

      # Ok, now present the occam dependency
      sourceURL = self.sourceURLFor(package['name'])
      id = self.idFor(identity, occamType = "rust-crate",
                                occamName = package['name'],
                                crateName = package['name'],
                                sourceURL = sourceURL)
      RustCrate.Log.output('{')
      RustCrate.Log.output(f'  "name": "{package["name"]}",')
      RustCrate.Log.output(f'  "type": "rust-crate",')
      RustCrate.Log.output(f'  "id": "{id}",')
      RustCrate.Log.output(f'  "version": "{self.__reformVersion(package["version"])} || {self.__reformVersion(package["version"]) + ".x"}",')
      RustCrate.Log.output(f'  "replace": "never"')
      RustCrate.Log.output('},')

    return ret

  def __queryAll(self):
    """ Retrieves a listing of all packages.
    """

    RustCrate.Log.write(f"Parsing crate listing at {RustCrate.INDEX_SOURCE}")

    ret = []

    # The crate index is a git repository we need to clone
    #data, content_type, size = self.network.get(RustCrate.INDEX_SOURCE)

    #RustCrate.Log.writePercentage("Parsing...")

    #position = 0
    #for line in data.readlines():
    #  position += len(line)
    #  RustCrate.Log.updatePercentage(position / size * 100)

    #  line = line.strip()
    #  if line.startswith(b'<a'):
    #    name = line.split(b'>', 1)[1].split(b'<', 1)[0].decode('utf-8')
    #    ret.append(name)

    #RustCrate.Log.updatePercentage(100)

    return ret

  def __queryMetadata(self, crateName, version = None):
    """ Retrieves the crate metadata for the given rust crate that matches the given version.

    Returns:
      None when the data cannot be found.
    """

    version = self.__reformVersion(version)

    # Get the metadata listing
    metadata = self.__retrieveMetadata(crateName)

    # Bail if the crate cannot be found
    if metadata is None:
      return None

    # Gather the versions
    releases = [crate['vers'] for crate in metadata]

    # Reform the version tags to fit the Occam scheme
    releases = list(map(lambda x: self.__reformVersion(x), releases))

    # Sort the releases (usually they seem to be sorted already, but whatev)
    releases = Semver.sortVersionList(releases)

    # Go through the releases until we find something we can use
    for release in reversed(releases):
      if not version or Semver.resolveVersion(version, [release]) or Semver.resolveVersion(version + ".x", [release]):
        for crate in metadata:
          crateVersion = self.__reformVersion(crate['vers'])
          if crateVersion == release:
            return crate

    return None

  def __retrieveMetadata(self, crateName, version = None):
    """ Retrieves the crate metadata for the given rust crate at the given package version.

    Returns:
      None when the data cannot be found.
    """

    # Look up the metadata from the crate index
    
    # The crate index <index url>: https://github.com/rust-lang/crates.io-index
    # And at <index url>/blob/master/<a>/<b>/<crateName>
    # where <a> is the first two letters and <b> is the next two letters
    # Then it is the entire crate name.

    url = f"{RustCrate.INDEX_URL}/{crateName[0:2]}/{crateName[2:4]}/{crateName}"

    if len(crateName) <= 2:
      url = f"{RustCrate.INDEX_URL}/{len(crateName)}/{crateName}"

    if len(crateName) == 3:
      url = f"{RustCrate.INDEX_URL}/3/{crateName[0]}/{crateName}"

    RustCrate.Log.noisy(f"Pulling metadata at {url}")

    ret = []

    try:
      data, _, size = self.network.get(url)
      for line in data.readlines():
        line = line.decode('utf-8')
        item = json.loads(line)
        ret.append(item)
    except Exception as e:
      ret = None

    return ret

  def sourceURLFor(self, crateName):
    """ Returns the canonical source url.
    """

    sourceURL = RustCrate.CANONICAL_SOURCE % (crateName.lower())

    return sourceURL

  def uidFor(self, occamType, occamName, crateName, sourceURL):
    """ Returns the uid for a particular source package name.
    """

    return self.objects.uidFor(None, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def idFor(self, identity, occamType, occamName, crateName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.idFor(None, identity, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def queryAll(self, identity, lockfile=None, tomlfile=None):
    """ Returns a list of all crates.
    """

    if lockfile:
      return self.__queryLockfile(identity, lockfile, tomlfile)

    return self.__queryAll()

  def query(self, identity, crateName, pending = None, version = None):
    """ Returns the id and initial metadata for the crate.
    """

    # Retrieve metadata
    metadata = self.__queryMetadata(crateName, version = version)

    # Get the checksum
    checksum = metadata['cksum']

    # Get the canonical name
    name = metadata['name']
    version = self.__reformVersion(metadata['vers'])

    # Get the canonical URL
    sourceURL = self.sourceURLFor(name)

    # Now we generate the object metadata
    objectInfo = {
      "name": name,
      "type": "rust-crate",
      "source": sourceURL,
      "install": [
        {
          "type": "resource",
          "subtype": "application/gzip",
          "source": sourceURL + "/" + metadata['vers'] + "/download",
          "to": f"{name}-{metadata['vers']}.tar.gz",
          "name": f"{name} {metadata['vers']} Source Code",
          "actions": {
            "unpack": "."
          }
        }
      ],
      "dependencies": [
      ],
      "init": {
        "link": [
          {
            "source": f"{name}-{metadata['vers']}",
            "to": f"/opt/cargo/{name}-{metadata['vers']}"
          },
          {
            "source": ".cargo-checksum.json",
            "to": f"/opt/cargo/{name}-{metadata['vers']}/.cargo-checksum.json"
          }
        ]
      }
    }

    id = self.idFor(identity, occamType = "rust-crate",
                              occamName = name,
                              crateName = name,
                              sourceURL = sourceURL)

    uid = self.uidFor(occamType = "rust-crate",
                      occamName = name,
                      crateName = name,
                      sourceURL = sourceURL)

    RustCrate.Log.write("uid: {}".format(uid))
    RustCrate.Log.write(" id: {}".format(id))

    ingestRequests = []

    # Gather dependencies
    for dependency in metadata['deps']:
      if dependency['optional'] == True:
        continue

      if dependency['kind'] != 'normal':
        continue

      if dependency.get('package'):
        # This is the actual package it depends on
        dependency['name'] = dependency['package']

      info = {}
      info['type'] = "rust-crate"
      info['name'] = dependency['name']

      info['id'] = self.idFor(identity, occamType = "rust-crate",
                                        occamName = info['name'],
                                        crateName = dependency['name'],
                                        sourceURL = self.sourceURLFor(dependency['name']))

      info['version'] = self.__reformVersion(dependency['req']) + " || " + self.__reformVersion(dependency['req']) + ".x"
      info['replace'] = 'never'

      objectInfo["dependencies"].append(info)
      ingestRequests.append({
        "packageType": info['type'],
        "packageName": info['name'],
        "version": info['version']
      })

    return objectInfo, {
      "files": [
        [".cargo-checksum.json", '{"files":{},"package":"' + checksum + '"}']
      ],
      "ingest": ingestRequests,
      "tag": version
    }

  def report(self, identity, runType, options, objectInfo, crateName, data, runReport, pending = None, version = None):
    """ Finishes the process.
    """

    return newObjectInfo, report

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """
