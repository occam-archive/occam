import sys

try:
  import pytoml
except ImportError:
  exit(0)

import json

if len(sys.argv) > 2 and sys.argv[2] == "--ignore-setup-dependencies":
  exit(0)

path = sys.argv[1]

data = {}
with open(path, 'rb') as f:
  data = pytoml.load(f)

if 'build-system' in data:
  if 'requires' in data['build-system']:
    with open("_output", 'w+') as f:
      f.write(json.dumps({
        "setup_requires": data['build-system']['requires']
      }))
