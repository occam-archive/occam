import sys, os

ret = []

with open(sys.argv[2]) as f:
  for line in f:
    if line.startswith("Requires-Dist:"):
      line = line[14:].strip()
      line = line.replace('(', '');
      line = line.replace(')', '');
      ret.append(line)

import json
print(json.dumps(ret))
