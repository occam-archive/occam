# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager

from occam.manager import uses

from occam.git_repository import GitRepository
from occam.semver import Semver

import uuid as UUID
import os, re, json

@package("ruby-gem")
@uses(ObjectManager)
@uses(NetworkManager)
class RubyGem:
  """ This aids in importing ruby gems into Occam for preservation.
  """

  RUBYGEMS_IDENTITY = ""

  METADATA_URL = "https://rubygems.org/api/v2/rubygems/%s/versions/%s.json"
  VERSIONS_URL = "https://rubygems.org/api/v1/versions/%s.json"
  DOWNLOAD_URL = "https://rubygems.org/downloads/%s-%s.gem"

  CANONICAL_SOURCE = "https://rubygems.org/gems/%s"

  def __reformVersion(self, version):
    """ Converts a ruby version to an Occam version.

    Arguments:
      version (str): The ruby version.

    Returns:
      str: The corresponding Occam version.
    """

    # Strip whitespace
    version = version.strip()

    # Convert '~>' to either '~1.2.3' or '^1.2' depending on the number of
    # values.
    if version.startswith("~>"):
      if version.count(".") >= 2:
        version = "~" + version[2:]
      else:
        version = "^" + version[2:]

    return version.replace('+', '.')

  def __queryGemfile(self, identity, lockfile, gemfile=None):
    """ Retrieves all referenced crates in the given Gemfile.lock.

    Optionally, can refer to a Gemfile file as well.

    Arguments:
      identity (str): The identity URI of the ingesting actor.
      lockfile (str): The path to the Gemfile.lock to parse.
      gemfile (str): The path to the Gemfile to also parse.

    Returns:
      list: A set of metadata referencing the ingested objects.
    """

    import toml

    ret = []
    data = {}
    info = {}
    members = []

    with open(lockfile, 'r') as f:
      data = toml.load(f)

    if tomlfile:
      with open(tomlfile, 'r') as f:
        info = toml.load(f)

      members = info.get('workspace', {}).get('members', [])

    for package in data.get('package', []):
      if package['name'] in members:
        continue

      ret.append({
        'name': package['name'],
        'version': package['version']
      })

      # Ok, now present the occam dependency
      sourceURL = self.sourceURLFor(package['name'])
      id = self.idFor(identity, occamType = "rust-crate",
                                occamName = package['name'],
                                gemName = package['name'],
                                sourceURL = sourceURL)
      RubyGem.Log.output('{')
      RubyGem.Log.output(f'  "name": "{package["name"]}",')
      RubyGem.Log.output(f'  "type": "rust-crate",')
      RubyGem.Log.output(f'  "id": "{id}",')
      RubyGem.Log.output(f'  "version": "{self.__reformVersion(package["version"])}",')
      RubyGem.Log.output(f'  "replace": "never"')
      RubyGem.Log.output('},')

    return ret

  def __queryAll(self):
    """ Retrieves a listing of all packages.
    """

    RubyGem.Log.write(f"Parsing crate listing at {RubyGem.INDEX_SOURCE}")

    ret = []

    # The crate index is a git repository we need to clone
    #data, content_type, size = self.network.get(RubyGem.INDEX_SOURCE)

    #PythonLibrary.Log.writePercentage("Parsing...")

    #position = 0
    #for line in data.readlines():
    #  position += len(line)
    #  PythonLibrary.Log.updatePercentage(position / size * 100)

    #  line = line.strip()
    #  if line.startswith(b'<a'):
    #    name = line.split(b'>', 1)[1].split(b'<', 1)[0].decode('utf-8')
    #    ret.append(name)

    #PythonLibrary.Log.updatePercentage(100)

    return ret

  def __queryMetadata(self, gemName, version=None, rubyVersion=None):
    """ Retrieves the JSON gem metadata for the given gem that matches the given version and ruby version.

    Returns:
      None when the data cannot be found.
    """

    if version is not None:
      version = self.__reformVersion(version)

    # Get the metadata listing
    return self.__retrieveMetadata(gemName, version = version,
                                            rubyVersion = rubyVersion)

  def __retrieveMetadata(self, gemName, version=None, rubyVersion=None):
    """ Retrieves the JSON gem metadata for the given gem at the given version.

    Returns:
      None when the data cannot be found.
    """

    # Look up the metadata from RubyGems

    # First, determine the appropriate version
    url = RubyGem.VERSIONS_URL % gemName

    RubyGem.Log.noisy(f"Pulling metadata at {url}")

    data = []
    try:
      data = self.network.getJSON(url)
    except Exception as e:
      ret = None

    chosen = None
    for gem in data:
      # Pull out the metadata
      gemVersion = gem.get('number')
      gemRubyVersion = gem.get('ruby_version', rubyVersion) or rubyVersion

      # Compare to what is requested
      if not rubyVersion or Semver.resolveVersion(gemRubyVersion, [rubyVersion]):
        if not version or Semver.resolveVersion(version, [gemVersion]):
          chosen = gem
          break

    if chosen is None:
      return None

    # Then retrieve that metadata
    gemVersion = chosen.get('number')

    url = RubyGem.METADATA_URL % (gemName, gemVersion)

    try:
      data = self.network.getJSON(url)
    except Exception as e:
      data = None

    return data

  def sourceURLFor(self, gemName):
    """ Returns the canonical source url.
    """

    sourceURL = RubyGem.CANONICAL_SOURCE % (gemName.lower())

    return sourceURL

  def uidFor(self, occamType, occamName, gemName, sourceURL):
    """ Returns the uid for a particular source package name.
    """

    return self.objects.uidFor(None, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def idFor(self, identity, occamType, occamName, gemName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.idFor(None, identity, objectInfo = {
      "name": occamName,
      "type": occamType,
      "source": sourceURL
    })

  def queryAll(self, identity, lockfile=None, gemfile=None):
    """ Returns a list of all crates.
    """

    if lockfile:
      return self.__queryGemfile(identity, lockfile, gemfile)

    return self.__queryAll()

  def query(self, identity, gemName, pending=None, version=None, rubyVersion=None):
    """ Returns the id and initial metadata for the gem.
    """

    # Retrieve metadata
    metadata = self.__queryMetadata(gemName, version = version,
                                             rubyVersion = rubyVersion)

    print(metadata)

    # Get the canonical name
    name = metadata['name']
    version = metadata['version']

    # Get the canonical URL
    sourceURL = self.sourceURLFor(name)

    # Ensure the authors tag is a list
    if isinstance(metadata.get('authors'), str):
      metadata['authors'] = [metadata['authors']]

    # Now we generate the object metadata
    objectInfo = {
      "name": name,
      "type": "ruby-gem",
      "source": sourceURL,
      "authors": metadata.get('authors', []),
      "summary": metadata.get('info', metadata.get('description', "A ruby library.")),
      "license": metadata.get('licenses', []),
      "website": metadata.get('project_uri', metadata.get('homepage_uri', sourceURL)),
      "environment": "linux",
      "architecture": "x86-64",
      "dependencies": [
        {
          "name": "ruby",
          "type": "language",
          "id": "QmNXGtyAbWFBX4SAy5eZG5KdJy2kkiwZWTfrK68ttKA9dy",
          "version": f"{rubyVersion}.x"
        },
        {
          "name": "xz",
          "type": "library",
          "id": "QmQ7FqKWNy1cEHg9FjQH4WfFRnzcyHQ3ijwDWoBZ1SxaQK",
          "version": "5.x"
        },
        {
          "id": "QmPxP6yHYWbSA3z3sFVQVp67xUmAmKAD7kLUCc7j3JJjvi",
          "type": "library",
          "name": "png",
          "version": "1.x"
        },
        {
          "name": "RubyGems",
          "type": "application",
          "id": "Qmewvy1uovYsc1AgAKs99K1ghNxTam7cMAnHtqTp7AvGFD",
          "version": "x",
          "subversion": rubyVersion
        }
      ],
      "build": {
        "install": [
          {
            "type": "resource",
            "subtype": "application/x-tar+gem",
            "source": RubyGem.DOWNLOAD_URL % (name, version),
            "to": f"{name}-{version}.gem",
            "name": f"{name} {version} RubyGems Distribution"
          }
        ],
        "dependencies": [
          {
            "type": "collection",
            "name": "build",
            "id": "QmQ1i5VjdxdU7dWCkhpM7ccVDCBNPX2swTLQSHRiCW8sK1",
            "version": "1.0"
          },
          {
            "type": "application",
            "name": "unzip",
            "id": "QmPvYt44E95j78i7MSi3rJrMZ6K3Jyj677Yg3Q8wruEcKE",
            "version": "6.x"
          },
          {
            "id": "QmSPK9zLkxwtcj2QsrZNVz3sXH5zwR2nAaBiWVSUcXg64S",
            "version": "x",
            "name": "tzdata",
            "type": "data"
          },
          {
            "type": "compiler",
            "name": "g++",
            "id": "QmVseooVZ4dn2Vo4ikwKXzmBcqDH9avt4YWYtKmujP4KC8",
            "version": ">5",
            "inject": "ignore"
          },
          {
            "name": "patch",
            "type": "application",
            "id": "QmcPFABNUb4zCpdKizXxxcr7cbSQLxgwkCxbZW1NKBUTau",
            "version": "2.x"
          },
          {
            "name": "git",
            "type": "application",
            "id": "QmSvghqXxVtuJWoRJ5KUEr5dECKkJMMj4A12w8horEwhtZ",
            "version": "2.x"
          },
          {
            "name": "gettext",
            "type": "collection",
            "id": "QmQGjhRYdiZUNTprxVbuJjub1zP3jN2SfKNE837bK8qcZK",
            "version": "0.19.x"
          },
          {
            "name": "autoconf",
            "id": "QmPs1dypAif2WGKT4LND2dBY8yDCKv5eDbhBwikzq1k4AM",
            "type": "collection",
            "version": "2.x"
          },
          {
            "name": "automake",
            "type": "collection",
            "id": "QmReFYohsdWmkbY5wUUWRpR2XCkfC479xfWF97W2LMUgpy",
            "version": "1.x"
          },
          {
            "name": "libtool",
            "type": "application",
            "id": "QmfBE1Mny7X1QQQmn8tp6RirGsog9ZJGNoaMeMdEMP2XVn",
            "version": "2.x"
          }
        ],
        "command": [
          "/bin/bash", "build.sh", name, version
        ]
      },
      "init": {
        "link": [
          {
            "source": "usr/bin",
            "to": "/usr/bin"
          },
          {
            "source": "usr/share",
            "to": "/usr/share"
          }
        ],
        "copy": [
          {
            "source": "usr/lib",
            "to": "/usr/lib"
          }
        ]
      }
    }

    # Reform the version number
    version = self.__reformVersion(metadata['version'])

    id = self.idFor(identity, occamType = "ruby-gem",
                              occamName = name,
                              gemName = name,
                              sourceURL = sourceURL)

    uid = self.uidFor(occamType = "ruby-gem",
                      occamName = name,
                      gemName = name,
                      sourceURL = sourceURL)

    RubyGem.Log.write("uid: {}".format(uid))
    RubyGem.Log.write(" id: {}".format(id))

    ingestRequests = []

    # Gather dependencies
    for dependency in metadata.get('dependencies', {}).get('runtime', []):
      info = {}
      info['type'] = "ruby-gem"
      info['name'] = dependency['name']

      info['id'] = self.idFor(identity, occamType = "ruby-gem",
                                        occamName = info['name'],
                                        gemName = dependency['name'],
                                        sourceURL = self.sourceURLFor(dependency['name']))

      info['version'] = self.__reformVersion(dependency['requirements'])
      info['subversion'] = rubyVersion

      objectInfo["dependencies"].append(info)
      ingestRequests.append({
        "packageType": info['type'],
        "packageName": info['name'],
        "version": dependency['requirements'],
        "rubyVersion": rubyVersion
      })

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__)))
    buildScriptPath = os.path.join(basePath, "build.sh")

    return objectInfo, {
      "files": [
        buildScriptPath
      ],
      "build": True,
      "ingest": ingestRequests,
      "tag": version + "@" + rubyVersion
    }

  def report(self, identity, runType, options, objectInfo, gemName, data, runReport, pending=None, version=None, rubyVersion=None):
    """ Finishes the process.
    """

    if runReport.get('runReport', {}).get('status') == "failed":
      # We do not accept failure, here
      return None

    if runType == "build":
      # We are done
      return None

    return None

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """
