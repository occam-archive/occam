# JavaScript (npm) Package Ingest

This plugin ingests packages from the
[npm registry](https://registry.npmjs.org/) compatible with the
[`npm`](https://docs.npmjs.com/about-npm) package manager or any tools also
compatible with `npm` creating objects of the type `js-package`.

## Invoking

The command takes a package name and, optionally, a version.

```
occam ingest js-package object.omit 2.0.1
```

Which will return the object created by the currently logged in account. It will
recursively pull in any object that is a dependency of the one specified.

You can specify the object lockfile to create a dependency list:

```
occam ingest js-package - --lockfile=./package-lock.json
```

This will print to standard out the Occam metadata for the dependencies.

And also pull in any object not found already to satisfy the package listing
provided by this file. Further in this document is a thorough explanation of
what this file contains and how Occam uses it.

And, of course, we can pull in ***every*** package in the `npm` registry via:

```
occam ingest js-package -
```

## JavaScript packages

Within the node ecosystem, a project can have a set of packages described and
installed as part of that project independent of any other on the same system.
This is managed with metadata in the form of a JSON document in a file called
`package.json` typically at the root of the project's file layout. A program,
typically `npm`, is used to parse that metadata and satisfy the dependencies
within and place them, typically, in a directory off of that root called
`node_modules`. The program, then, creates a file called `package-lock.json`
which contains a listing of the satisfied modules within the `node_modules`
directory. This differs from the original `package.json` document as it
contains specific versions and lists packages that were dependencies of those
in the original listing, but not themselves listed there.

In this case, the `package-lock.json` document is the "frozen" listing of
versions of packages that are known to work. Often, and as best practice, this
file is made part of the source distribution for a project. As such, `npm` or
any similar program will satisfy using that file instead of the more generic
`package.json` to provide, in theory, the exact same `node_modules` subpath for
anybody who checks out the source listing at the same point in time. This is a
useful feature for preservation, and it is one we will make use of in our
preservation context.

The `package.json` contains, also, descriptive (human-readable) metadata for the
purpose of catagorizing and discovery such as the name and description of the
package, its license, a webpage, etc. The dependencies and certain metadata that
denote commands to run and such are also present in the more normative parts of
the document, such as `dependencies`, `devDependencies`, `scripts`, and `main`.
The specification for this metadata can be found on the
[npm documentation](https://docs.npmjs.com/cli/v7/configuring-npm/package-json)
website.

## `npm`

The `npm` application is the typical package management utility in service for
JavaScript projects today. It serves as a tool to generate and parse the
`package.json` and/or `package-lock.json` documents as mentioned above.

The tool is designed to assume that a network exists, which is not particularly
useful in our preservation context. However, the tool has several means of
caching such requests and can be somewhat bluntly pushed to not make network
requests. (Although, in some cases, it lovingly just hangs if there is no
network and it wants some metadata from somewhere.) We will describe, here, that
caching mechanism at a high-level and how the preservationist will make use of
it.

The `npm` tool has much of its functionality split into smaller reusable
packages. In this case, the
[`pacote`](https://www.npmjs.com/package/pacote),
[`cacache`](https://www.npmjs.com/package/cacache),
and [`make-fetch-happen`](https://www.npmjs.com/package/make-fetch-happen)
libraries are the main trio that handle and cache requests issued to the network
on behalf of `npm`. Whenever `npm` receives data, such as the binary
distribution of a package, the `pacote` and `cacache` libraries will hash and
store it in a "content-addressable" cache (hence the name `cacache`). Such a
cache stores files by their hash (SHA1 or SHA512 in most cases I have found)
within a directory known to `npm`. Whenever it is about to retrieve some binary
data with a pre-known hash, it will look in that cache first. The
`package-lock.json` file contains such hashes alongside the locked in version
information. This gives the `npm` application enough information to pull down
and verify the exact library versions and, thus, `node_modules` directory
for each and every person.

For the preservation case, this means we can act as the cache manager and
capture such binary data serving as the distributions of packages and place them
in the way of `npm` such that it does not request them from the network. In this
scheme, we store such data in our normal repository and load them into the
virtual filesystems as needed by objects in the system. This requires no change
to `npm` and indeed works for every normal case that has a `package-lock.json`.
When a project does not preserve its `package-lock.json`, you will need to
generate one from `npm` on the local system and place it there yourself when
wrapping the archived artifact.

### Content-Address Cache

As mentioned, `npm` will satisfy data requests by looking in its
content-addressed storage first before downloading from the network if it knows
of the hash of that remote file. Such a hash is located in the
`package-lock.json` file, for instance, and also located in package metadata.

The hash is calculated as the SHA1 or SHA512 hash (other hashes are technically
possible). This hash is then transformed into a hexstring and written as that
string to the local filesystem. The normal place for the cache is located in
`$HOME/.npm/_cacache`. This is then split into directories based on the type
of request. We are most concerned with the `content-v2` path which contains
binary data. The hex string is split to create subdirectories in order to not
saturate a single directory with subdirectories or files by using the first two
and then third and fourth digit. This, itself, is placed in a subdirectory based
off of the digest type (sha1, sha512, etc). For instance the SHA1 hex string:

```
1a9c744829f39dbb858c76ca3579ae2a54ebd1fa
```

would be placed in:

```
$HOME/.npm/_cacache/content-v2/sha1/1a/9c/744829f39dbb858c76ca3579ae2a54ebd1fa
```

Which would then be the path of the file the represents the binary data of a
request made in the past. In this case, it is the tarball for the `object.omit`
package: `https://registry.npmjs.org/object.omit/-/object.omit-2.0.1.tgz`.

The `package-lock.json` file or package metadata received from the registry both
contain hashes alongside the URLs of tarballs and other binary data related to a
package. In the `package-lock.json` file, for instance, the hash is found in the
`integrity` field of any module description alongside the URL found in the
`resolved` field. The value of the `integrity` contains the hash type followed
by, confusingly, the base64 encoded hash. The hash we saw before is encoded this
way as such (as part of a much larger `package-lock.json`):

```
  "node_modules/object.omit": {
    "version": "2.0.1",
    "resolved": "https://registry.npmjs.org/object.omit/-/object.omit-2.0.1.tgz",
    "integrity": "sha1-Gpx0SCnznbuFjHbKNXmuKlTr0fo="
  }
```

So the sha1 hash `Gpx0SCnznbuFjHbKNXmuKlTr0fo=` is equivalent to the hex string
`1a9c744829f39dbb858c76ca3579ae2a54ebd1fa`. We can verify this with the command
line:

```
$ echo "Gpx0SCnznbuFjHbKNXmuKlTr0fo=" | base64 -d - | xxd -p
1a9c744829f39dbb858c76ca3579ae2a54ebd1fa
```

And if we want the SHA hash for a particular file, we can compute that
similarly (with file `FILENAME`):

```
$ openssl dgst -sha1 -binary FILENAME | xxd -ps
1a9c744829f39dbb858c76ca3579ae2a54ebd1fa
```

And SHA512 with (the output will be 128 characters long):

```
$ openssl dgst -sha512 -binary FILENAME | xxd -ps -c 129
cdeabc84a84862186210e8b18670c75f24f467f9a0...long...aef76a352b7069f84040af6e
```

So we can recreate the above `object.omit` caching by downloading it:

```
wget https://registry.npmjs.org/object.omit/-/object.omit-2.0.1.tgz
```

And then creating the hash:

```
$ openssl dgst -sha1 -binary object.omit-2.0.1.tgz | xxd -ps
1a9c744829f39dbb858c76ca3579ae2a54ebd1fa
```

And then moving the file to the cache path of your choosing:

```
mkdir -p $HOME/my-cache/_cacache/content-v2/1a/9c
mv object.omit-2.0.1.tgz $HOME/my-cache/_cacache/content-v2/1a/9c/744829f39dbb858c76ca3579ae2a54ebd1fa
```

And now we can point `npm` to this cache using `--cache=$HOME/my-cache`.

## Wrapping

In Occam, JavaScript (npm) packages are stored such that when they are placed
within the virtual environment, they copy their binary distribution to the
content-addressed cache (`cacache`) path that can be used with `npm`.

In our case, it is the path `/opt/npm/cache`. In "Occam-aware" projects, this
cache can be forced upon the normal default using the `--cache` argument. You
can also elect to force the cache to always be used (force off network access)
by using the `--cache-min=Infinity` argument. This will sometimes hang the
process if it actually needs the network in some cases, so it may not be
advisable since containers have no network normally and will fail quickly
anyway.

At any rate, a simple package would be translated into the Occam object
metadata below.. The resource is retrieved and hashed by Occam independently and
then the SHA1 hash (in this case) is generated and used in the `init` section to
provide the file into the virtual machine. This file is copied and not
linked since `npm` cannot handle a symlink in the cache properly, although
that would be nice in our case to prevent the need to copy or overlay the files
from our global data store.

Since we are copying the data, we do not need to mount the resource as part of
the object data. So we provide `mount` equal to `ignore` in the install section
which prevents the files from being considered part of the mounted object. We
will also mark dependencies as `mount` equal to `ignore` to prevent them from
being mounted at all since the only files that are part of a JavaScript package
is the binary distribution data. JavaScript packages will very commonly use
hundreds if not over a thousand packages to build themselves. This means the
`mount` options, here, can shave considerable time off task deploys and
potentially allow invocations of, say, Docker or Apptainer (Singularity) where
a long list of command line options would not be allowed. It is quite dramatic
how many packages things will need. Absurd, even.

```
{
  "name": "object.omit",
  "type": "js-package",
  ...
  "dependencies": [
    {
      "type": "js-package",
      "name": "for-own",
      "id": "QmWLLT7WrjypwAg91X2esgh7h43FRJLssRiE7uxt1WMFk4",
      "version": "^0.1.4",
      "replace": "never"
      "mount": "ignore"
    }
  ],
  "install": [
    {
      "type": "resource",
      "subtype": "application/gzip",
      "source": "https://registry.npmjs.org/object.omit/-/object.omit-2.0.1.tgz",
      "to": "object.omit-2.0.1.tgz",
      "name": "object.omit 2.0.1 Distribution",
      "mount": "ignore"
    }
  ],
  "init": {
    "copy": [
      {
        "source": "object.omit-2.0.1.tgz",
        "to": "/opt/npm/cache/content-v2/sha1/1a/9c/744829f39dbb858c76ca3579ae2a54ebd1fa"
      }
    ]
  }
}
```

In this case, we can create a project that has a build phase that includes
these libraries alongside `npm` and any other typical applications required to
build the project. Then, our build script would contain something along the
lines of:

```
#!/bin/bash
# build.sh

npm install --cache=/opt/npm/cache --no-audit --no-fund
```

Which will hopefully allow `npm` to install or build the project without the
use of the network assuming the libraries are populated in `/opt/npm/cache`.

This `install` subcommand of `npm` will populate `node_modules` which should
mean that every other command or script in the project's `package.json` should
also work. Such as the typical build command:

```
npm run build
```
