# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, json

from datetime import datetime

from occam.log import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.export.manager      import ExportManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager

@command('export', 'workflow',
         category='Export Container(s)',
    documentation = "Exports the given workflow/experiment to the given output directory.")
@argument("workflow", type="object", help = "The identifier for the job you wish to export.")
@argument("output_dir", type=str, help = "The directory to write the output to.")
@option("-l", "--log", action = "store_true",
                       dest   = "write_to_log",
                       help   = "Writes job output to a log file.")
@uses(ExportManager)
@uses(ObjectManager)
@uses(ManifestManager)
@uses(JobManager)
class WorkflowCommand:
    def do(self):
        experiment = self.objects.resolve(self.options.workflow, person = self.person)

        if experiment is None:
          Log.error("cannot find workflow with id %s" % (self.options.workflow.id))
          return -1
        
        info = self.objects.infoFor(experiment)
        workflow = None
        if info.get('type') == "workflow":
          workflow   = experiment
          experiment = None
        elif info.get('type') == "experiment":
          for i, item in enumerate(info.get('contains', [])):
            if item.get('type') == "workflow":
              self.options.workflow.index = [i]
              workflow = self.objects.resolve(self.options.workflow, person = self.person)

        if workflow is None:
          Log.error("given object %s is not a workflow" % (self.options.workflow.id))
          return -1
        
        ret = self.export.launch(workflow, outputDir = self.options.output_dir, person = self.person)

        return 0


