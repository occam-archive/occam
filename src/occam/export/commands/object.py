# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, json

from datetime import datetime

from occam.log import Log
from occam.object import Object

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.export.manager      import ExportManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager

@command('export', 'object',
         category='Export Container(s)',
    documentation = "Exports the given object to the given output directory.")
@argument("job_id", type="object", help = "The identifier for the job you wish to export.")
@argument("output_dir", type=str, help = "The directory to write the output to.")
@option("-l", "--log", action = "store_true",
                       dest   = "write_to_log",
                       help   = "Writes job output to a log file.")
@uses(ExportManager)
@uses(ObjectManager)
@uses(ManifestManager)
@uses(JobManager)
class ObjectCommand:
  """ Immediately runs the given job.
  """

  def do(self):
    local = True
    path = "."
    obj = None

    if self.options.job_id:
      obj = self.objects.resolve(self.options.job_id, person=self.person)
      path = obj.path
      if obj is None:
        Log.error("Cannot determine what this object is.")
        return 125

      id = obj.id

    if obj is None:
      obj = Object(path = path)
      id = self.objects.tokenFor(obj)

    if path != os.path.realpath(os.curdir):
      local = False

    revision = obj.revision

    task = None

    # If this is running a staged object, we don't need to re-create the task if the
    # object metadata has not changed.
    token = None
    cachedToken = None
    taskPath = None
    tokenPath = None
    if obj.link != None:
      import hashlib

      digest = hashlib.sha256()

      if os.path.exists(os.path.join(obj.root.path, "..", "metadata")):
        # Look for a cached task
        taskPath = os.path.join(obj.root.path, "..", "metadata", obj.id + ".json")
        tokenPath = os.path.join(obj.root.path, "..", "metadata", obj.id + ".token")

        if os.path.exists(tokenPath):
          with open(tokenPath, "r") as f:
            cachedToken = f.read()

        stream = self.objects.retrieveFileFrom(obj, "object.json")
        digest.update(stream.read())
        token = digest.hexdigest()

    while task is None:
      try:
        task = self.manifests.run(object      = obj,
                                  id          = id,
                                  revision    = revision,
                                  local       = local,
                                  person      = self.person)
        break
      except BuildRequiredError as e:
        def build(object, local):
          while True:
            try:
              info = self.objects.infoFor(object)
              Log.write("Building %s %s" % (info.get('type', 'object'), info.get('name', 'unknown')))
              if 'build' not in info:
                ownerInfo = self.objects.ownerInfoFor(object)
                if 'build' in ownerInfo:
                  object = self.objects.ownerFor(object, person = self.person)
              buildTask = self.manifests.build(object    = object,
                                               uuid      = object.uuid,
                                               revision  = object.revision,
                                               local     = local,
                                               #arguments = self.options.arguments,
                                               person    = self.person)
              if buildTask is None:
                Log.error("Build task could not be generated.")
                return -1
              # Deploy the task as a job
              job = self.jobs.deploy(self.objects.infoFor(buildTask), revision=buildTask.revision, local=local, person = self.person)
              if job is None or job != 0:
                Log.error("Build failed.")
                return -1
            except BuildRequiredError as e:
              job = build(e.requiredObject, False)
        job = build(e.requiredObject, False)


    if task is None:
      Log.error("Could not generate a task for %s" % (obj.id))
      return 125

    if obj.link != None:
      # If the object.json has changed, remember the task
      if cachedToken != token and taskPath:
        with open(taskPath, "w+") as f:
          json.dump({'id': task.id, 'revision': task.revision, 'uid': task.uid}, f)

        with open(tokenPath, "w+") as f:
          f.write(token)

        # Invalidate the cached task
        tokenLocalPath = os.path.join(obj.root.path, "..", "tasks", obj.id)
        if os.path.exists(tokenLocalPath):
          # Delete the tree, which will force the new task to reinstantiate
          import shutil
          shutil.rmtree(tokenLocalPath)

    cwd = None
    opts = self.jobs.deploy(self.objects.infoFor(task),
                            revision    = task.revision,
                            person      = self.person,
                            cwd         = cwd,
                            local       = local)
    ret = self.export.execute(*opts, outputDir = self.options.output_dir, ignoreStdin = True)
    # for runSection in ret['runReport'].get('phases', []):
    #   opts = self.jobs.deploy(self.objects.infoFor(task),
    #                          revision    = task.revision,
    #                          runSection  = runSection,
    #                          uuid        = ret['id'],
    #                          person      = self.person,
    #                          cwd         = cwd,
    #                          local       = local)
    #   ret = self.export.execute(*opts, ignoreStdin = True)

    return 0
