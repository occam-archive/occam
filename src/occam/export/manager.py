# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime
import time
import json

from pathlib import Path

from occam.log    import loggable
from occam.object import Object
from occam.error  import OccamError
from occam.config import Config

from occam.manager import uses, manager

from occam.objects.manager import ObjectManager
from occam.databases.manager     import DatabaseManager, DataNotUniqueError
from occam.links.write_manager   import LinkWriteManager
from occam.system.manager        import SystemManager
from occam.jobs.manager import JobManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.workflows.manager import WorkflowManager
from occam.network.manager import NetworkManager
from occam.accounts.manager import AccountManager

from occam.workflows.datatypes.workflow import Workflow
from occam.workflows.datatypes.node import Node
@loggable
@manager("export")
@uses(ObjectManager)
@uses(DatabaseManager)
@uses(LinkWriteManager)
@uses(SystemManager)
@uses(ManifestManager)
@uses(WorkflowManager)
@uses(NetworkManager)
@uses(AccountManager)
@uses(JobManager)
class ExportManager:
  """
  """
# Keeps track of the Export plugins loaded into the system
  handlers = {}
  instantiated = {}

  def __init__(self):
    """ Initialize the export manager.
    """

    #import occam.Exports.plugins.docker
    import occam.export.plugins.singularity

    self.handlers = ExportManager.handlers

  @staticmethod
  def register(ExportName, handlerClass):
    """ Adds a new Export type.
    """

    ExportManager.handlers[ExportName] = { 'class': handlerClass }

  def isAvailable(self, ExportName):
    """ Returns True when the Export given is available on this node.
    """

    return ExportName in self.handlers

  def available(self):
    """ Returns a list of Export names that are available on this node.
    """

    ret = []
    for k, v in ExportManager.handlers.items():
      handler = None
      try:
        handler = self.handlerFor(k)
      except:
        handler = None

      if handler:
        ret.append(k)

    return ret
  
  def configurationFor(self, ExportName):
    """ Returns the configuration for the given Export.

    This is found within the occam configuration (config.yml) under the
    "Exports" section under the given plugin name.
    """

    config = self.configuration
    subConfig = config.get(ExportName, {})

    return subConfig

  def handlerFor(self, ExportName):
    """ Returns an instance of a handler for the given type.
    """

    if not ExportName in ExportManager.handlers:
      raise ExportPluginMissingError(ExportName)

    if not ExportName in ExportManager.instantiated:
      # Pull the configuration (from the 'Exports' subpath and keyed by the name)
      subConfig = self.configurationFor(ExportName)

      # Get the storage path for this Export
      occamPath = os.path.join(self.occamPath, ExportName)

      # Create a driver instance
      instance = ExportManager.handlers[ExportName]['class'](subConfig, occamPath)

      # If there is a problem detecting the Export, cancel
      # This will set the value in the instantiations to None
      try:
        if not instance.detect():
          instance = None
      except:
        instance = None

      # Set the instance
      ExportManager.instantiated[ExportName] = instance

    if ExportManager.instantiated.get(ExportName) is None:
      # The driver could not be initialized
      raise ExportPluginInitializationError(ExportName)

    return ExportManager.instantiated[ExportName]

  def handlerCall(self, ExportName, name, *args, **kwargs):
    handler = self.handlerFor(ExportName)

    if not hasattr(handler, name) or \
       not callable(getattr(handler, name)) or \
       not hasattr(getattr(handler, name), 'registered') or \
       not getattr(handler, name).registered:
      raise ExportPluginUnimplementedError(ExportName, name)

    return getattr(handler, name)(*args, **kwargs)

  def initialize(self):
    """ Initializes the Export plugins.
    """

    from occam.services.write_manager import ServiceWriteManager
    services = ServiceWriteManager()

    ret = {}
    for k, v in ExportManager.handlers.items():
      info = self.handlerCall(k, 'initialize')
      if not info is None:
        ret[k] = info

      # Pull out services so we have a record that they exist
      result = self.handlerCall(k, 'services')
      for serviceInfo in result:
        services.storeExport(Export = k, service = serviceInfo.get('name'))

    return ret

  def export(self, task, outputDir, paths=[], network = None, stdout = None, stdin = None, workflow = False):
    """ Runs the object represented by the given task.

    Each runnable object has a 'run' section in its metadata. This lists
    everything the ManifestManager needs to build a task for the environment
    needed to run the object.

    This function will take a task and, based on its Export, run that object.

    The run will be marked in an invocation record along with information about
    the runtime or any failures. Other invocations of this object on other
    machines may use that information to make decisions about how to best run
    the object in the future.
    """

    ExportManager.Log.noisy("Running with %s" % (task.get('backend')))
    backend = task.get('backend')
    return self.handlerCall(backend, 'run', task, outputDir, paths, network, stdout, stdin, workflow)
    
  def execute(self, task, paths, network, stdout, outputDir, ignoreStdin = False, workflow = False):
    # Keep track of date
    date = datetime.datetime.utcnow().isoformat()

    taskPath = paths['home']

    # Get stdin stream
    stdin = None
    elapsed = time.perf_counter()
    backendExitCode = self.export(task, outputDir, paths, network, stdout, stdin, workflow)
    elapsed = time.perf_counter() - elapsed

    self.uninstallNetwork(network)

    self.cleanupTaskOutputFilenames(task, taskPath)

    return {
      'time': elapsed,
      'date': date,
      'code': 0,
      'runReport': None,
      'machineInfo': self.system.machineInfo(),
      'occamInfo': self.system.localInfo(),
      'paths': paths,
    }
  
  def cleanupTaskOutputFilenames(self, task, path, root=None):
    """ Rename the output files to indicate which phase it belonged to.
    """

    if root is None:
      root = task

    # Move stdout
    # Look at running task
    runningObject = root.get('runs', root.get('builds'))
    runningTaskLocal = os.path.join(path, "task", "objects", str(runningObject['index']))

    stdoutPath = os.path.join(runningTaskLocal, "stdout")
    stderrPath = os.path.join(runningTaskLocal, "stderr")

    # Rename the stdout and stderr files to differentiate different run outputs.
    for oldPath in [stdoutPath, stderrPath]:
      if os.path.exists(oldPath):
        i = 0
        while True:
          newPath = oldPath + ".%s" % (str(i))
          i += 1

          if not os.path.exists(newPath):
            break

        os.rename(oldPath, newPath)



  def uninstallNetwork(self, network):
    """ Unallocates resources allocated by installNetwork.
    """

    for portInfo in network.get('ports', []):
      if 'port' in portInfo:
        systemPort = portInfo['port']
        JobManager.Log.write("freeing system port %s" % (systemPort))
        self.network.freePort(systemPort)

  def launch(self, object, outputDir, person=None):
    """ Launches the given object.

    This will run the object in the appropriate environment, as determined by
    the object's metadata.
    """

    # Get the task for this object
    workflow = self.workflows.createWorkflow(object, person = person)

    tasks = []
    nodes = []
    visited_nodes = []
    init_nodes = self.workflows.getInitialNodes(workflow)

    nodes.append(init_nodes)

    candidate_nodes = []

    for node in init_nodes:
      for index, output in enumerate([node[0].self, *node[0].outputs]):
        index = index - 1
        for output_node_to in node[0].followOutputWire(index):
          node_idx = output_node_to[0]
          if node_idx not in visited_nodes:
            visited_nodes.append(node_idx)
            node = workflow.nodes[node_idx]
            candidate_nodes.append((node, output_node_to[1],))

    nodes.append(candidate_nodes)

    runs = self.workflows.runsForWorkflow(object, person = person, revision = object.revision)

    run = runs[-1]

    job = self.jobs.retrieve(job_id = run.job_id)

    for node in nodes:
      task = self.workflows.generateNextTasks(nodes       = node,
                                   finishedJob = job[0],
                                   run         = run,
                                   person      = person)
    
      tasks.append(task)

      # for n, t in task:
      #   job = self.jobs.queue(task = t, person = person, interactive = False)

      #   self.jobs.run(job, person = person)
    
    ret = []
    for item in tasks:
      for node, task in item:
        taskInfo = self.objects.infoFor(task)

        job = self.jobs.deploy(taskInfo, revision=task.revision, local=False, person = person)

        temp_ret = self.execute(*job, outputDir = outputDir, ignoreStdin = True, workflow = True)

        ret.append(temp_ret)

    return ret

class ExportError(OccamError):
  """ Base class for all export errors.
  """

class ExportPluginError(ExportError):
  """ Base class for all export plugin errors.
  """

class ExportPluginMissingError(ExportPluginError):
  """ Thrown when a plugin does not exist.
  """

  def __init__(self, ExportName):
    super().__init__(f"Export provider {ExportName} not known")

class ExportPluginInterfaceUnknownError(ExportPluginError):
  """ Thrown when a plugin has an interface implementation we are not aware of.
  """

  def __init__(self, ExportName, funcName):
    super().__init__(f"Export provider {ExportName} found but is not compatible due to the presence of unknown interface function {funcName}.")

class ExportPluginInitializationError(ExportPluginError):
  """ Thrown when a plugin reported a problem when initializing.
  """

  def __init__(self, ExportName):
    super().__init__(f"Export provider {ExportName} found but could not be initialized")

class ExportPluginUnimplementedError(ExportPluginError):
  """ Thrown when a method within a plugin is not implemented.
  """

  def __init__(self, ExportName, method):
    super().__init__(f"Export provider {ExportName} does not implement {method}")

  
# Plugin Registry

def export(name, priority=0):
  """ This decorator will register the given class as an export.
  """

  def register_export(cls):
    ExportManager.register(name, cls)
    cls = loggable("ExportManager")(cls)

    def init(self, subConfig, occamPath):
      self.configuration = subConfig
      self.occamPath = occamPath

    cls.__init__ = init
    return cls

  return register_export

def interface(func):
  """ This decorator assigns the given function as part of the plugin.

  Only functions tagged with this decorator can be used as part of a plugin
  implementation.
  """

  # Attach it to the plugin
  def _pluginCall(self, *args, **kwargs):
    return func(self, *args, **kwargs)

  _pluginCall.registered = True
  return _pluginCall