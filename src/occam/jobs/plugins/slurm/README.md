# Slurm Scheduling Plugin

## Usage

Occam supports both centralized and distributed installations. On a centralized installation of Occam, the Occam (default) scheduler is used to schedule jobs in order to run workflows and objects for a user. Jobs queued by the Occam scheduler are found and run by the job daemon, and are limited to running locally. For running Occam on a distributed system, Occam currently supports [Slurm](https://slurm.schedmd.com/overview.html). Slurm support requires minor configuration changes.

To enable Slurm support, Slurm must be installed and configured so that it can be used by the user running the Occam daemon / commands. All compute nodes to be used for running Occam jobs must also be configured so that Occam is installed, and the Occam daemon is running. The $OCCAM\_ROOT/config.yml must also be changed so that Slurm is set as the default adapter. The Occam instance on the compute node must be able to reach the coordinator (not just the Slurm controller!).

On compute nodes, be sure the following steps have taken place:
  * Install Occam and any software it depends on.
  * Run `occam system intialize`.
  * In the Occam configuration, set the `host` to "0.0.0.0" or the IP assigned to the coordinator visible network adapter.
  * Set `externalHost` to the hostname by which Slurm knows the node.
  * Create an `occam` user (`occam accounts new occam`) and log in (`occam accounts login occam`). There should be an auth.json under `$OCCAM_ROOT` after this step.
  * Launched the Occam daemon as a Slurm job. This should not be done via `interact` because `interact` will time out after a period of no input. The following is an example sbatch command to run the daemon on a reservation called "cc5pifp": `sbatch -C EGRESS –partition RM-shared –reservation cc5pifp -t 0 –wrap=“source ~/.bashrc; occam daemon run”`. Note the `-t 0`. This means the job should run indefinitely. However, effectively this will only run for the life of the reservation (plus whatever overrun is allowed by the Slurm configuration).

### Scheduler Configuration

The following is an example of what the jobs section of the configuration on the coordinator should look like to enable Slurm support.

```
jobs:
  schedulers:
    defaultAdapter: 'slurm'
    occam:
    slurm:
      partition: 'myslurmpartition'
      #reservation: 'myreservation'
      #nodelist: 'slurm-node-[1-3,5],slurm-vm0[1-2]'
      #sbatchAdditionalArgs: ["-C", "EGRESS"]
```

The above snippet tells Occam to use Slurm as the scheduler, and to run jobs on the partition `myslurmpartition`. Which should be changed to the name of the Slurm partition on which Occam is expected to run. If Occam should run on a specific reservation of the partition, the reservation line should be uncommented and `myreservation` should be set to the name of the Slurm reservation made for Occam. Note that the reservation and partition listed should match up. This means that the reservation specified should be a reservation on the partition configured.

If Occam should only submit jobs to a subset of the partition or reservation, the nodelist should be uncommented and set to a valid Slurm nodelist specification (as allowed by the [sbatch command](https://slurm.schedmd.com/sbatch.html)'s `--nodelist` flag) which defines the preferred nodelist. If the job won't fit within the nodelist, such as if the job demands more nodes than are found in the nodelist, then additional resources outside of the nodelist may be allocated by Slurm.

The above example tells Occam to use Slurm for all job submissions, but you can also run ad-hoc commands that specify the scheduler to use. For example, the command `occam jobs new --scheduler slurm TASK_ID` will launch a task with the Slurm scheduler regardless of the configured default.

Once configured, Occam will submit jobs to the Slurm cluster in a way that is transparent to the user. Note that there may be longer wait times between telling a job to run and getting its results due to Slurm controller load or post-job triggers that may be configured in Slurm.

Note that all of these options can be configured per target in the target's configuration section as shown below.

### Target Configuration

Slurm may be used alongside other targets on the system. Below is an example of a configuration that uses the local machine's Occam scheduler as a default, but allows users to target the Slurm cluster if they choose.

```
targets:
  default:
    name: Occam Server
    scheduler: occam
    host: localhost

  my-compute-cluster:
    name: Slurm Cluster
    scheduler: slurm
    host: localhost
    configuration:
      slurmLogDir: '/dev/null'
      scriptSetup:
        - 'export OCCAM_ROOT=${SLURM_SCRATCH}/.occam'
        - 'export OCCAMPATH=/zfs2/occam/occam-dev.cs.pitt.edu/occam'
        - 'export LD_LIBRARY_PATH=${OCCAMPATH}/vendor/lib64:${OCCAMPATH}/vendor/lib:${LD_LIBRARY_PATH}'
        - 'export GOPATH=${HOME}/go'
        - 'export PATH=${OCCAMPATH}/vendor/bin:${OCCAMPATH}/bin:/usr/local/go/bin:${PATH}'
        - 'export PYTHONPATH='
        - '. /ihome/occam/occam/.bashrc'
        - 'module load singularity/3.5.3'
    provides:
      - environment: singularity
        architecture: x86-64
```

Slurm could also have been made the default. What the Slurm target in the example does is prevent Slurm from generating logs (to prevent filling up any directories), sets up the compute node environment  needed to execute the commands in the sbatch script successfully, and declares what environments the Slurm cluster supports (for Occam to determine if jobs can be run in this environment). In this case it supports Singularity, but not Docker.

## Design

### Overview

The `JobManager` has a plugin system to support existing schedulers with an interface to create a queued job mapped to the 'job' record Occam is maintaining. A 'job' record is made for an individual 'task' object in the system, which denotes a running process and lists the dependencies and backend required to run that task. That external scheduler does all the work determining where and when that task runs.

### Job Plugin Interface

The Job plugin has the following functions.

### Slurm Interaction

A 'job' is a metadata wrapper around running a task. In order to run a task on a compute node, the objects within that task need to be migrated to the compute node chosen. The overall process, then, requires an initialization step and a finalize step which will migrate new data back to the storage system. It is as follows:

* Initialization (pulling required objects to the compute node)
* Runtime (actual execution)
* Finalize (pull new data and metadata away from the ephemeral compute node)

This will be done by creating Slurm jobs for each of these steps that are dependent on each other such that they execute in the above order.

Occam will create a Job record with "slurm" as the scheduler. This will have a job id, hereby known as `main-job-id`.

The initialization step will simply pull the basic task. After it does so, it pings the coordinator to tell it its local job id.

Then it will pull the task objects using the `--pull-task` option. This may take awhile depending on how many of the objects are already resident on the compute node.

```
srun <slurm options> occam objects pull http://<daemon web host>:<daemon web port>/<task id> -t <access token for coordinator>
JOB_ID=`srun <slurm options> occam jobs new <task id> --scheduler null`
srun <slurm options> occam jobs report ${JOB_ID} <main-job-id> occam://<daemon host>:<daemon port> 
srun <slurm options> occam objects pull http://<daemon web host>:<daemon web port>/<task id> -t <access token for coordinator> --pull-task
```

After these steps it will go to the "run" phase where the job properly "starts". It will ping the coordinator just before moving to the actual run. This report command will actually "start" the job; that is, it will assign a start time.

The runtime will simply run that task using Occam by taking that local job with no scheduler and deploying that job. The "null" scheduler has no implementation and thus never pops the job off the queue. It then calls the `jobs run` command to invoke the job so it is monitored as a normal Slurm task.

```
srun <slurm options> occam jobs report ${JOB_ID} <main-job-id> occam://<daemon host>:<daemon port> --status started
srun <slurm options> occam jobs run ${JOB_ID}
```

The finalize step will tell the coordinating node what it should pull. The `occam://` URI will send occam commands to that daemon directly. This offers the ability to use a web URL in the future.

```
srun <slurm options> occam jobs report ${JOB_ID} <main-job-id> occam://<daemon host>:<daemon port>
```

### Finalize

The `jobs report` command will tell a coordinator node that a job has completed. It does this via a very light command `jobs ping <job id> <compute node job id> occam://<compute node host>:<compute node port>` where the latter argument is a URI yielding the host and port and signifies it uses the Occam protocol.

It is the coordinator's role at this point to query information about that completed job, which that coordinator may itself schedule at it's own discretion in a work queue.

It is to-be-discussed what information is sent to the coordinator during the initial `report` and what can be queried and how *by* the coordinator. It makes some sense to send as little information to the coordinator as possible and just have it "ping" and then allow the coordinator all the time it wants to retrieve the information. The coordinator's resources are somewhat more limited.

The basic idea is that the coordinator will certainly do a `jobs status` and `jobs view` on that local job id, which will be reported to the coordinator by the compute node in the `jobs report` command. The coordinator will supplant its own job status and output with that reported by the compute node essentially duplicating the information provided by that compute node. The `jobs status` command should report the object outputs for that job. The coordinator will pull those (generally before updating the jobs status so that it appears atomic.)

The compute node, after it has sent the information, becomes truly ephemeral. The coordinator knows that once it has received the information and duplicated the metadata from that compute node, it may power it down.

### In-progress Reporting

Slurm can monitor the life of its own jobs. When the local system sees a `jobs status`, it can query Slurm (via Slurm command `squeue`) to provide details of whether or not it is queued or running or finished. To aid in this, the Slurm compute node will "ping" the coordinator whenever the status of the job changes.

The `job connect` command attaches to the IO of a running job, which is equivalent to the Slurm `sattach` command. This allows the viewing of jobs as they are running and the ability to control interactive jobs.

The `job view` prints the output of a job. This output will be transferred during the finalize phase. In-progress jobs will have to pull the output either via `jobs connect` described above or by pass-through by calling `jobs view <node-job-id>` on the compute node's Occam daemon. This requires some way of determining that daemon host and port which can be known to Slurm after it completes the initialization phase since it will know the address of the compute node. This pass-through happens within the Slurm plugin within the function that is meant to view ongoing job output.

### Job Failure

Upon a failed Slurm task, the coordinator should resubmit the entire dependency chain such that it runs on a different node. Although, for now, it will be reasonable to fail the job outright and have the human at the other end resubmit the job.

If the object itself fails naturally, then the finalize step will report that to the coordinator. When the coordinator pulls the information as described in "Finalize" above, it will naturally update its job metadata with that of the failed job.
