# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import math
import re
import copy
import json
import base64

from occam.config     import Config
from occam.log        import loggable
from occam.key_parser import KeyParser

from occam.manager import manager, uses

from occam.configurations.compiler import Compiler
from occam.configurations.permutor import Permutor

from occam.objects.write_manager import ObjectWriteManager
from occam.links.write_manager   import LinkWriteManager
from occam.permissions.manager   import PermissionManager

# TODO: handle unknown variables
# TODO: handle variables assigned twice

@loggable
@uses(ObjectWriteManager)
@uses(LinkWriteManager)
@uses(PermissionManager)
@manager("configurations")
class ConfigurationManager:
  """ This OCCAM manager handles configurations including modification and validation.
  """

  def schemaFor(self, obj, index = None, key = None, person = None):
    """ Returns the configuration schema for the given object.
    """

    info = self.objects.infoFor(obj)

    schemaFile = None
    if index is not None:
      inputs = info.get('inputs', [])

      configurationData = None
      if index >= 0 and index < len(inputs):
        configurationData = inputs[index]

      if configurationData and configurationData.get('type') == "configuration":
        schemaFile = configurationData.get('schema')
    else:
      schemaFile = info.get('schema')

    schema = None
    if schemaFile:
      schemaObject = obj

      if 'id' in schemaFile:
        # Pull schema object
        schemaObject = self.objects.retrieve(id = schemaFile['id'], revision = schemaFile['revision'], person = person)
        schemaFile = schemaFile['file']

      schema = self.objects.retrieveJSONFrom(schemaObject, schemaFile)

    if schema and key:
      schema = self.schemaItem(schema, key)

    return schema

  def schemaItem(self, schema, key):
    """ Returns the schema item within the schema for the given key.
    """

    parser = KeyParser()
    # Get the schema information for the key
    if key in schema:
        if "\." not in key:
            key = "\.".join(key.split("."))
    
    parts = parser.parseKeyParts(key)
    for subKey in parts:
      subKey, arrayParts = parser.parseArrayParts(subKey)

      schema = schema[subKey]
      if len(arrayParts) > 0:
        # Is this a tuple/array?
        if schema["type"] == "tuple":
          schema = schema["elements"]

          for indexKey in arrayParts:
            schema = schema[int(indexKey)]
        elif schema["type"] == "array":
          schema = schema["element"]

    return schema

  def createDefaultFor(self, obj, index, person = None):
    """ Creates a default configuration for the given object and person.

    Arguments:
      obj (Object): The object the configuration is for.
      index (int): The index of the input wire for the configuration.
      person (Person): The person requesting the configuration.

    Returns:
      Object: The configuration default object.
    """

    index = int(index)

    default = self.defaultFor(obj, index, person = person)

    # Create a new default configuration if there is not one already
    if default is None:
      # This means create an object for the configuration and then creating a
      # local link.

      # Determine if we are looking for a system default or an account default
      identity = None
      if person is not None:
        identity = person.identity

      # Get the object information
      info = self.objects.infoFor(obj)
      objName = info.get('name', 'unknown')

      # Initial configuration is empty
      data = {}

      new_obj = None

      # Create a configuration object
      object_info = {}

      input_metadata = info["inputs"][index]

      if input_metadata["type"] != "configuration":
        # Not a configuration input!
        return None

      object_info["subtype"] = input_metadata.get("subtype", ["application/json"])
      object_info["file"]    = input_metadata.get("file", "data.json")
      object_info["schema"]  = {
        "file": input_metadata["schema"],
        "revision": obj.revision,
        "id": obj.id,
        "uid": obj.uid,
        "name": info.get('name', 'unknown'),
        "type": info.get('type', 'object')
      }

      name = "Default Configuration for %s" % (objName)
      new_obj = self.objects.write.create(name = name, object_type = "configuration", info = object_info, root = None, path = None)

      # Store the configuration object globally
      self.objects.write.store(new_obj, identity)

      # Set initial permissions
      self.permissions.update(new_obj.id, canRead = False,
                                          canWrite = False,
                                          canClone = False)

      if identity is not None:
        self.permissions.update(new_obj.id, identity = identity,
                                            canRead = True,
                                            canWrite=True,
                                            canClone=True)

      # Now create a tracked link
      link = None
      if identity is not None:
        db_objects = self.objects.search(id = new_obj.id)
        if len(db_objects) == 0:
          ConfigurationManager.Log.error("No record of this object found.")
          return None

        db_object = db_objects[0]

        # Create the actual local link
        link = self.links.write.createLocalLink(db_object, revision = new_obj.revision,
                                                           path = None)

        # Initialize the object at the link path
        self.links.write.initializeStage(new_obj, link.path)

      # Then store the default configuration record
      self.datastore.createDefaultConfiguration(id = obj.id,
                                                targetObjectId = new_obj.id,
                                                identity = identity,
                                                index = index,
                                                link = link.id)

      # Craft the configuration object to return
      default = self.objects.retrieve(id = new_obj.id,
                                      link = link.id,
                                      person = person)

      # Add the default configuration files
      self.objects.write.addFileTo(default, object_info["file"], json.dumps(data))

    return default

  def defaultFor(self, obj, index, person = None):
    """ Returns the object representing the default for the given person.

    Arguments:
      obj (Object): The object the configuration is for.
      index (int): The index of the input wire for the configuration.
      person (Person): The person requesting the configuration.

    Returns:
      Object: The configuration default object or None if it does not exist.
    """

    # Determine if we are looking for a system default or an account default
    identity = None
    if person is not None:
      identity = person.identity

    # Get the default record, if it exists
    default = self.datastore.retrieveDefaultConfiguration(id = obj.id,
                                                          index = index,
                                                          identity = identity)

    ret = None
    if default:
      ret = self.objects.retrieve(id = default.target_object_id,
                                  link = default.link,
                                  person = person)

    # Retrieve a link attached object for the given tracked link
    return ret

  def defaultsFor(self, schema):
    """ Returns the data representing the default values.
    """

    ret = None

    # Go through the schema and generate a data structure with the defaults
    if self.isItem(schema):
      # A configuration item (not a group)
      if schema["type"] == "tuple":
        # Tuple
        ret = []
        for item in schema.get('elements', []):
          ret.append(self.defaultsFor(item))
      elif "default" in schema:
        ret = schema["default"]

        # Ensure the given type, just in case
        try:
          if schema["type"] == "int":
            ret = int(ret)
          elif schema["type"] == "float":
            ret = float(ret)
          elif schema["type"] == "string":
            ret = str(ret)
        except:
          # If the type is invalid, then the parameter is nulled
          ret = None

        # Optional values do not need a value
        if schema.get('optional'):
          ret = None
    elif self.isGroup(schema):
      # Group
      ret = {}
      for k, v in schema.items():
        if not isinstance(v, dict):
          # These are keys describing the group and not items
          # (This is pedantic... these should be resolved anyway since
          # the recursive call will fail each item check in the ifs
          # above)
          continue

        # Recursively call for each key
        value = self.defaultsFor(v)

        # When the value is None, there is no default.
        if value is not None:
          ret[k] = value

    return ret

  def filterSchema(self, schema, values):
    """
    """

    ret = {}

    for k,v in values.items():
      check = schema

      parts = k.split('.')

      for part in parts:
        key, *indices = part.split('[')

        if key in check:
          check = check[key]
        else:
          check = None

      if check:
        ret[k] = check

    return ret

  def removeDefaultsFrom(self, data, schema):
    """ Returns a new dictionary containing only values that are not defaults.
    """

    # Get the defaults from the schema
    defaults = self.defaultsFor(schema)

    # Remove any data in the data block that is equal to a default
    def filterOutDefaults(data, schema, defaults):
      ret = {}
      for k,v in data.items():
        if defaults.get(k) != data.get(k):
          if k in defaults and isinstance(v, dict) and isinstance(defaults[k], dict):
            ret[k] = filterOutDefaults(v, schema[k], defaults[k])
          else:
            ret[k] = data[k]

      return ret

    return filterOutDefaults(data, schema, defaults)

  def dataFor(self, obj):
    """ Returns the configuration data for the given configuration object.
    """

    info = self.objects.infoFor(obj)

    filename = info.get('file')
    ret = None
    if filename:
      ret = self.objects.retrieveJSONFrom(obj, filename)

    return ret

  def set(self, key, value, data, schema):
    """ Returns updated configuration data to set the given key to the given value.
    """

    #if key exists in schema escape .
    if key in schema:
        if "\." not in key:
            key = "\.".join(key.split("."))
    # If it is a tuple, we need to set it to the default first (if it is not set)
    # Validate the key
    validated, message = self.validate(key, value, data, schema)

    if not validated:
      return None, message

    # If validated, message can actually come with a replacement value:
    #  e.g. value: "on" => True or "0.3" => 0.3
    value = message

    parser = KeyParser()

    # Get the value from the parent of the key
    chunk, subKey = parser.getParent(data, key, create = True)

    # Set the key
    if isinstance(chunk, list) and isinstance(subKey, int):
      while len(chunk) <= subKey:
        chunk.append(None)

    if value is None:
      # Delete the key, which might set it to the default again
      value = self.delete(key, data, schema)
    else:
      # Otherwise set it
      chunk[subKey] = value

    # Return the value (and no error message)
    return value, None

  def delete(self, key, data, schema):
    """ Deletes the given key from the configuration data.
    """

    parser = KeyParser()

    if key in schema:
        if "\." not in key:
            key = "\.".join(key.split("."))

    # Get the value from the parent of the key
    chunk, subKey = parser.getParent(data, key)

    description = self.schemaItem(schema, key)
    default = self.defaultsFor(description)

    # Set the key
    if subKey:
      if default:
        chunk[subKey] = default
      else:
        try:
          del chunk[subKey]
        except:
          pass

    # Return the data
    return data

  def get(self, key, data, schema):
    """ Returns the value for the given key and data.
    """

    parser = KeyParser()
    return parser.get(data, key)

  def parseRangedValue(self, value):
    """ This returns an array of the ranged values.

    Returns a tuple of the form (var, locked, ranges,) where var is either None
    or a string containing the variable being assigned, locked is either None or
    a string containing the variable being iterated with, and ranges which is an
    array of tuples of the form (start, final, step,) where start is a string
    containing the initial value of the range, final is a string with the final
    value of the range, and step is a string containing an expression using "x"
    to indicate how the range increments from start to final.
    """
    if not isinstance(value, str):
      value = str(value)

    ret = []

    # var = locked ! start...end:step, value, start...end:step
    # Parse out any variable assignment
    var = None
    if "=" in value:
      var, value = value.split('=', 1)
      var   = var.strip()
      value = value.strip()

    # Parse out the iteration locked variable
    locked = None
    if "!" in value:
      locked, value = value.split('!', 1)
      locked = locked.strip()
      value  = value.strip()

    # Split on commas, and then split by ranges
    for section in value.split(','):
      parts = [x.strip() for x in section.split(':')]
      step = "x+1"
      if len(parts) > 1:
        section = parts[0]
        step = parts[1]

      span = [x.strip() for x in section.split('...')]
      start = span[0]
      if len(span) > 1:
        final = span[1]
      else:
        final = span[0]

      ret.append((start, final, step,))
    return (var, locked, ret,)

  def isGroup(self, schema):
    """ Returns True when the given schema is a configuration group.
    """

    return not self.isItem(schema) and isinstance(schema, dict)

  def isItem(self, schema):
    """ Returns True when the given schema is a configuration item.
    """

    return isinstance(schema, dict) and "type" in schema and not isinstance(schema["type"], dict)

  def rangedValuesFor(self, value, schema, values = None, nesting = ""):
    """ Returns a set of keys which contain ranged values.

    Configurations can have values which are ranged or contain a series of values.
    This will return the set of configuration options that involve ranges.

    Returns an array of tuples of the form: (full_key, [(start, end, step,), ...])
    """

    # Initialize the resulting list
    if values is None:
      values = []

    # Go through the value and schema together and look for ranges
    if self.isItem(schema):
      # A configuration item (not a group)
      if schema["type"] == "tuple":
        # Tuple

        # Go through each item in the tuple
        for index, item in enumerate(value or []):
          subNesting = "%s[%s]" % (nesting, index)
          values = self.rangedValuesFor(value   = item,
                                        schema  = schema["elements"][index],
                                        values  = values,
                                        nesting = subNesting)
      elif schema["type"] == "array":
        # Array
        # Go through each item in the array
        for index, item in enumerate(value or []):
          subNesting = "%s[%s]" % (nesting, index)
          values = self.rangedValuesFor(value   = item,
                                        schema  = schema["element"],
                                        values  = values,
                                        nesting = subNesting)
      elif schema["type"] != "color" and schema["type"] != "string" and not isinstance(schema["type"], list):
        # Everything else
        var, lockedWith, valueRanges = self.parseRangedValue(value)

        # If there are more than one value for this ranged value,
        # add it to the possible ranges
        if valueRanges:
          if not (len(valueRanges) == 1 and valueRanges[0][0] == valueRanges[0][1] and (schema["type"] == "boolean" or isinstance(schema["type"], list) or not re.match(r"^[^\d]\w*$", valueRanges[0][0]))):
            values.append((nesting, valueRanges, schema["type"], lockedWith, var, schema,))
      else:
        if isinstance(value, dict):
          # This is a list of strings to permute
          valueRanges = value.get('values', [])
          valueRanges = [(x, x, None,) for x in valueRanges]
          values.append((nesting, valueRanges, "string", value.get('with'), value.get('variable'), schema,))

    elif self.isGroup(schema):
      # Group
      ret = {}
      for k, v in schema.items():
        if not isinstance(v, dict):
          # These are keys describing the group and not items
          # (This is pedantic... these should be resolved anyway since
          # the recursive call will fail each item check in the ifs
          # above)
          continue

        if nesting == "":
          subNesting = k
        else:
          subNesting = "%s.%s" % (nesting, k)

        # Recursively call for each key
        if k in value:
          values = self.rangedValuesFor(value   = value[k],
                                        schema  = schema[k],
                                        values  = values,
                                        nesting = subNesting)

        # When the value is None, there is no default.
        if value is not None:
          ret[k] = value


    return values

  def resolveValue(self, value, valueType, variables, slot):
    """ Returns the resolved value for the given value and context.
    """

    # Parse the value for any variables
    tokenizer = Compiler.Tokenizer()
    tokens    = tokenizer.tokenize(value)

    lastToken = None
    variableName = None
    variableLocals = []
    for token in tokens + [None]:
      if (token is None or token != "(") and variableName is not None:
        # This is a variable (as opposed to a function call)
        if variableName not in variables:
          return (False, variableName,)

        variableLocals.append(variableName)
      else:
        variableName = None

      if lastToken == "identifier":
        variableName = token
      else:
        variableName = None

      lastToken = token

    # Look up the variables and replace them with the value

    values = []
    compilerLocals = {}
    for var in variableLocals:
      compilerLocals[var] = slot[variables[var][0]][0][0]

    result = value
    try:
      result = Compiler.execute(value, compilerLocals)
    except:
      pass

    # Finally, type check them
    result = self.typeCheck(result, valueType, {})
    values.append(result[1])

    return (True, values,)

  def iterate(self, start, final, step, valueType, schema, limit = 1000):
    """ Iterates based on the pattern and schema.

    The relationship between the schema, valueType, and the values given will
    yield a set of possible values that exist between the given start and final
    values. The `step` parameter is, possibly, a mathematical expression that
    evaluates using the `Compiler` class.

    This will provide at most `limit` iterations.

    Arguments:
      start (str): The configured start value.
      final (str): The configured final value.
      step (str): The configured step expression.
      valueType (str): The type for this parameter.
      schema (dict): The schema description for this parameter.
      limit (int): The maximum number of values to compute.

    Returns:
      list: The set of values that complete the expression.
    """

    if step is None:
      return [start]

    if valueType == "int":
      i = int(start)
      final = int(final)
    else:
      i = float(start)
      final = float(final)

    ret = []

    iterations = 0
    last = None

    while i <= final and iterations < limit:
      lastError = self.validate(None, i, None, None, schema)

      if last == i:
        break

      if lastError[0] == True:
        ret.append(i)

      last = i

      # Goto next item
      i = Compiler.execute(step, {'x': i})
      iterations += 1

    return ret

  def expandRangedValue(self, valueTuples):
    """ This returns an array of values for the given ranged value tuple.
    """

    # This is not the most efficient way of doing this. You could keep a list
    # of needed variables and cross-reference it and only add to 'needed'
    # when those requirements are finally met.

    # The first thing to do is sort by variable dependency

    # We keep a list of variables and point to their resolved ranges
    variables = {}

    # The configuration tuples that are post-poned
    needed    = valueTuples

    # The configuration tuples that are being considered
    left      = []

    # Each configuration is represented by a complicated data structure
    # There are a set of possible configuration passed
    # Each configuration pass has a series of dependent time slots, where
    #   values must happen at the same time
    # Each time slot has a section of time-dependent (iterative) values and
    #   a set of independent (permuted) values. The third section are the values
    #   of variables that are locked for this time slot.
    # The complete configuration space is an explosion of values where the
    #   product of the independent configuration space is permuted with
    #   each possible permutation key.
    configurationSpace = [ {} ]

    # The state of any all variables
    # This contains a list of values whose keys are the variable names
    # The value is a tuple: (configuration key, value type,)
    variables = {}

    # Go through and look at variables
    lastLength = 0

    while len(needed) > 0:
      if lastLength == len(needed):
        break

      lastLength = len(needed)
      left = needed
      needed = []
      for key, ranges, valueType, lockedWith, var, schema in left:
        complete = True

        if lockedWith and lockedWith not in variables:
          complete = False
          needed.append((key, ranges, valueType, lockedWith, var, schema,))
          continue
        for slot in configurationSpace:
          if key in slot:
            continue
          # Determine the values given the current slot
          # For each range
          valueCount = 0

          for start, final, step in ranges:
            complete, resolvedStart = self.resolveValue(start, valueType, variables, slot)

            if not complete:
              break

            complete, resolvedFinal = self.resolveValue(final, valueType, variables, slot)

            if not complete:
              break
            values = self.iterate(resolvedStart[0], resolvedFinal[0], step, valueType, schema)

            if values:
              if lockedWith:
                # We must permute this value with the locked value
                for value in values:
                  if slot[variables[lockedWith][0]][0][1] == valueCount:
                    slot[key] = [(value, valueCount,)]
                  valueCount += 1
              elif var:
                # We must prematurely permute this value
                if valueCount == 0:
                  slot[key] = [(values[0], valueCount,)]
                  values = values[1:]
                  valueCount += 1

                for value in values:
                  # Clone this slot
                  newVariation = slot.copy()
                  newVariation[key] = [(value, valueCount,)]
                  valueCount += 1
                  configurationSpace.append(newVariation)
              else:
                # We do not necessarily have to permute this value
                if not key in slot:
                  slot[key] = []
                slot[key].extend([(value, i,) for i,value in enumerate(values)])

            if not complete:
              break

          # Remember that the variable has been resolved
          if var:
            variables[var] = (key, valueType,)

        if not complete:
          needed.append((key, ranges, valueType, lockedWith, var,))

    return configurationSpace

  def permute(self, configurationSpace, values):
    """ Yields an iterator the produces a configuration given a permutation space.
    """

    return Permutor(configurationSpace, values)

  def typeCheck(self, value, valueType, typeInfo):
    """ Validates the current value against the type of an item.

    Returns a tuple consisting of the validation result, the type corrected
    value when validated correctly, or the validation message when the value
    does not validate.
    """

    # Type check
    if isinstance(valueType, list):
      # Array (enum)
      if not value in valueType:
        return [False, "must be one of the following: %s" % (valueType)]
    elif valueType == "int":
      # Integer valueType
      try:
        # If value is float and valueType is int, python would cast the integer part of the number
        # So, if 1.42 is said to be an int, then the value would be 1.
        if isinstance(value, float):
          return [False,"cannot cast float to integer"]
        value = int(value)
      except (ValueError, TypeError):
        return [False, "must be an integer"]
    elif valueType == "float":
      # Floating Point valueType
      try:
        value = float(value)
      except(ValueError, TypeError):
        return [False, "must be a float"]
    elif valueType == "boolean":
      # Boolean Type
      if not isinstance(value, bool):
        try:
          if value.lower() in ["true", "yes", "on"]:
            value = True
          elif value.lower() in ["false", "no", "off"]:
            value = False
          else:
            return [False, "must be a boolean (true/false): %s" % (value)]
        #If the user sends any number different than 0,1 it should raise this exception
        #Numbers dont have the function .lower()
        except(AttributeError):
          return [False, "must be a boolean (true/false): %s" % (value)]
    elif valueType == "tuple":
      elements = typeInfo.get("elements", [])
      if not isinstance(value, list):
        value = [value]

      if len(value) < len(elements):
        return [False, "does not have enough values. this field takes %s values" % (len(elements))]

      if len(value) > len(elements):
        return [False, "has too many values. this field takes %s values" % (len(elements))]

    return [True, value]

  def variablesIn(self, value, schema, variables=None):
    """ Returns a list of variables found within the configuration.
    """

    # Initialize return value
    if variables is None:
      variables = []

    if self.isItem(schema):
      # A configuration item (not a group)
      var = None
      rest = []
      if schema["type"] == "tuple":
        # Tuple
        #If value is ("something=another"), this is not a tuple and should be verified
        if not isinstance(value,tuple):
          value = (value,)
        if len(value) != len(schema.get('elements')):
          return "Value and schema.elements must be equal"
        for item, subschema in (zip(value,schema["elements"]) or []):
          #var, *rest  = str(item).split('=', 1)
          variables=self.variablesIn(item, subschema, variables)
      elif schema["type"] != "string":
        var, *rest = str(value).split('=', 1)

        if len(rest) > 0:
          variables.append(var.strip())
    elif self.isGroup(schema):
      for k, v in schema.items():
        if not isinstance(v, dict):
          # These are keys describing the group and not items
          # (This is pedantic... these should be resolved anyway since
          # the recursive call will fail each item check in the ifs
          # above)
          continue

        # Recursively call for each key
        if k in value:
          variables = self.variablesIn(value[k], schema[k], variables=variables)
    return variables

  def validate(self, key, value, data, schema, schemaItem = None):
    """ Validates the given configuration data against the given schema.

    Returns a tuple consisting of the validation result, any corrected
    value when validated correctly, or the validation message when the value
    does not validate.
    """

    parser = KeyParser()

    # Get the information about the key
    if schemaItem is None:
      info = schema
      # Get the schema information for the key
      parts = parser.parseKeyParts(key)
      for subKey in parts:
        subKey, arrayParts = parser.parseArrayParts(subKey)

        info = info[subKey]

        if len(arrayParts) > 0:
          # Is this a tuple/array?
          if info["type"] == "tuple":
            info = info["elements"]

            for indexKey in arrayParts:
              info = info[int(indexKey)]
          elif info["type"] == "array":
            info = info["element"]
    else:
      info = schemaItem

    # Determine if the value should be deleted since it is empty and optional
    if isinstance(value, str) and (value == "" or value == info.get('default')) and info.get('optional'):
      return [True, None]

    # Pull out ranged value, if ranged
    if isinstance(value, str) and info['type'] in ['int', 'float'] and (value.find('...', 1) >= 0 or value.find(',', 1) >= 0):
      ret = [False, None]
      # Split on commas, and then split by ranges

      var, lockedWith, values = self.parseRangedValue(value)
      if values is None:
        return [False, "Range is invalid."]

      for valueRange in values:
        start   = valueRange[0]
        final   = valueRange[1]
        step    = valueRange[2]

        if start.isalpha() or final.isalpha() or start =='' or final =='':
          return [False,"Wrong parameters. Range values should be numbers"]
        if int(start)<0 or int(final)<0:
          return [False,"Range values should be positive"]

        # Validate that there is a valid entry within the range
        iterations = 0

        i = int(start)
        passed = False
        lastError = [False, "Range does not include any items."]
        while i <= int(final) and iterations < 50:
          lastError = self.validate(key, i, data, schema, info)
          if lastError[0] == True:
            passed = True
            break

          i = Compiler.execute(step, {'x': i})
          iterations += 1

        ret[0] = passed

        if ret[0] == False:
          if start != final:
            ret[1] = "range does not include a valid value: " + lastError[1]
          return ret

      ret[1] = value
      return ret

    # Perform type checking for this item

    valueType = info.get('type', info)
    typeCheckValidation = self.typeCheck(value, valueType, info)

    if typeCheckValidation[0] == False:
      return [False, typeCheckValidation[1]]

    value = typeCheckValidation[1]

    if valueType == "tuple":
      # We must now validate each item of the tuple
      for i, item in enumerate(value):
        validates, message = self.validate(key + "[" + str(i) + "]", value[i], data, schema)
        if not validates:
          return [validates, "value[" + str(i) + "]: " + message]
    else:
      # Validations
      validations = info.get('validations') or []
      for validation in validations:
        if 'min' in validation:
          # Min test
          if valueType == "int":
            minimum = int(validation['min'])
          elif valueType == "float":
            minimum = float(validation['min'])

          if value < minimum:
            return [False, "must be at least %s" % (minimum)]

        if 'max' in validation:
          # Max test
          if valueType == "int":
            maximum = int(validation['max'])
          elif valueType == "float":
            maximum = float(validation['max'])

          if value > maximum:
            return [False, "must be at most %s" % (maximum)]

        if 'test' in validation:
          # Coded test
          if not Compiler.execute(validation['test'], {'x': value}):
            return [False, validation.get('message') or "did not validate %s" % (validation['test'])]

    return [True, value]

  def valuesFor(self, data, schema):
    """ Returns a list of tuples for any non-default values in a configuration.

    This will represent the difference between this and the default
    configuration set. Effectively, this presents the established values given
    by an actor.
    """

    # Get the configuration data

    # Get the schema

    # Filter out any inputs that are provided as default values

    # Reform, again, to a set of items

  def keyStringFor(self, values):
    """ Returns the stored normalized key string.

    This string is searchable and represents the entire set values for a
    configuration. It is likely in the form ';key=value;key=value;', etc,
    where each key and value is encoded in base64.

    Arguments:
      values (dict): A dictionary representing the configuration.

    Returns:
      str: The key string in the normalized form.
    """

    import base64

    def hashToList(values, prefix = None, ret = None):
      if ret is None:
        ret = []

      if isinstance(values, dict):
        for k,v in values.items():
          if prefix:
            k = prefix + "." + k

          hashToList(v, k, ret)
      elif isinstance(values, list):
        for i, item in enumerate(values):
          hashToList(item, prefix + "[" + str(i) + "]", ret)
      else:
        ret.append([prefix, values])

      return ret

    # Get the values in a list form
    values = hashToList(values)

    # Sort the keys in this list
    values = sorted(values, key=lambda x: x[0])

    # Return the encoded form
    return ';' + ';'.join(list(map(lambda x: base64.b64encode(x[0].encode('utf-8')).decode('utf-8').strip('=') + "=" + base64.b64encode(json.dumps(x[1]).encode('utf-8')).decode('utf-8').strip('='),
                             values))) + ';'

  def hashFor(self, values):
    """ Returns a hash representing the given configuration values.

    The values are, first, normalized and then hashed to a common digest format.

    This function will then return the hash in the form of a hex string.

    Arguments:
      values (list): A list of tuples for each key and value.

    Returns:
      str: The hash as a hex string.
    """

    # Get the hash string
    configuration = self.keyStringFor(values)

    # Compute the hash
    import hashlib
    digest = hashlib.sha256()
    digest.update(configuration.encode('utf-8'))

    # Return it as a string
    return digest.hexdigest()

  def lookupValues(self, obj, relationship=None,
                              index=-1,
                              keys=None,
                              values=None):
    # Encode keys
    if keys:
      keys = list(map(lambda x: base64.b64encode(x.encode('utf-8')).decode('utf-8').strip('='),
                      keys or []))

    # Encode values
    if values:
      values = list(map(lambda x: base64.b64encode(x[0].encode('utf-8')).decode('utf-8').strip('=') + "=" + base64.b64encode(x[1].encode('utf-8')).decode('utf-8').strip('='),
                        values or []))

    return self.datastore.retrieveConfigurationValues(obj.id, obj.revision, relationship = relationship,
                                                                            index = int(index),
                                                                            keys = keys,
                                                                            values = values)

  def lookupOutput(self, obj, relationship=None,
                              index=-1,
                              values=None):
    # Encode values
    if values:
      values = list(map(lambda x: base64.b64encode(x[0].encode('utf-8')).decode('utf-8').strip('=') + "=" + base64.b64encode(x[1].encode('utf-8')).decode('utf-8').strip('='),
                        values or []))

    return self.datastore.retrieveConfigurationOutputs(obj.id, obj.revision, relationship = relationship,
                                                                             index = int(index),
                                                                             values = values)
