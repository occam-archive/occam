# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.configurations.database import ConfigurationDatabase

@uses(ConfigurationDatabase)
@datastore("configurations.write", reader=ConfigurationDatabase)
class ConfigurationWriteDatabase:
  """ Manages the database interactions for writing new configuration metainfo.
  """

  def createConfigurationMapping(self, id, revision, baseId, baseRevision, inputIndex, configuration, hash):
    """ Creates a mapping between a set of values and an output.

    This is used for caching output based on input key/values and input object.

    Arguments:
      id (str): The id of the output object.
      revision (str): The revision of the output object.
      baseId (str): The id of the generating object.
      baseRevision (str): The revision of the generator object.
      inputIndex (int): The index of the input corresponding to the configuration.
      configuration (str): The encoded key string representing the set values.
      hash (str): The hashed representation of this configuration.

    Returns:
      ConfigurationMappingRecord: The new record.
    """

    from occam.configurations.records.configuration_mapping import ConfigurationMappingRecord

    session = self.database.session()

    record = ConfigurationMappingRecord()
    record.id = id
    record.revision = revision
    record.base_id = baseId
    record.base_revision = baseRevision
    record.inputIndex = int(inputIndex)
    record.configuration = configuration
    record.input_index = inputIndex
    record.hash = hash

    self.database.update(session, record)
    self.database.commit(session)

    return record
