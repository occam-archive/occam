# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager

@command('configurations', 'set',
  category      = 'Configuration Management',
  documentation = "Updates items within a configuration object.")
@argument("object",   type = "object",
                      help = "The configuration object to update.")
@option("-t", "--input-type",  action  = "store",
                               dest    = "input_type",
                               default = "text",
                               help    = "determines what encoding the new value is. defaults to 'text'. 'json' for JSON encoded values.")
@option("-i", "--item", action  = "append",
                        dest    = "items",
                        nargs   = "+",
                        default = [],
                        help    = "the key/value pair to set. If no value is given, the key is deleted or written to its default.")
@option("-d", "--default", dest   = "default",
                           action = "store",
                           help   = "when specified, this refers to the index of the wire and modifies the account default; object refers to the configured object")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@uses(ObjectWriteManager)
@uses(ConfigurationManager)
class SetCommand:
  def do(self):
    # Get the object to update
    obj = self.objects.resolve(self.options.object, person = self.person)

    if obj is None:
      Log.error("cannot find object with id %s" % (self.options.object.id))
      return -1

    # Pull out the schema, which will be stored within the configuration object
    schema = self.configurations.schemaFor(obj, person = self.person)

    if self.options.default is not None:
      if self.person is None:
        Log.error("Must be authenticated to store a default configuration")
        return 1

      info = self.objects.infoFor(obj)
      objName = info.get('name', 'unknown')
      Log.write("Updating default configuration for %s" % (objName))

      baseObject = obj

      # Pull out the default object

      schema = self.configurations.schemaFor(obj, int(self.options.default))

      obj = self.configurations.createDefaultFor(obj, int(self.options.default),
                                                      person = self.person)

      # Update the schema in the configuration object metadata
      info = self.objects.infoFor(obj)
      info['schema']['revision'] = baseObject.revision
      self.objects.write.updateObjectJSON(obj, self.person.identity, "object.json", info, "Updates Schema", False)

    # Create/use a mutable copy
    elif not self.options.object.id == "." and not self.options.object.link:
      # Clone a copy to edit the data, unless we are working from a local stage
      root, obj, objpath = self.objects.temporaryClone(obj, person = self.person)

    info = self.objects.infoFor(obj)
    objName = info.get('name', 'unknown')
    Log.write("Updating %s" % (objName))

    # Pull out the current data
    data = self.configurations.dataFor(obj)
    if data is None:
      # The data file doesn't exist in this object
      # We need to create it
      data = {}
      self.objects.write.addFileTo(obj, info["file"], json.dumps(data))

    # Pull out the defaults
    defaults = self.configurations.defaultsFor(obj)

    # For each key, set the configuration option
    for item in self.options.items:
      if self.options.input_type == "json":
        if len(item) == 1:
          Log.noisy("Clearing key %s" % item[0])
        else:
          if item[0].endswith("[]"):
            Log.noisy("Appending to key %s with json value" % item[0])
          else:
            Log.noisy("Setting key %s to json value" % item[0])

          try:
            item[1] = json.loads(item[1])
          except:
            Log.error("Could not decode the given json value: " + item[1])
            return -1

      key, *value = item
      if len(value) == 1:
        value = value[0]

      new_value = None
      error     = None
      if len(item) == 1:
        self.configurations.delete(key, data, schema)
      else:
        new_value, error = self.configurations.set(key, value, data, schema)

      if error:
        Log.error("Could not set %s: %s" % (key, error))
        return -1

    self.objects.write.addFileTo(obj, info["file"], json.dumps(data))

    if obj.link is None:
      self.objects.write.commit(obj, self.person.identity, message="Updates configuration files")

      # Store the updated configuration object
      self.objects.write.store(obj, self.person.identity)

    ret = {}
    ret["updated"] = []

    for x in (obj.roots or []):
      ret["updated"].append({
        "id": x.id,
        "uid": x.uid,
        "revision": x.revision,
        "position": x.position,
      })

    ret["updated"].append({
      "id": obj.id,
      "uid": obj.uid,
      "revision": obj.revision,
      "position": obj.position,
    })

    if self.options.to_json:
      Log.output(json.dumps(ret))
    else:
      Log.write("new object id: ", end ="")
      Log.output("%s" % (obj.id), padding="")
      Log.write("new object revision: ", end ="")
      Log.output("%s" % (obj.revision), padding="")

    Log.done("Successfully updated %s" % (self.objects.infoFor(obj).get('name')))
    return 0
