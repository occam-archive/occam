# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.associations.manager import AssociationManager
from occam.objects.manager      import ObjectManager

from occam.commands.manager import command, option, argument

@command('associations', 'list',
  category      = 'Association Management',
  documentation = "Lists known associations")
@argument("kind",   help = "the type of association. (viewer, editor, provider)")
@option("-m", "--major",       action  = "store",
                               dest    = "major",
                               help    = "the major matcher for this assocation, if desired (type or architecture)")
@option("-n", "--minor",       action  = "store",
                               dest    = "minor",
                               help    = "the minor matcher for this assocation, if desired (subtype or environment)")
@option("-s", "--system-wide", action  = "store_true",
                               dest    = "system_wide",
                               help    = "will only list default associations for the entire system")
@option("-f", "--full",        action  = "store_true",
                               dest    = "full",
                               help    = "will also pull out object metadata")
@uses(ObjectManager)
@uses(AssociationManager)
class AssociationListCommand:
  def do(self):
    person = self.person

    if self.options.system_wide:
      person = None

    rows = self.associations.retrieveAll(associationType = self.options.kind,
                                         major           = self.options.major,
                                         minor           = self.options.minor,
                                         identity        = person and person.identity,
                                         systemWide      = True,
                                         full            = self.options.full)

    ret = {}
    def updateRows(ret, row):
      if row.major not in ret:
        ret[row.major] = {}

      minor = '*'
      if row.minor:
        minor = row.minor

      if minor not in ret[row.major]:
        ret[row.major][minor] = []

      if self.options.full:
        ret[row.major][minor].append({
          "object": {
            "id": row.object.id,
            "uid": row.object.uid,
            "revision": row.revision,
            "owner": {
              "id": row.object.owner_id or row.object.id
            },
            "name": row.object.name,
            "type": row.object.object_type,
            "subtype": list(filter(None, row.object.subtype[1:-1].split(';'))),
            "tags": list(filter(None, row.object.tags[1:-1].split(';'))),
            "description": row.object.description,
            "environment": row.object.environment,
            "architecture": row.object.architecture,
            "organization": row.object.organization,
          },
          "kind": row.association_type,
          "default": row.identity_uri is None,
          "major": row.major,
          "minor": row.minor,
        })
      else:
        ret[row.major][minor].append({
          "object": {
            "id": row.internal_object_id,
            "revision": row.revision,
          },
          "kind": row.association_type,
          "default": row.identity_uri is None,
          "major": row.major,
          "minor": row.minor,
        })

    list(map(lambda row: updateRows(ret, row), rows))

    import json
    Log.output(json.dumps(ret))

    return 0
