# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.log    import Log

from occam.manager import uses

from occam.licenses.manager import LicenseManager

@command('licenses', 'list',
  category      = 'License Management',
  documentation = "Lists all known licenses")
@uses(LicenseManager)
class LicenseListCommand:
  def do(self):
    licenses = self.licenses.retrieveAll()

    Log.output(json.dumps(licenses))

    return 0
