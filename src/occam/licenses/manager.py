# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log     import loggable
from occam.manager import manager, uses

@loggable
@manager("licenses")
class LicenseManager:
  """ This OCCAM manager holds information about the possible license texts.
  """

  def __init__(self):
    self._licenses = None

  def retrieveAll(self):
    """ Retrieves a list of all known licenses.
    """

    if self._licenses is None:
      # Initialize license database
      import yaml
      try:
        self._licenses = yaml.safe_load(open(os.path.join(os.path.dirname(__file__), "data", "index.yml")))
      except:
        self._licenses = {}

    return self._licenses

  def retrieve(self, key):
    """ Retrieves information about a particular license that matches the given key.
    """

    ret = {}

    licensePath = os.path.join(os.path.dirname(__file__), "data")
    for info in self.retrieveAll():
      for tag in info['aliases']:
        if key.lower() == tag.lower():
          with open(os.path.join(licensePath, info['filename']), "r") as f:
            ret = info
            ret['fullName'] = info.get('fullName', info.get('name', tag))
            ret['name'] = tag
            ret['text'] = f.read()
          break
      if 'text' in ret:
        break

    return ret
