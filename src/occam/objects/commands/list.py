# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e  # defaults to listing "/"
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/ # same as above
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e/configs/0
#
# occam list dd44fcce-5274-11e5-b1d4-dc85debcef4e@abc123/configs/0

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.resources.manager import ResourceManager
from occam.discover.manager  import DiscoverManager

from occam.object import Object
from occam.log import Log

import os

@command('objects', 'list',
  category      = 'Object Inspection',
  documentation = "Displays a file listing within a path within the given object.")
@argument("object", type = "object", nargs = '?', default=".")
@option("-l", "--long",  dest   = "list_details",
                         action = "store_true",
                         help   = "lists the metadata for each file in a table listing")
@option("-a", "--all",   dest   = "list_all",
                         action = "store_true",
                         help   = "includes linked resources in the list")
@option("-j", "--json",  dest   = "to_json",
                         action = "store_true",
                         help   = "returns result as a json document")
@option("-b", "--build", dest   = "build_id",
                         action = "store",
                         help   = "When specified, lists the files within the given build.")
@option("-f", "--full",  dest   = "full",
                         action = "store_true",
                         help   = "When specified, duplicate items are also included.")
@uses(ObjectManager)
@uses(ResourceManager)
@uses(DiscoverManager)
class List:
  """ The list command will view a directory within a given object.
  """

  def do(self):
    # Get the object to list
    path = "/"
    data = None

    if self.options.object and not self.options.object.path is None:
      path = self.options.object.path

    path = os.path.normpath(path)

    obj = self.objects.resolve(self.options.object, person = self.person)

    discovered = False
    if obj is None:
      if self.options.object:
        data = self.discover.retrieveDirectory(self.options.object, person = self.person)
        discovered = True
      else:
        Log.error(key="occam.objects.errors.localObjectNotFound")
        return -1

      if data is None:
        if self.options.object.id == ".":
          Log.error(key="occam.objects.errors.localObjectNotFound")
        else:
          Log.error(key="occam.objects.errors.specifiedObjectNotFound", id=self.options.object.id)
        return -1

    # 'ls' the directory structure
    if data is None:
      data = {'items': []}

      # Lists resources as well
      # This will show the file listing as it would be seen when the resources are
      # installed into the object.
      try:
        data = self.objects.retrieveDirectoryFrom(obj, path,
                                                  includeResources = self.options.list_all,
                                                  person           = self.person,
                                                  buildId          = self.options.build_id)
      except IOError:
        Log.error(key="occam.objects.errors.pathNotFound", path=path)
        return -1

    if not discovered:
      pool = data['directory'].values()
      if self.options.full:
        pool = data['items']
    else:
      pool = data['items']

    if self.options.to_json:
      import json

      if self.options.list_details:
        Log.output(json.dumps(list(pool)))
      else:
        Log.output(json.dumps([item['name'] for item in pool]))

      return 0

    for i, item in enumerate(pool):
      if i > 0:
        Log.output("  ", end="", padding="")
      Log.output(item['name'], end="", padding="")

    Log.output("\n", end="", padding="")
    return 0
