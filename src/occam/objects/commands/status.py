# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager     import ObjectManager
from occam.versions.manager    import VersionManager
from occam.resources.manager   import ResourceManager
from occam.databases.manager   import DatabaseManager
from occam.permissions.manager import PermissionManager
from occam.accounts.manager    import AccountManager
from occam.discover.manager    import DiscoverManager
from occam.manifests.manager   import ManifestManager

from occam.semver import Semver

from occam.manager import uses

@command('objects', 'status',
  category      = 'Object Inspection',
  documentation = "Lists high-level information about the given object or file path.")
@argument("object", type = "object", nargs = '?')
@option("-a", "--all",      dest   = "list_all",
                            action = "store_true",
                            help   = "when object is a file path, includes all files within resources as well and will redirect to linked resources")
@option("-j", "--json",     dest   = "to_json",
                            action = "store_true",
                            help   = "returns result as a json document")
@option("-v", "--viewers",  dest   = "show_viewers",
                            action = "store_true",
                            help   = "includes a set of objects that would view this one")
@option("-d", "--discover", dest   = "discover",
                            action = "store_true",
                            help   = "query the federation when no result is found locally")
@option("-s", "--storage",  dest   = "show_storage",
                            action = "store_true",
                            help   = "adds information about the storage space taken up by this object")
@option("-r", "--resources", dest   = "show_resources",
                             action = "store_true",
                             help   = "adds resource status metadata for each resource in the object")
@uses(ObjectManager)     # For pulling the object
@uses(VersionManager)    # For pulling version information
@uses(ResourceManager)   # For pulling resource objects
@uses(DatabaseManager)   # For pulling out the normalized data
@uses(PermissionManager) # For pulling access control information
@uses(ManifestManager)   # For pulling task information
@uses(AccountManager)    # For determining authorship
@uses(DiscoverManager)
class ObjectsStatusCommand:
  """ This class handles listing the current knowledge about an object or a file within an object.

  For local paths when developing an object, this will show you the differences
  between the stored object and the current state of the object.
  """

  def reportStatus(self, obj):
    resource_object = None
    path = "/"
    ret = None
    id = None

    if obj is None:
      # See if it is a Resource Object
      if self.options.object and self.options.object.id:
        # Check id
        resource_object = self.resources.infoFor(id=self.options.object.id)

        # Check uid
        if not resource_object:
          resource_object = self.resources.infoFor(uid=self.options.object.id)

      # Last ditch effort: ask the federation
      if resource_object is None and self.options.object:
        if self.options.discover:
          if ret is None and self.options.object and self.options.object.path:
            ret = self.discover.retrieveFileStat(id = self.options.object.id,
                                                 revision = self.options.object.revision,
                                                 path = self.options.object.path,
                                                 person = self.person)
          else:
            ret = self.discover.status(id = self.options.object.id,
                                       revision = self.options.object.revision,
                                       person = self.person)

      elif resource_object is None:
        Log.error(key="occam.objects.errors.localObjectNotFound")
        return -1

      if resource_object is None and ret is None:
        Log.error(key="occam.objects.errors.specifiedObjectNotFound", id=self.options.object.id)
        return -1

    if ret is None and obj and self.options.object and self.options.object.path:
      # Return file stat
      id = obj.id
      ret = self.objects.retrieveFileStatFrom(obj, path             = self.options.object.path,
                                                   includeResources = self.options.list_all,
                                                   person           = self.person)
    elif ret is None and resource_object:
      # This is a Resource Object, so ret is the basic description
      ret = resource_object

      id = resource_object["id"]
    elif obj and ret is None:
      # A Version Object

      id = obj.id

      # Get the database knowledge of the object
      db_rows = self.objects.search(id=obj.id)
      storedRevision = None
      identity = None

      db_obj = None
      if db_rows:
        db_obj = db_rows[0]
        storedRevision = db_obj.revision
        identity = db_obj.identity_uri

      if db_obj is None:
        raise Exception("Cannot find the object in the database for id %s" % (obj.id))

      # Get authors/collaborators
      authors = self.accounts.authorsFor(obj.id)
      collaborators = self.accounts.collaboratorsFor(obj.id)

      # Get included objects
      included = []

      # Generate the result
      ret = {
        "id": obj.id,
        "uid": obj.uid,
        "identity": identity,
        "revision": obj.revision,
        "currentRevision": storedRevision,
        "includes": included,
        "trusted": self.permissions.isTrusted(obj, self.person),
        "authors": [x._data for x in authors],
        "collaborators": [x._data for x in collaborators]
      }

      owner_obj = obj

      if obj.owner_id != obj.id:
        ret["owner"] = {
          "id": obj.owner_id
        }
        owner_obj = self.objects.ownerFor(obj, self.person)

      versions = self.versions.retrieve(owner_obj)
      versionTags = list(set(map(lambda x: x.tag, versions)))
      versionTags = Semver.sortVersionList(versionTags)
      ret["sortedVersions"] = versionTags
      ret["versions"] = dict([(version.tag, version.revision,) for version in versions])

      # Pull out known partial tasks
      for section in ["run", "build"]:
        task, buildId, _ = self.manifests.retrievePartialTaskFor(owner_obj, section = section,
                                                                            person = self.person)
        if task:
          ret["task"] = ret.get('task', {})
          ret["task"][section] = {
            "id": task.id,
            "uid": task.uid,
            "identity": task.identity,
            "revision": task.revision
          }

          if buildId:
            ret["task"][section]["build"] = {
              "id": buildId
            }

      # If this is a Person, determine authorization credentials
      if db_obj.object_type == "person":
        db_account = self.accounts.retrieveAccount(identity = db_obj.identity_uri)
        if db_account:
          ret["account"] = {
            "username": db_account.username,
            "roles":    list(filter(None, (db_account.roles or ";;")[1:-1].split(';'))),
            "subscriptions": list(filter(None, (db_account.subscriptions or ";;")[1:-1].split(';'))),
          }

          # Account email will be included if it's public or if belongs to the
          # account that's logged in.
          showEmail = db_account.email_public or (
            self.person and self.person.id == db_account.person_id)
          if db_account.email and showEmail:
            ret["account"]["email"] = db_account.email

      # Handle index navigation (aka uuid@revision[1][2][0]
      #   will report "roots" with 3 objects, the root, the item at root[1]
      #   and the object at root[1][2])
      if obj.roots:
        ret["roots"] = []

        for subObject in obj.roots or []:
          subInfo = self.objects.infoFor(subObject)
          ret["roots"].append({
            "id": subObject.id,
            "uid": subObject.uid,
            "type": subInfo["type"],
            "name": subInfo["name"],
            "revision": subObject.revision})

    # Gather access permissions
    person_id = None
    if self.person and hasattr(self.person, 'id'):
      person_id = self.person.id

    mainAccess = self.permissions.retrieve(
        id     = id,
        person = self.person)

    records = self.permissions.retrieveAllRecords(
        id     = id,
        person = self.person)

    # Determine each type of access
    universeAccess = {}
    personAccess   = {}

    universeChildAccess = {}
    personChildAccess   = {}

    # Put into a more standard form:
    for record, person in records:
      item = {}
      if record.can_read is not None:
        item['read']  = record.can_read  == 1
      if record.can_write is not None:
        item['write'] = record.can_write == 1
      if record.can_clone is not None:
        item['clone'] = record.can_clone == 1
      if record.can_run is not None:
        item['run'] = record.can_run == 1

      if record.identity_uri is None and record.for_child_access == 1:
        universeChildAccess = item
      if record.identity_uri is None and record.for_child_access == 0:
        universeAccess = item
      if record.identity_uri is not None and record.for_child_access == 1:
        personChildAccess = item
      if record.identity_uri is not None and record.for_child_access == 0:
        personAccess = item

    ret.update({
      "access": {
        "current": mainAccess,
        "object": {
          "person":   personAccess,
          "universe": universeAccess
        },
        "children": {
          "person":   personChildAccess,
          "universe": universeChildAccess
        }
      }
    })

    # Gather viewers
    if obj and self.options.show_viewers and not resource_object:
      # We will return this array of objects
      viewers = []

      subtypes = None
  
      info = self.objects.infoFor(obj)

      subtypes = info.get('subtype')

      if subtypes is None:
        subtypes = []

      if not isinstance(subtypes, list):
        subtypes = [subtypes]

      type = info.get('type')

      # First get the viewers that are very specific
      for subtype in subtypes:
        viewers.extend(self.objects.viewersFor(viewsType    = type,
                                               viewsSubtype = subtype))

      # Get the generic viewers
      viewers.extend(self.objects.viewersFor(viewsType = type))

      for subtype in subtypes:
        viewers.extend(self.objects.viewersFor(viewsSubtype = subtype))

      # Do some discovery of viewers within the federation, perhaps
      if self.options.discover:
        if not viewers:
          for subtype in subtypes:
            viewers.extend(self.discover.viewersFor(type = type, subtype = subtype))
          viewers.extend(self.discover.viewersFor(type = type))

      ret["viewers"] = [{
        "id":           obj.id,
        "uid":          obj.uid,
        "identity":     obj.identity_uri,
        "revision":     obj.revision,
        "name":         obj.name,
        "description":  obj.description,
        "type":         obj.object_type,
        "subtype":      obj.subtype.split(';')[1:-1],
        "architecture": obj.architecture,
        "organization": obj.organization,
        "environment":  obj.environment,
      } for obj in viewers]

    if obj and self.options.show_storage and not resource_object:
      ret["storage"] = self.objects.retrieveSizeFor(obj)

    if obj and self.options.show_resources and not resource_object:
      ret["resources"] = {}

      info = self.objects.infoFor(obj)

      for section in ['general', 'build', 'run', 'init']:
        if section == 'general' or ('install' in info.get(section, {})):
          ret["resources"][section] = []

          within = info
          if section != 'general':
            within = info.get(section, {})

          for resource in within.get("install", []):
            # Return resource stat
            try:
              resourceStat = self.resources.retrieveFileStat(resource.get("id"),
                                                             resource.get("uid"),
                                                             revision = resource.get("revision"),
                                                             resourceType = resource.get("subtype"))
            except:
              # Cannot find the resource
              resourceStat = {}

            ret["resources"][section].append(resourceStat)

    # Gather read-only and review tokens
    if self.person and hasattr(self.person, 'id'):
      if obj is None and resource_object:
        obj = SimpleNamespace(id = resource_object['id'])

      account = self.accounts.retrieveAccount(identity = self.person.identity)
      ret.update({
        "tokens": {
          "readOnly": self.accounts.generateToken(account, obj, "readOnly"),
          "anonymous": self.accounts.generateToken(account, obj, "anonymous"),
        }
      })
    elif self.person and hasattr(self.person, 'anonymous') and self.person.anonymous:
      ret["anonymous"] = True
    elif self.person and hasattr(self.person, 'readonly') and self.person.readonly:
      ret["readOnly"] = True

    if self.options.to_json:
      import json
      Log.output(json.dumps(ret))
    else:
      Log.output(str(ret))

    return 0

  def do(self):
    # Get the object to list

    # Query for object by id
    obj = self.objects.resolve(self.options.object, person = self.person, allowNone = True)
    return self.reportStatus(obj)
