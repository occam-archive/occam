# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# On the topic of database ORM libraries that could be used instead:

# Much of this would be implemented just as well or better by an ORM type of
# library such as SQLAlchemy. However, that library is so bulky that it slows
# down just about every operation done on the command line because of how long
# it takes to import (around 0.4 to 0.5 seconds) Nothing I tried, no matter how
# clever I thought I was being, worked to reduce this quite noticeable lag.

# Therefore, since the database is used to *speed up* operations, and the ORM
# library makes that prohibitive, we are rolling our own light ORM-type thing
# and creating SQL queries ourselves (which is something you generally end up
# crafting anyway for trickier and heavier queries.)

# The ORM functionality is not really needed by anything the toolchain does.
# Much of the logic would be detached from it, anyway. It just can't justify
# itself enough as a 'clean-code' solution when it gives so much headache.

# Also, this allows for us to create database driver backends for anything
# that is not supported by the ORM much more easily if we ever need to.

# Used to import using a string as a path. This allows us to lazyload the
# database driver to just load the one referred by the configuration file.
# We won't be able to simple import all the drivers because we don't want
# to force the installation of all driver libraries. For instance, postgres
# driver requires psycopg2, which requires the installation of a systemwide
# postgresql installation. That's unacceptable when you have people who will
# simply use sqlite3.

from occam.manager import uses as manager_uses, manager
from occam.config  import Config
from occam.object  import Object

from occam.log import loggable
from datetime import datetime

import sys
import os
import sql

class Reader:
  def __init__(self, reader, writer):
    self.write  = writer
    self.reader = reader

  def __getattr__(self, token):
    return self.reader.__getattr__(token)

@loggable
@manager("database")
class DatabaseManager:
  """ This manages database access and management.
  """

  # The database drivers loaded
  drivers    = {}

  # The table schemas we know of
  tables     = {}

  # The index schemas we know of
  indexes    = {}

  # The datastores we are aware of
  datastores = {}

  # The migrations we have queued
  migrations = []

  def __init__(self):
    """Instantiates the Database Manager.
    """

    self._driver = None

  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new database driver to the possible drivers.
    """
    DatabaseManager.drivers[adapter] = driverClass

  @staticmethod
  def registerIndex(table, indexName, indexClass):
    """ Adds a new database index to the list of known indexes.
    """
    DatabaseManager.indexes[table] = DatabaseManager.indexes.get(table, {})
    DatabaseManager.indexes[table][indexName] = indexClass

  @staticmethod
  def registerTable(tableName, tableClass):
    """ Adds a new database table to the list of known tables.
    """
    DatabaseManager.tables[tableName] = tableClass

  @staticmethod
  def registerDatastore(managerName, datastoreClass):
    """ Adds a new database manager to the list of known datastores.
    """

    DatabaseManager.datastores[managerName] = datastoreClass

  @staticmethod
  def tableFor(tableName):
    """ Retrieves the table class for the given table by its name.
    """
    
    return DatabaseManager.tables.get(tableName)

  @staticmethod
  def schemaFor(tableName):
    """ Retrieves the schema for the given table by its name.
    """

    return DatabaseManager.tableFor(tableName).schema

  @staticmethod
  def datastoreFor(manager):
    """ Returns the datastore for the given manager name.

    Returns None if the datastore is not known.
    """

    return DatabaseManager.datastores.get(manager)

  def driverFor(self, adapter):
    """ Returns an instance of a driver for the given database adapter.
    """

    if not adapter in self.drivers:
      # TODO: error?
      adapter = 'sqlite3'

    return self.drivers[adapter](self.configuration)

  def disconnect(self):
    """ Uninitializes a database connection.
    """

    # All connect() does is grabs and initializes a driver.
    # But a driver is effectively a singleton for each thread.
    self.connect()

    if self._driver:
      self._driver.disconnect()

    self._driver = None

  def connect(self):
    """ Initializes a database connection.
    """

    # Determine the database type
    if self._driver is None:
      databasePath = Config.root()
      adapter      = self.configuration.get('adapter', 'sqlite3')

      # Attempt to find that database driver
      import importlib

      # try:
      importlib.import_module("occam.databases.plugins.%s" % (adapter))
      # except ImportError:
      #   pass

      self._driver = self.driverFor(adapter)

    return self._driver

  def recordFor(self, uuid):
    """ Retrieves the ObjectRecord for the given object uuid.
    """

    rows = self.retrieveObjects(uuid = uuid)
    if len(rows) > 0:
      return rows[0]

    return None

  def insert(self, session, record):
    """ Inserts the given record into the database.
    """

    table = sql.Table(record._table)

    query = None
    columns = [table.__getattr__(x) for x in list(record._dirty.keys())]
    values = list(record._dirty.values())

    query = table.insert(columns=columns, values=[values])

    if os.getenv('OCCAM_QUERY'):
      DatabaseManager.Log.write(self.formQuery(session, query))

    self._driver.execute(session, query)

  def updateSerial(self, session, table, column_name):
    """ Updates the table's identity serial value if necessary.
    """
    self._driver.updateSerial(session, table, column_name)

  def commit(self, session):
    """ Commits any changes made to the database.
    """

    self._driver.commit(session)

  def retrieveOutputs(self, objectRecord):
    """ Retrieves the ObjectOutputRecord rows for the given ObjectRecord.
    """

    session = self.session()

    outputs = sql.Table('object_outputs')

    query = outputs.select()
    query.where = (outputs.occam_object_id == objectRecord.id)

    from occam.objects.records.object_output import ObjectOutputRecord

    self._driver.execute(session, query)

    records = self._driver.many(session, size=10)
    records = [ObjectOutputRecord(x) for x in records]

    return records

  def retrieveInputs(self, objectRecord):
    """ Retrieves the ObjectOutputRecord rows for the given ObjectRecord.
    """

    session = self.session()

    inputs = sql.Table('object_inputs')

    query = inputs.select()
    query.where = (inputs.occam_object_id == objectRecord.id)

    from occam.objects.records.object_input import ObjectInputRecord

    self._driver.execute(session, query)

    records = self._driver.many(session, size=10)
    records = [ObjectInputRecord(x) for x in records]

    return records

  def formQuery(self, session, query):
    """ Returns the full query from the sql Query object.
    """

    # Attempt to form query (although, the param might not be '%s, etc')
    mark = sql.Flavor.get().param
    try:
      if mark == "%s":
        q = q % tuple(map(lambda x: '"' + x + '"' if isinstance(x, str) else str(x), parts[1]))
      elif mark == "?":
        parts = tuple(query)
        q = parts[0].replace('= ?', '= %s')
        q = q % tuple(map(lambda x: '"' + x + '"' if isinstance(x, str) else str(x), parts[1]))
      else:
        q = "".join(map(str, tuple(query)))
    except:
      # The query is complicated, I guess... so... just do something lackluster
      q = "".join(map(str, tuple(query)))

    return q

  def execute(self, session, query, setID=False, postamble=None):
    """ Executes the given query within the given session.
    """

    if os.getenv('OCCAM_QUERY'):
      DatabaseManager.Log.write(self.formQuery(session, query))

    self._driver.execute(session, query, setID=setID, postamble=postamble)

    return self._driver.rowCount(session)

  def fetch(self, session):
    """ Fetches the next record from the current session.
    """
    return self._driver.fetch(session)

  def many(self, session, size=10):
    """ Fetches the next set of records from the current session.
    """
    return self._driver.many(session, size)

  def delete(self, session, record):
    """ Deletes the given record or records.
    """

    # Recursively call if this is a list of records
    if isinstance(record, list):
      for item in record:
        self.delete(item)
      return

    table = sql.Table(record._table)

    query = table.delete()
    query.where = (table.id == record.id)

    if os.getenv('OCCAM_QUERY'):
      DatabaseManager.Log.write(self.formQuery(session, query))

    self._driver.execute(session, query)
    self._driver.commit(session)

    # TODO: return False on error.
    return True

  def session(self):
    """ Creates a session (typically a cursor) to a database.
    
    You can then execute queries and create transactions with this session.
    """

    return self.connect().session()

  def renameTable(self, session, tableOld, tableNew):
    self._driver.renameTable(session, tableOld, tableNew)

  def update(self, session, record):
    """ Saves a new version of the given row representing a record.
    """

    # Recursively call if this is a list of records
    if isinstance(record, list):
      ret = 0
      for item in record:
        ret += self.update(session, item)
      return ret

    table = sql.Table(record._table)
    schema = DatabaseManager.schemaFor(record._table)

    updatedValues = self._driver.massageValues(schema, record._dirty)

    columns = [table.__getattr__(x) for x in list(updatedValues.keys())]
    values  = list(updatedValues.values())

    if record._newRecord:
      # This triggers python-sql's table.attribute style usage for update():

      # INSERT queries can insert multiple rows so values is an array
      query = table.insert(columns = columns, values = [values])
    else:
      # This triggers python-sql's table.attribute style usage for update():

      if columns == []:
        # Nothing to update
        return True

      # UPDATE only takes a single list of values because it is performed per row
      query = table.update(columns = columns, values = values)

      # Look up by primary key
      if 'id' in schema and schema['id'].get('primary') and 'constraint' not in schema['id']:
        query.where = (table.id == record.id)
      else:
        # Find a unique constraint to use
        found = False
        query.where = sql.Literal(True)
        for k,v in schema.items():
          if v.get('primary') and 'constraint' not in v:
            query.where = query.where & (getattr(table, k) == getattr(record, k))
            found = True

          if 'constraint' in v:
            if v.get('primary') and 'columns' in v['constraint']:
              for column in v['constraint']['columns']:
                query.where = query.where & (getattr(table, column) == getattr(record, column))
                found = True

        if found == False:
          # Cannot commit this change
          return None

    setID = (('id' in schema) and schema['id'].get('primary', False) and 'constraint' not in schema['id'])
    postamble = None
    if record._newRecord:
      postamble = self._driver.massageInsert(query, record, record._table, schema)
    else:
      postamble = self._driver.massageUpdate(query, record, record._table, schema)
    ret = self.execute(session, query, setID=setID, postamble=postamble)

    if record._newRecord:
      record._newRecord = False
      if setID:
        row_id = self._driver.lastRowID(session)
        if row_id and record._data.get('id') is None:
          record._data['id'] = row_id

    return ret

  def lastMigration(self):
    """ Returns information about the last migration.
    """

    from occam.databases.records.migration import MigrationRecord

    # We directly access the database driver, since we cannot use ourselves
    # to do it...

    session = self.session()

    migrations = sql.Table("migrations")
    query = migrations.select()

    self.execute(session, query)
    row = self.fetch(session)
    if row is None:
      row = MigrationRecord()
      row.last = datetime(1970, 1, 1)
    else:
      row = MigrationRecord(row)

    return row

  def queryMigrations(self, time=None):
    """ List the migrations that exist since the given time.

    Args:
      time (datetime): The time to start from. All returned entries will be
        after this time. Defaults to None, which lists all migrations.

    Returns:
      datetime: The date of the most recent migration found.
      list: A list of dates of migrations.
    """

    # First, we find the years we need
    years = []

    # Look for migrations listed under scripts/migrations of the running Occam
    # binary's repository.
    occamBinaryPath = os.path.dirname(os.path.realpath(__file__))
    migrationsPath = os.path.join(occamBinaryPath, "..", "..", "..", "scripts", "migrations")
    migrationsPath = os.path.realpath(migrationsPath)

    if os.path.isdir(migrationsPath):
      # Migrations are listed in directories with year numbers. Find each
      # directory named after a year so we can record the year and path to the
      # year.
      for subpath in os.listdir(migrationsPath):
        try:
          yearPath = os.path.join(migrationsPath, subpath)
          if os.path.isdir(yearPath):
            year = int(subpath)
            if time is None or year >= time.year:
              # Pick that year
              years.append((year, yearPath,))
        except ValueError as e:
          continue

    # Sort by year.
    years = sorted(years, key = lambda x: x[0])

    scriptPaths = []

    # Track the oldest date of all the scripts we see.
    mostRecent = time

    # Find all scripts in the year directories and add them to the list of
    # script paths if they occur after the time parameter passed in.
    import re
    dateRegex = re.compile(r"(\d+)-(\d+)\D")
    for year, yearPath in years:
      # For each year directory, take files that make a date and add them to
      # the scriptPaths.
      for subpath in os.listdir(yearPath):
        try:
          datePath = os.path.join(yearPath, subpath)
          if os.path.isfile(datePath):
            # Parse date (mm-dd)
            matches = dateRegex.match(subpath)
            if matches is None:
              continue

            month = int(matches.group(1))
            day = int(matches.group(2))
            migrationDate = datetime(year, month, day)
            if time is None or migrationDate > time:
              scriptPaths.append(datePath)
            if mostRecent is None or migrationDate > mostRecent:
              mostRecent = migrationDate
        except ValueError as e:
          continue

    # Return the most recent date and all migration script paths.
    return mostRecent, scriptPaths

  def performMigrations(self):
    """ Performs the queue of pending migrations.
    """

    for tableName, migration, type in DatabaseManager.migrations:
      if type == "up":
        # Patch the table schemas to match columns in the migration
        session = self.session()
        newSchema = migration()
        oldSchema = DatabaseManager.schemaFor(tableName)
        self._driver.migrate(session, tableName, newSchema)
        self.commit(session)
      elif type == "patch":
        # Call the migration code and pass along a DatabaseManager
        migration(self)

    DatabaseManager.migrations = []

  def updateLastMigration(self, time):
    """ Updates the timestamp of the last migration to the given time.
    """

    from occam.databases.records.migration import MigrationRecord

    # We directly access the database driver, since we cannot use ourselves
    # to do it...

    session = self.session()

    row = self.lastMigration()

    row.last = time

    self.update(session, row)
    self.commit(session)

  def createIndex(self, tableName, tableClass, indexName, indexClass):
    """ This will create the given index.
    """

    session = self.session()

    DatabaseManager.Log.noisy("Creating the %s index." % (tableName))
    self._driver.index(session, tableName, tableClass.schema,
                                indexName, indexClass)

    self.commit(session)

  def createTable(self, tableName, tableClass, classList):
    """ This will create the given table.
    """

    session = self.session()

    DatabaseManager.Log.noisy("Creating the %s table." % (tableName))
    self._driver.create(session, tableName, tableClass.schema, classList)

    self.commit(session)

  def createDatabase(self):
    """ This will create the database and initialize the tables.
    """

    # Load the driver
    session = self.session()

    # Load all tables in the project
    import os
    import importlib

    # These are located in each component (under 'components' in the configuration)
    configuration = Config.load()
    components = configuration.get('system', {}).get('components', [])

    for component in components:
      component_module = importlib.import_module("%s.manager" % (component))
      component_path = os.path.dirname(component_module.__file__)

      tablePath = os.path.realpath("%s/records" % (os.path.realpath(component_path)))

      # Import every command and list its help contents
      if os.path.exists(tablePath):
        for f in os.listdir(tablePath):
          if f.startswith("_") or not f.endswith(".py"):
            continue

          moduleName = os.path.splitext(f)[0]

          try:
            importlib.import_module("%s.records.%s" % (component, moduleName))
          except ImportError:
            pass

    # Create tables using the driver
    # We have to order them by who depends on who (for foreign key refs)

    # Look for foreign keys
    dependencies = {}
    rootTables   = []
    handledTables = {}

    for tableName, tableClass in DatabaseManager.tables.items():
      dependencies[tableName] = []
      for k,v in tableClass.schema.items():
        if "foreign" in v:
          # Get the dependency
          dependency = v['foreign']['table']

          # Do not add the dependency if it is trying to point to itself
          if dependency != tableName:
            if not dependency in dependencies[tableName]:
              dependencies[tableName].append(dependency)

      if len(dependencies[tableName]) == 0:
        rootTables.append((tableName, tableClass,))

    # For each root table, create that table
    while len(rootTables) > 0:
      tableName, tableClass = rootTables.pop()
      handledTables[tableName] = tableClass.schema

      self.createTable(tableName, tableClass, handledTables)

      for k, v in dependencies.items():
        if tableName in v:
          v.remove(tableName)

          if len(v) == 0:
            rootTables.append((k, DatabaseManager.tables[k],))

    # Now that tables are created, create the indexes
    for tableClass, indexes in DatabaseManager.indexes.items():
      tableName = tableClass._table
      for indexName, indexClass in indexes.items():
        self.createIndex(tableName, tableClass, indexName, indexClass)

    # Assume we do not need to perform any migrations
    time, _ = self.queryMigrations()
    self.updateLastMigration(time)

def database(name):
  """ This decorator will register a possible database backend.
  """

  def _database(cls):
    DatabaseManager.register(name, cls)
    return cls

  return _database

def index(table, indexName):
  """ This creates an index for the given table.
  """

  def _index(cls):
    DatabaseManager.registerIndex(table, indexName, cls)
    pass

  return _index

def table(tableName):
  """ This decorator will register a table into our database. It will keep
      track of changes which can then be used by a database driver to update
      the records.
  """

  def _table(cls):
    DatabaseManager.registerTable(tableName, cls)

    # Go through the schema and create the attributes
    def _readAttribute(self, name):
      if name in cls.schema:
        if not name in self._data:
          self._data[name] = cls.schema[name].get('default')
        if isinstance(self._data[name], str) and cls.schema[name].get('type') == "json":
          import json
          try:
            self._data[name] = json.loads(self._data[name])
          except:
            self._data[name] = None
        return self._data[name]

      return self.__getattribute__(name)

    old_setattr = cls.__setattr__

    def _updateAttribute(self, name, value):
      if name in cls.schema:
        self._data.__setitem__(name, value)
        if 'type' in cls.schema[name] and cls.schema[name]['type'] == 'json':
          import json
          value = json.dumps(value)
        self._dirty.__setitem__(name, value)
        return value

      return old_setattr(self, name, value)

    old_init = cls.__init__

    def _initialize(self, data=None):
      self._newRecord = False
      if data is None:
        data = {}
        self._newRecord = True
      self._data = data
      old_init(self)

      self._dirty = {}

    cls.__getattr__ = _readAttribute
    cls.__setattr__ = _updateAttribute
    cls.__init__    = _initialize

    cls._table = tableName

    return cls

  return _table

def uses(datastoreClass):
  def _uses(cls):
    writerClass = None
    readerClass = datastoreClass

    # Our base case propagation of __getattr__
    def _empty(self, token):
      return self.__getattribute__(token)
    datastoreGetattr = _empty

    if datastoreClass._reader:
      # We append the reader, not ourselves
      # The reader will append us when it sees a 'write' attribute
      writerClass = datastoreClass
      readerClass = datastoreClass._reader

    # Ensure datastore table exists
    if not hasattr(cls, '_datastores'):
      cls._datastores = {}
      cls._datastoreTokens = []
      cls._writers = {}

    if not readerClass._token in cls._datastores:
      # The datastore is new... initialize the lazy loading:

      try:
        # This will fail when __getattr__ isn't already known
        datastoreGetattr = cls.__getattr__
      except:
        pass

      def initManager(self, token):
        if (token in cls._datastoreTokens):
          datastore = cls._datastores[token]

          if datastore._instance is None:
            datastore._instance = datastore()

          instance = datastore._instance

          if (token in cls._writers):
            writer = cls._writers[token]
            if writer._instance is None:
              writer._instance = writer()
            instance = Reader(instance, writer._instance)

          setattr(self, token, instance)
          return instance

        return datastoreGetattr(self, token)

      cls.__getattr__ = initManager

      cls._datastores[readerClass._token] = readerClass

      if writerClass:
        cls._writers[readerClass._token]  = writerClass

    cls._datastoreTokens.append(readerClass._token)

    return cls

  return _uses

def datastore(name, reader=None):
  """ Decorator that marks a class with the capability of accessing the database.
  """

  def _datastore(cls):
    cls._instance = None
    cls._token = name
    cls._reader = reader

    def _empty(self, token):
      return self.__getattribute__(token)

    try:
      old_getattr = cls.__getattr__
    except:
      old_getattr = _empty

    cls = manager_uses(DatabaseManager)(cls)
    DatabaseManager.registerDatastore(name, cls)

    return cls

  return _datastore

def patch(tableName):
  """ Decorator that defines an 'patch' migration.

  This will perform updates to the data in the database.
  """

  def _func(f):
    # Perform the changes to the schema
    DatabaseManager.migrations.append((tableName, f, "patch"))

  return _func

def up(tableName):
  """ Decorator that defines an 'up' migration.

  This occurs when changes are made to the database to bring it forward in time.
  """

  def _func(f):
    # Perform the changes to the schema
    DatabaseManager.migrations.append((tableName, f, "up"))

  return _func

class DataError(Exception):
  """ Base class for all database errors.
  """

class DataNotUniqueError(DataError):
  """ When a database record to be inserted already exists.
  """
