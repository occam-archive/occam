# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.databases.manager import DatabaseManager

from occam.object import Object
from occam.log import Log

import os

@command('databases', 'migrate',
  category      = 'Database Management',
  documentation = "Migrates database schemata")
@option("-s", "--script", dest    = "script",
                            action  = "store",
                            help    = "run a specific migration script unconditionally")
@uses(DatabaseManager)
class DatabasesMigrateCommand:
  """ This command will import an Identity from a JSON representation.
  """

  def do(self):
    Log.header(key="occam.databases.commands.migrate.header")

    script = self.options.script
    if script:
      exec(open(script).read())
      self.database.performMigrations()
      return 0

    # Retrieve the last migration (as a datetime)
    last = self.database.lastMigration().last

    # Query all migrations from that date onward
    mostRecent, migrations = self.database.queryMigrations(time = last)

    # Run each migration
    for migration in migrations:
      year = os.path.basename(os.path.realpath(os.path.join(migration, "..")))
      module = os.path.basename(os.path.realpath(os.path.join(migration, "..", "..", "..", "..")))
      Log.write(key="occam.databases.commands.migrate.migrating", year=year, module=module, file=os.path.basename(migration))
      exec(open(migration).read())

      self.database.performMigrations()

    self.database.updateLastMigration(mostRecent)

    Log.done(key="occam.databases.commands.migrate.done")

    return 0
