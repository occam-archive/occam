# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log     import loggable
from occam.manager import manager, uses

import os

from occam.databases.manager import DataNotUniqueError
from occam.people.manager    import PersonManager

@loggable
@manager("people.write", reader=PersonManager)
class PersonWriteManager:
  """ This manages the retrieval, use, and creation of private keys.
  """

  def update(self, identity, obj):
    """ Updates the Person object associated with the given identity.

    Args:
      identity (str) The identity URI.
      obj (Object) The "person" object that represents this identity.

    Returns:
      Boolean: Whether or not the record now exists.
    """

    try:
      record = self.datastore.write.update(identity, obj.id)
    except DataNotUniqueError as e:
      pass

    return True
