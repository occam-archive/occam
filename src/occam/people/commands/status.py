# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager import ObjectManager
from occam.people.manager  import PersonManager

from occam.objects.commands.status import ObjectsStatusCommand

@command('people', 'status',
  category      = 'Person Information',
  documentation = "Lists high-level information about the given identity.")
@argument("identity", type = str, help = "The identity to query.")
@argument("object", nargs = "0", using="findPerson") # Turn off the 'object' argument
@uses(PersonManager)
@uses(ObjectManager)
class PeopleStatusCommand(ObjectsStatusCommand):
  """ This class handles listing the current knowledge about a person object or file within.
  """

  def findPerson(self):
    person = self.people.retrieve(self.options.identity, person = self.person)

    if person is None:
      raise Exception("Person cannot be found.")
      return -1

    return self.objects.objectTagFor(person)
