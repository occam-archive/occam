# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from occam.log import loggable
from occam.config import Config
from occam.object import Object
from occam.semver import Semver
from occam.key_parser import KeyParser
from occam.manager import uses, manager
from occam.datetime import Datetime

from occam.objects.manager       import ObjectResolveVersionError
from occam.objects.write_manager import ObjectWriteManager
from occam.versions.manager      import VersionManager
from occam.storage.manager       import StorageManager
from occam.builds.manager        import BuildManager
from occam.backends.manager      import BackendManager
from occam.nodes.manager         import NodeManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.system.manager        import SystemManager
from occam.services.manager      import ServiceManager
from occam.keys.write_manager    import KeyWriteManager

import os
import json
import copy
import shutil
import time
import datetime
import base64

@loggable
@uses(ObjectWriteManager)
@uses(VersionManager)
@uses(StorageManager)
@uses(BuildManager)
@uses(BackendManager)
@uses(NodeManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(SystemManager)
@uses(ServiceManager)
@uses(KeyWriteManager)
@manager("manifests")
class ManifestManager:
  """ This component manages the creation and querying of tasks to build or run
  objects.
  """

  @staticmethod
  def deepMerge(destination, source):
    """
    """

    # TODO: lists should probably be appended

    for key, value in source.items():
      if isinstance(value, dict):
        # get node or create one
        node = destination.setdefault(key, {})
        ManifestManager.deepMerge(node, value)
      else:
        destination[key] = value

    return destination

  def __init__(self):
    self.parser = KeyParser()

  def store(self, task, person):
    """
    """

    # Create the task object itself
    taskObject = self.objects.write.create(name        = task.get('name'),
                                           object_type = "task",
                                           subtype     = task.get('subtype'),
                                           info        = task)

    # Store it with the person who created it
    if person:
      db_obj = self.objects.write.store(taskObject, identity = person.identity)
      taskObject.id  = db_obj.id
      taskObject.uid = db_obj.uid

    # Set task permissions to be permissive
    self.permissions.update(taskObject.id, canRead = True,
                                           canWrite = True,
                                           canClone = True)

    # Return the crafted task object
    return taskObject

  def build(self, object=None, id=None, revision=None, local=False, penalties=None, backend=None, locks=None, person=None, force=False):
    """ Builds the given object by any means.
    """

    objectInfo = {}

    if object:
      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not revision:
        revision = object.revision

    task = None
    if object is None:
      object = self.objects.retrieve(id=id, revision=revision, person=person)

    if object is None:
      ManifestManager.Log.error("Object not found.")
      return -1

    objectInfo = self.objects.infoFor(object)
    id = object.id

    if not "build" in objectInfo:
      raise BuildInformationMissingError(f"No build information for {objectInfo.get('type')} {objectInfo.get('name')}")

    task = self.taskFor(object, section = "build",
                                backend = backend,
                                person  = person,
                                force   = force)

    if task is None:
      ManifestManager.Log.write("Could not produce a task to build this object.")
      return None

    taskName = f"Task to build {objectInfo.get('name')} on {task.get('backend')}"
    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "build",
                                           info        = task)
    if person:
      db_obj = self.objects.write.store(taskObject, identity = person.identity)
      taskObject.id  = db_obj.id
      taskObject.uid = db_obj.uid

    # Set build task permissions to be permissive
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    return taskObject

  def run(self, object=None, id=None, revision=None, local=False, arguments=None, inputs=None, penalties=None, backend=None, generator=None, service=None, services=None, preferred=None, locks=None, person=None):
    """ Runs the given object.

    Args:
      service (string): The service to run for this object, given that it provides that service. Can be None.
    """

    if not person:
      ManifestManager.Log.error("Cannot create task: must be logged in.")
      return None

    ownerInfo = self.objects.ownerInfoFor(object)
    info      = self.objects.infoFor(object)

    if id is None:
      id = self.objects.idFor(object, person.identity)
      object.id = id

    if revision is None:
      revision = object.revision

    # TODO: Pull out the build infos if this object requires a build
    #       and use that task to run this object with the built version.
    buildTask = None
    if local == False and ("build" in info or "build" in ownerInfo) and object.link is None:
      buildTasks = self.builds.retrieveAll(object)
      if buildTasks is None:
        buildTasks = []

      if "build" in ownerInfo and "build" not in info:
        buildTasks.extend(self.builds.retrieveAll(self.objects.ownerFor(object, person = person)) or [])

      for buildRecord in buildTasks:
        # Build the object
        # TODO: issue build command
        buildTask = self.objects.retrieve(buildRecord.build_id, buildRecord.build_revision, person=person)

        if buildTask is None:
          continue

        # If we found a suitable build, just quit looking
        buildTask = self.objects.infoFor(buildTask)
        buildTask['id'] = buildRecord.build_id
        break

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error(f"Object {info.get('type')} {info.get('name')} must be built first.")
        return None

    if local == True and "build" in self.objects.infoFor(object):
      # Look for the buildtask in the local path
      buildTaskPath = os.path.join(object.path, ".occam", "local", "object.json")

      try:
        with open(buildTaskPath, "rb") as f:
          buildTask = json.load(f)
      except:
        pass

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object must be built locally first.")
        return None

    if object.link and "build" in self.objects.infoFor(object):
      buildTaskPath = os.path.join(object.path, "..", "build", "run", "task", "object.json")
      buildTask = None
      try:
        with open(buildTaskPath, "rb") as f:
          buildTask = json.load(f)
      except Exception as e:
        pass

    task = None

    # Get the object to run
    if object is None:
      ManifestManager.Log.error("Object not found.")
      return None

    task = self.taskFor(object, buildTask=buildTask, arguments = arguments, backend = backend, generator=generator, inputs=inputs, services=services, person=person)

    # Store task
    objectInfo = self.objects.infoFor(object)

    environment = task.get('environment')
    architecture = task.get('architecture')

    taskName = f"Task to run {objectInfo.get('name')} on {task.get('backend')}"
    if environment and architecture:
      taskName = f"Task to run {objectInfo.get('name')} on {environment}/{architecture}"

    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "run",
                                           info        = task)
    db_obj = self.objects.write.store(taskObject, identity = person.identity)
    taskObject.id  = db_obj.id
    taskObject.uid = db_obj.uid

    # For now the permissions will be permissive:
    # This task should belong to the object, however, and inherit permissions.
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    if task is None:
      ManifestManager.Log.error("Could not determine how to run this object.")
      return None

    return taskObject

  def isBuildable(self, object):
    """ Returns True if the given object is buildable.
    """

    dependencyInfo = self.objects.infoFor(object)
    dependencyOwnerInfo = self.objects.ownerInfoFor(object)
    return 'build' in dependencyInfo or 'build' in dependencyOwnerInfo

  def retrieveBuildTaskFor(self, object, preferred, person = None):
    """ Retrieve the task object that built the given object.
    """

    # An object that is not buildable obviously has no build task
    if not self.isBuildable(object):
      return None

    # Our goal is to determine the build task
    buildTask = None

    # Look up a build or report that we need to build this object
    objectInfo = self.objects.infoFor(object)
    buildingObject = object
    if 'build' not in objectInfo:
      buildingObject = self.objects.ownerFor(object, person = person)

    buildingObjectInfo = self.objects.infoFor(buildingObject)
    buildIds = [{"id": x.build_id, "revision": x.build_revision} for x in self.builds.retrieveAll(buildingObject, preferred)]

    # For every build we know, we attempt to find that build's content
    for build in buildIds:
      path = self.objects.buildPathFor(buildingObject, build["id"])

      if path is not None:
        buildTask = self.objects.retrieve(id = build["id"],
                                          revision = build["revision"],
                                          person = person)

        if buildTask:
          buildId = buildTask.id
          buildRevision = buildTask.revision
          buildTask = self.objects.infoFor(buildTask)
          buildTask['id'] = buildId
          buildTask['revision'] = buildRevision
          break

    if buildTask is None:
      raise BuildRequiredError(objectInfo, buildingObject, f"cannot find a built task for {objectInfo.get('type')} {objectInfo.get('name')}")

    return buildTask

  def completeInitSection(self, currentInit, currentProvider):
    """ Will finalize the given init section based on the given provider object info.

    Modifies currentInit in place.

    Returns:
      currentInit
    """

    providesInfo = currentProvider.get('provides', {})
    if isinstance(providesInfo, list):
      # TODO: handle multiple environments
      providesInfo = providesInfo[0]

    providerEnvVars = providesInfo.get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in currentInit.get('env', {}).items():
      if isinstance(value, str):
        value = {
          "items": [value]
        }
      separator = value.get('separator')
      unique    = value.get('unique', False)

      if separator:
        # TODO: handle escapes
        items = value.get('items', [])
        items = separator.join(items)
      else:
        value = value.get('items', [])
        if len(value) == 0:
          items = ""
        else:
          items = value[-1]

      currentInit['env'] = currentInit.get('env', {})
      currentInit['env'][key] = items

    return currentInit

  def combineEnvSection(self, currentEnv, secondEnv):
    """ Will produce a combined 'env' metadata section from two given ones.

    Will add any addition items to the 'currentEnv' in place and return
    the same 'currentEnv'.
    """

    if not isinstance(currentEnv, dict) or not isinstance(secondEnv, dict):
      return currentEnv

    for key, value in secondEnv.items():
      # Canonize value as a dict with an items field as a list
      if not isinstance(value, dict):
        value = { "items": value }

      if not 'items' in value:
        value['items'] = []

      if not isinstance(value['items'], list):
        value['items'] = [value['items']]

      # If the key already exists, merge environment metadata properly
      if key in currentEnv:
        if not isinstance(currentEnv[key], dict):
          currentEnv[key] = {
            "items": currentEnv[key]
          }

        if not 'items' in currentEnv[key]:
          currentEnv[key]['items'] = []

        if not isinstance(currentEnv[key]["items"], list):
          currentEnv[key]["items"] = [currentEnv[key]["items"]]

        for subKey, subValue in value.items():
          # For items, which is a list of values, just extend the list
          if subKey == 'items':
            currentEnv[key]['items'].extend(value.get('items', []))
          elif not subKey in currentEnv[key]:
            # For other metadata, add it only if it doesn't already exist
            currentEnv[key][subKey] = subValue
      else:
        # Otherwise, just add it
        currentEnv[key] = value

    return currentEnv

  def parseInitSection(self, objectInfo, currentInit, currentProvider):
    """ Will modify the given current init section with the contents of the given init section.

    It will use currentProvider as a basis of understanding what certain environment variables mean.
    """

    # Retrieve the 'init' section of the given object
    objectInit = objectInfo.get('init')

    # Bail if the init section is not a dictionary
    if objectInit is None or (not isinstance(objectInit, OrderedDict) and not isinstance(objectInit, dict)):
      return currentInit

    # Retrieve 'env' section
    objectEnvVars = objectInit.get('env', {})

    # Bail if env section is invalid
    if objectEnvVars is None or (not isinstance(objectEnvVars, OrderedDict) and not isinstance(objectEnvVars, dict)):
      return currentInit

    providerEnvVars = currentProvider.get('provides', {}).get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in objectEnvVars.items():
      separator = None
      unique    = False

      if key in currentInit.get('env', {}):
        separator = currentInit['env'][key].get('separator')
        unique = currentInit['env'][key].get('unique', False)

      if isinstance(value, dict):
        separator = value.get('separator')
        unique = value.get('unique', False)

      if key in providerEnvVars:
        separator = providerEnvVars[key].get('separator', separator)
        unique    = providerEnvVars[key].get('unique', unique)

      if isinstance(value, dict):
        value = value.get('items', [])

      if isinstance(value, list):
        items = value
      elif separator:
        # TODO: handle escapes
        items = value.split(separator)
      else:
        items = [value]

      currentInit['env'] = currentInit.get('env', {})
      newItems = currentInit['env'].get(key, {}).get('items', [])
      newItems.extend(items)

      if unique:
        newItems = list(set(newItems))

      currentInit['env'][key] = {
        "unique":    unique,
        "separator": separator,
        "items":     newItems
      }

    return currentInit

  def resolveDependency(self, dependencyList, dependencyIndex, context, locks, person):
    """ Resolves the given dependency and returns its contents.
    """

    dependency = dependencyList[dependencyIndex]
    penalties = context['penalties']
    dependencyInfo = None
    dependencyObject = None

    # The default replace policy is 'always'
    # When it is 'never', the dependency is always added even if it conflicts
    replacePolicy = dependency.get('replace', 'always')

    # Pull out dependency index information about the currently resolved dependency
    # This keeps track of what we had done previously
    dependenciesIndex = context['dependenciesIndex']
    dependencyInfo = dependenciesIndex.get(dependency['id'], {})
    requestedVersion = ",".join(dependencyInfo.get('version', [])) or None
    requestedSubversion = dependencyInfo.get('subversion', dependency.get('subversion'))
    priorDependencyInfo = dependencyInfo

    # Keep track of the reason why we failed first
    reason = None

    # We don't look to see if we are replacing an existing dependency if
    # the policy is to never replace this one once resolved
    if replacePolicy == 'never':
      requestedVersion = dependency.get('version')
      requestedSubversion = dependency.get('subversion')

    # If we are currently resolving this dependency, just ignore this new context
    if priorDependencyInfo.get('pending', False):
      if replacePolicy == 'never':
        if ("requesting-" + requestedVersion) in priorDependencyInfo['pending']:
          return True
      else:
        if Semver.resolveVersion(requestedVersion, priorDependencyInfo['pending']):
          return True

        # Otherwise, we fail to resolve... satisfying with the current pending
        # version recurses to require a conflicting version of itself.
        return False

    # Determine if we have already successfully resolved that dependency
    dependencyInfos = priorDependencyInfo.get('resolved')
    for dependencyInfo in (dependencyInfos or []):
      if dependencyInfo and 'version' in dependencyInfo:
        if Semver.resolveVersion(requestedVersion, [dependencyInfo['version']]):
          # Update the dependency list with the resolved dependency
          dependencyList[dependencyIndex] = copy.deepcopy(dependencyInfo)
          dependencyList[dependencyIndex]['requested'] = dependency['version']

          # Make sure it inherits the inject property
          if dependency.get('inject'):
            dependencyList[dependencyIndex]['inject'] = dependency['inject']

          return True

    # Make sure we know we are currently resolving this dependency
    priorDependencyInfo['pending'] = priorDependencyInfo.get('pending') or []
    if requestedVersion:
      priorDependencyInfo['pending'].append("requesting-" + requestedVersion)

    # We have never resolved this dependency, or an updated version invalidated it
    # Let's resolve it
    dependencyInfo = None
    resolved = False

    # Retain the original version requirement that was satisfied
    if 'version' in dependency:
      dependency['requested'] = dependency['version']

    # We only 'retry' once when we find we need to penalize a sub-dependency
    lastAttempted = None

    while resolved is not True:
      # Resolve each dependency and pull out subDependencies
      ManifestManager.Log.noisy(f"looking for dependency {dependency.get('type')} {dependency.get('name')} @ {dependency.get('revision', requestedVersion)} replace: [{replacePolicy}]")

      try:
        dependencyObject = self.objects.retrieve(id         = dependency.get('id'),
                                                 version    = requestedVersion,
                                                 subversion = requestedSubversion,
                                                 revision   = dependency.get('revision'),
                                                 penalties  = penalties,
                                                 person     = person)
      except ObjectResolveVersionError:
        # Cannot find the version requested...
        # The version is likely constricted by a previous resolve
        # So, we need to reject this version and propagate that information
        #   back up to the parent call.

        # If we fail here, it is because we cannot find another valid version
        # of this required object after attempting all the others.

        # If we already have a 'reason' for failing, it means one of the valid
        # versions we rejected met a version conflict that affects something we
        # already resolved elsewhere. We need to report that one instead, so
        # don't overwrite it!
        if reason is None:
          # Make a note of the reason we have failed here
          reason = dependency

          # Determine how we already resolved this conflicted dependency
          if dependenciesIndex.get(reason['id'], {}).get('resolved'):
            previous = dependenciesIndex[reason['id']]['resolved'][0]
            reason = previous

        # Normalize resolved to be False
        resolved = False
        break

      if dependencyObject is None:
        ManifestManager.Log.noisy(f"failed to find {dependency.get('type')} {dependency.get('name')} @ {dependency.get('revision', requestedVersion)} replace: [{replacePolicy}]")
        resolved = False
        break

      # Get the actual realized dependency information
      dependencyObjectInfo = self.objects.infoFor(dependencyObject)

      # Create the dependency information and copy things we want to keep
      dependencyInfo = {}
      dependencyInfo.update(dependency)
      dependencyInfo['revision'] = dependencyObject.revision
      if dependencyObject.version:
        dependencyInfo['version'] = dependencyObject.version

        # Update our pending status to reflect that chosen version
        priorDependencyInfo['pending'] = priorDependencyInfo.get('pending') or []
        try:
          priorDependencyInfo['pending'].remove("requesting-" + requestedVersion)
        except:
          pass
        priorDependencyInfo['pending'].append(dependencyObject.version)

      if dependencyObject.uid:
        dependencyInfo['uid'] = dependencyObject.uid

      # Overwrite with known metadata
      for key in ['init', 'install', 'owner', 'file', 'network',
                  'metadata', 'environment', 'architecture', 'filter', 'inputs']:
        if key in dependencyObjectInfo:
          dependencyInfo[key] = dependencyObjectInfo[key]

      # These are keys we allow overrides for
      for key in ['dependencies', 'requires']:
        if key in dependencyObjectInfo and key not in dependencyInfo:
          dependencyInfo[key] = dependencyObjectInfo[key]

      # If we are not building, we are running.
      # If we are running, we need a build to use to know where those binaries are.
      if self.isBuildable(dependencyObject) and not dependency.get('build', {}).get('id'):
        # Get a built copy
        try:
          buildTask = self.retrieveBuildTaskFor(dependencyObject, preferred = None, person = person)
        except BuildRequiredError as e:
          buildTask = None

        # If we can't find a build... we fail to resolve this dependency
        if buildTask is None:
          # Penalize this choice of resolving the version
          dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
          penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
          ManifestManager.Log.noisy(f"Could not find a build task... rejecting {dependency.get('name', 'unknown')} {dependencyObject.version or dependencyObject.revision}.")
          continue

        # Set the build id for the build we will use
        dependencyInfo['build'] = {}
        dependencyInfo['build']['id'] = buildTask.get('id')
        dependencyInfo['build']['revision'] = buildTask.get('revision')
        dependencyInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

      # In case of recursive dependencies, we need to remember how we are
      # resolving ourselves:
      pushedIndex = None
      pushedPenalties = None
      pushedIndex = copy.deepcopy(dependenciesIndex)
      pushedPenalties = copy.deepcopy(penalties)

      # Resolve the dependencies of the dependencies
      resolved = self.resolveDependenciesFor(dependencyObject, dependencyInfo, context, locks, person, ignoreInject = (dependencyInfo.get('inject') == "ignore"))

      # If we could not resolve those dependencies... we didn't resolve the main
      # dependency this function was responsible for.
      if resolved is not True:
        # Restore our context
        if pushedIndex:
          dependenciesIndex = pushedIndex

        priorDependencyInfo = dependenciesIndex[dependency['id']]
        context['dependenciesIndex'] = dependenciesIndex

        if pushedPenalties:
          penalties = pushedPenalties
        context['penalties'] = penalties

        if resolved is not False:
          # We noted a failure to resolve a version of a dependency...
          # Keep track of that while we attempt our own resolution
          reason = resolved

          # Normalize our resolution signature as False
          resolved = False

          if 'revision' in reason:
            # Determine how we already resolved this conflicted dependency
            penalties[reason['id']] = [reason['revision']]
          else:
            # We have never actually resolved this dependency, so this is a
            # normal everyday failure
            lastAttempted = dependencyObject.revision
        else:
          # Ensure we always penalize ourselves since it was not a version conflict
          lastAttempted = dependencyObject.revision

        if lastAttempted == dependencyObject.revision:
          # Penalize this choice of resolving the version
          dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
          penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
          ManifestManager.Log.write(f"penalizing {dependency.get('name')} at revision {dependencyObject.revision}")

        lastAttempted = dependencyObject.revision

        if 'version' in dependency:
          ManifestManager.Log.noisy(f"failed to use {dependency.get('type')} {dependency.get('name')} version {dependency.get('version')}")

    try:
      priorDependencyInfo['pending'].remove("requesting-" + requestedVersion)
    except:
      pass

    if dependencyObject:
      try:
        priorDependencyInfo['pending'].remove(dependencyObject.version)
      except:
        pass

    if len(priorDependencyInfo['pending']) == 0:
      priorDependencyInfo['pending'] = None

    # Update the dependency list with the resolved dependency
    if dependencyInfo and resolved:
      dependencyList[dependencyIndex] = dependencyInfo

      dependenciesIndex = context['dependenciesIndex']
      dependenciesIndex[dependency['id']] = dependenciesIndex.get(dependency['id'], {})

      dependenciesIndex[dependency['id']]['resolved'] = dependenciesIndex[dependency['id']].get('resolved', [])
      dependenciesIndex[dependency['id']]['resolved'].append(dependencyInfo)

    # If we noted some version conflicts, return those instead
    if not resolved and reason:
      return reason

    return resolved

  def resolveDependenciesFor(self, object, objectInfo, context, locks, person, ignoreInject = False):
    """ Resolves the dependency list for the given object metadata.
    """

    dependenciesIndex = context['dependenciesIndex']

    # Resolve the list of dependencies
    resolved = True
    reason = None

    dependencies = objectInfo.get('dependencies', [])
    if 'init' in objectInfo:
      dependencies.extend(objectInfo['init'].get('dependencies', []))
      objectInfo['dependencies'] = dependencies

    dependencyLists = [(dependencies, "general")]

    # Also rake dependencies that are to be injected by the build task
    if not ignoreInject and objectInfo.get('build', {}).get('id'):
      # Get the dependencies that were used when the object was built
      buildTimeDependencies = objectInfo.get('build', {}).get('dependencies', [])

      # Do not consider if the inject parameter doesn't meet the given value
      buildTimeDependencies = list(
        filter(
          lambda x: x.get('inject') in ["init", "run"],
          buildTimeDependencies
        )
      )

      for buildDependency in buildTimeDependencies:
        # Remove existing build id so it will choose a new build if necessary
        if 'build' in buildDependency:
          del buildDependency['build']

        # Assert version requirements for the dependencies based on locks
        if 'lock' in buildDependency:
          # The 'lock' is something like '>=' or '=', etc
          # It allows for the version to be relative to the one used to build
          # the object prior to its inclusion in this task.
          # If there is a lock, then apply it
          lock = buildDependency['lock']
          buildDependency['version'] = lock + buildDependency.get('version', 'x')

          # And un-assert a revision
          if 'revision' in buildDependency:
            del buildDependency['revision']

        # Do not propagate the inject requirement into the new task
        del buildDependency['inject']

      # Reassert this list
      objectInfo.get('build', {})['dependencies'] = buildTimeDependencies

      # Append list of filtered build-time dependencies
      dependencyLists.append((buildTimeDependencies, "run-time",))
    else:
      if 'dependencies' in objectInfo.get('build', {}):
        del objectInfo['build']['dependencies']

    # Make a note of each dependency in this listing
    for dependencies, listType in dependencyLists:
      for i, dependency in enumerate(dependencies):
        # Determine the effective id for the dependency (allow it to inherit the
        # proper id/revision from its parent)
        if 'id' not in dependency:
          # This will occur if the dependency is provided by the object in a
          # sub-object, (or is the owner itself) and therefore has no specified 'id'
          owner = self.objects.ownerFor(object, person = person)
          ownerInfo = self.objects.ownerInfoFor(object)

          # First check if it is referring to its own owner
          if dependency.get('type') == ownerInfo.get('type') and dependency.get('name') == ownerInfo.get('name'):
            dependency['id'] = owner.id
            dependency['uid'] = owner.uid
          else:
            # The 'id' in this case is determined by the system relative to the
            # owning object.
            dependency['id'] = self.objects.idFor(owner, object.identity, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))
            dependency['uid'] = self.objects.uidFor(owner, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))

          # When a version is not explicit, we need to define a revision
          # In this case, we want the revision of the owning object
          if 'version' not in dependency:
            dependency['revision'] = dependency.get('revision', object.revision)

          # The 'version' should also be inherited by this dependency with the
          # version given by the owning object, when known.
          if 'version' not in dependency:
            if 'version' in objectInfo:
              dependency['version'] = objectInfo['version']
            if 'subversion' in objectInfo:
              dependency['subversion'] = objectInfo['subversion']
            if object.version:
              dependency['version'] = object.version

        if ignoreInject and 'inject' in dependency:
          del dependency['inject']

        # Add the intention to our ongoing dependency list
        if 'version' in dependency:
          # Override requested version with any 'lock' specified by the agent
          if locks and locks.get(dependency.get('id')):
            dependency['version'] = dependency.get('version', locks[dependency.get('id')]) + "," + locks[dependency.get('id')]
            dependency['version'] = ",".join(list(set(dependency['version'].split(","))))

          dependenciesIndex[dependency['id']] = dependenciesIndex.get(dependency['id'], {'version': []})
          dependencyInfo = dependenciesIndex[dependency['id']]
          dependencyVersion = dependency['version']
          #print(dependency['name'], dependencyVersion)

          if dependency.get('replace') != 'never':
            if 'version' not in dependencyInfo:
              dependencyInfo['version'] = []

            for subVersion in dependencyVersion.split(','):
              if subVersion not in dependencyInfo['version']:
                dependencyInfo['version'].append(subVersion)

    # Go through each dependency source
    for dependencies, listType in dependencyLists:
      # And then through each individual dependency
      for i, dependency in enumerate(dependencies):
        ManifestManager.Log.noisy(f"Raking {listType} dependency: {dependency.get('type')} {dependency.get('name')} {dependency.get('version', '')}{('@' + dependency.get('revision', '')) if 'revision' in dependency else ''}")
        overwriteInfo = None

        resolved = self.resolveDependency(dependencies, i, context = context,
                                                           locks   = locks,
                                                           person  = person)
        if resolved is not True:
          if resolved is not False:
            # There was a version conflict
            # We must penalize that version and try ourselves again
            reason = resolved
          break

      if resolved is not True:
        break

    return resolved

  def resolveServicesFor(self, objectInfo, section, services, context):
    # Get the list of requested services
    requiresInfo = objectInfo.get('requires', []) + objectInfo.get(section, {}).get('requires', [])
    requires = list(map(lambda x: x.get('name'), requiresInfo))

    unresolved = []

    # We can resolve some services with those given by a client.
    if objectInfo.get('environment') == "linux":
      for required in (services or []):
        if required not in requires:
          ManifestManager.Log.write(f"Adding client requested service requirement '{required}'")
          requiresInfo.append({
            "type": "service",
            "name": required
          })

          # Add the service to the object info so it is resolved as normal
          # later on by the backend.
          objectInfo["requires"] = objectInfo.get('requires', []) + [requiresInfo[-1]]

    # Adds each required service
    for service in requiresInfo:
      # Ignore any request that is marked 'if-needed'
      if service.get('priority') == 'if-needed':
        # Handle when a service is marked 'if-needed' and the client requests
        # it. However, it is unclear when that would even make sense to do.
        # (A client requesting a service is much like a child process
        # requesting a service... so it makes sense)
        if service.get('name') in (services or []):
          service['priority'] = 'required'
        else:
          continue

      required = service.get('name')
      environment  = objectInfo.get('environment')
      architecture = objectInfo.get('architecture')
      ManifestManager.Log.noisy(f"Fulfilling service requirement '{required}' for {objectInfo.get('name')} [{environment}/{architecture}]")

      knownServices = self.services.retrieve(environment, architecture, required)
      serviceObject = None
      for knownService in knownServices:
        serviceObject = self.objects.retrieve(id = knownService.internal_object_id,
                                              revision = knownService.revision,
                                              person = person)
        if serviceObject:
          serviceObjectInfo = self.objects.infoFor(serviceObject)
          ManifestManager.Log.noisy(f"Fulfilling service requirement '{required}' with {serviceObjectInfo.get('type')} {serviceObjectInfo.get('name')}")

          serviceObjectInfo['id'] = serviceObject.id
          serviceObjectInfo['uid'] = serviceObject.uid
          serviceObjectInfo['revision'] = serviceObject.revision

          serviceInfo = serviceObjectInfo.get('services', {}).get(required, {})
          ManifestManager.deepMerge(serviceObjectInfo, serviceInfo)

          # Adds a process to the current 'running' listing
          processInfo = self.addProcessToTask(currentInput, serviceObject, serviceObjectInfo, interactive=interactive, context=context, locks = locks, person=person)
          objectIndex = context['objectIndex']

          # Add the init section of the subprocess to the main process
          break
      if not knownServices or not serviceObject:
        # Cannot resolve a required service...
        # We then have to punt that request to the Provider/Backend
        unresolved.append(service)

    return unresolved

  def reduceDependencyInitSection(self, dependencyInfo):
    """ This will return a revised init section based on the one provided by a dependency.

    Dependencies do not need their entire init sections contained within the task manifest.
    This function will truncate the init section to only what is needed by the deployment
    backend.
    """

    # We will start from an empty dictionary
    ret = {}

    # Keep the 'link' section so the deployment backend knows to mount/copy directories
    # from objects into the root filesystem
    links = dependencyInfo.get('init', {}).get('link', [])
    if links:
      ret["link"] = links

    copies = dependencyInfo.get('init', {}).get('copy', [])
    if copies:
      ret["copy"] = copies

    env = dependencyInfo.get('init', {}).get('env', {})
    if env:
      ret["env"] = env

    # Return the minimized init section
    return ret

  def reduceDependency(self, process, dependencyCache, dependencyList, dependencyIndex):
    """ Shrinks the dependency information.
    """

    # Get the dependency list
    dependency = dependencyList[dependencyIndex]
    dependencies = dependency.get('dependencies', [])

    # Append all run-time dependencies
    buildTimeDependencies = dependency.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Do we add ourselves to the initial list?
    token = f"{dependency.get('id')}@{dependency.get('revision')}"
    if token in dependencyCache:
      # Revise with lock/inject when necessary
      # Locks add an extra constraint
      if 'lock' in dependency or 'inject' in dependency:
        dependencyOrder = dependencyCache[dependency.get('id')]
        for item in dependencyOrder:
          index = item['index']
          if 'lock' in dependency:
            process['dependencies'][index]['lock'] = dependency['lock']
          if 'inject' in dependency:
            process['dependencies'][index]['inject'] = dependency['inject']
    else:
      # Add our actual dependency
      fullDependency = copy.deepcopy(dependency)
      process['dependencies'].append(fullDependency)
      dependencyCache[token] = fullDependency

      # Order dependencies within list correctly (by revision, as found,
      # and then by version, ascending.)

      # This list is stored via the id and it points to the list of versions
      # or revisions and the indices they are currently located.

      # When a new dependency is added, it will be added where it should go and
      # the rest will be propagated down. The new dependency starts at the end
      # of the list and will bubble up.

      # This will sort the dependency list in-place.
      dependencyCache[dependency.get('id')] = dependencyCache.get(dependency.get('id'), [])
      dependencyOrder = dependencyCache[dependency.get('id')]
      listItem = {}
      if 'version' in dependency:
        listItem['version'] = dependency.get('version')
        if 'subversion' in dependency:
          listItem['subversion'] = dependency.get('subversion')
      else:
        listItem['revision'] = dependency.get('revision')

      listItem['index'] = len(process['dependencies']) - 1
      dependencyOrder.append(listItem)

      # Bubble up this listItem
      index = len(dependencyOrder) - 2
      for currentItem in reversed(dependencyOrder[:-1]):
        # Done if we hit the revision items
        if 'revision' in listItem and 'revision' in currentItem:
          break
        elif 'version' in listItem and 'version' in currentItem:
          # Done if the current item is a lesser version than
          # the new item
          if Semver.versionMatches(currentItem['version'], '<', listItem['version']):
            break

        # Swap
        dependencyOrder[index] = listItem
        dependencyOrder[index+1] = currentItem

        tmp = process['dependencies'][listItem['index']]
        process['dependencies'][listItem['index']] = process['dependencies'][currentItem['index']]
        process['dependencies'][currentItem['index']] = tmp

        tmp = listItem['index']
        listItem['index'] = currentItem['index']
        currentItem['index'] = tmp

        # Look at the next entry
        index = index - 1

    # Reduce ourselves
    dependency = {
      'id': dependency.get('id'),
      'revision': dependency.get('revision'),
      'version': dependency.get('version'),
      'subversion': dependency.get('subversion'),
      'requested': dependency.get('requested'),
      'replace': dependency.get('replace'),
      'lock': dependency.get('lock'),
      'init': dependency.get('init', {}),
    }

    if dependency['replace'] is None:
      del dependency['replace']

    if dependency['requested'] is None:
      del dependency['requested']

    if dependency['lock'] is None:
      del dependency['lock']

    if dependency['version'] is None:
      del dependency['version']

    if dependency['subversion'] is None:
      del dependency['subversion']

    # Add back the dependency list
    if dependencies:
      dependency['dependencies'] = dependencies

    # Reduce each sub-dependency
    for i in range(0, len(dependencies)):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Combine ENV section
    dependency['init'] = dependency.get('init', {})
    dependency['init']['env'] = dependency['init'].get('env', {})
    for i in range(0, len(dependencies)):
      self.combineEnvSection(dependency['init']['env'], dependencies[i].get('init', {}).get('env'))
      if 'env' in dependencies[i].get('init', {}):
        del dependencies[i]['init']['env']
    if len(dependency['init']['env']) == 0:
      del dependency['init']['env']

    # Update ourselves
    dependencyList[dependencyIndex] = dependency

  def reduceProcess(self, process):
    """ Shrinks down the process metadata to only what is necessary.
    """

    # We only need full information in the initial set of dependencies.
    dependencyCache = {}

    # All unique dependencies in subsections go into the main dependency list
    # for the process.
    dependencies = process.get('dependencies', [])
    process['dependencies'] = []

    # Append all run-time dependencies
    buildTimeDependencies = process.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Go through and reduce each dependency
    for i, dependency in enumerate(dependencies):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Retrieve the new list
    dependencies = process.get('dependencies', [])

    # The prior loop will aggregate all dependencies into our list
    # Reduce, now, each fully formed dependency
    for dependency in dependencies:
      # Get rid of the run-time dependencies of the sub-dependencies
      # And any other build information fluff
      if 'build' in dependency:
        build = {}
        for key in ['id', 'revision']:
          if key in dependency['build']:
            build[key] = dependency['build'][key]
        dependency['build'] = build

      # Reduce 'init' section
      if 'init' in dependency:
        dependency['init'] = self.reduceDependencyInitSection(dependency)

      # Get rid of any unnecessary keys
      keys = ["init", "build", "run", "id", "uid", "revision", "version",
              "name", "type", "dependencies", "network", "file", "metadata",
              "summary", "inject", "lock", "requested", "install", "stage",
              "filter", "inputs", "requires", "replace", "mount"]
      for key in list(dependency.keys()):
        if key not in keys:
          del dependency[key]

    # Append it back
    if dependencies:
      process['dependencies'] = dependencies

  def addProcessToTask(self, currentEnvironment, object, objectInfo, isBuilding=False, buildTask=None, interactive=False, context={}, locks={}, person=None):
    """ Adds the process given by object to the current 'running' array.

    Side effects include the updating of any manifest context.

    Args:
      object (Object): The object being added.
      objectInfo (dict): The object metadata being added.
      interactive (Boolean): Whether or not we are running the object interactively.
      context (dict): Contains context metadata for VM generation.
      person (Person): The current Person that is authenticated.

    Returns:
      dict: The added process manifest.
    """

    currentEnvironment['running'] = currentEnvironment.get('running', [])
    currentRunning = {}
    currentRunning['objects'] = []
    currentEnvironment['running'].append(currentRunning)

    # inputInfo will be the copy of the object description that gets placed in the task
    # It will have only the minimal information the deployment backend needs to deploy the task
    inputInfo = {
      "id":           objectInfo.get('id'),
      "uid":          objectInfo.get('uid'),
      "name":         objectInfo.get('name'),
      "type":         objectInfo.get('type'),
      "revision":     objectInfo.get('revision'),
      "environment":  objectInfo.get('environment'),
      "architecture": objectInfo.get('architecture'),
      "dependencies": []
    }

    # Capture the network effects
    if not isBuilding and 'network' in objectInfo.get('run', {}):
      inputInfo['network'] = objectInfo['run']['network']

    for key in ['run', 'build', 'init', 'provides', 'install', 'basepath', 'mount',
                'owner', 'file', 'outputs', 'inputs', 'network', 'lock', 'requires',
                'requested', 'metadata', 'summary', 'stage', 'filter', 'replace']:
      if key in objectInfo:
        inputInfo[key] = objectInfo[key]

    if interactive:
      inputInfo['interactive'] = True

    # Mark the index in this object
    objectIndex = context['objectIndex']
    while objectIndex in context['reservedIndices']:
      objectIndex += 1
    inputInfo['index'] = objectIndex
    objectIndex += 1
    context['objectIndex'] = objectIndex

    # For every dependency of this environment, add it to the previous
    # environment

    # Resolving the dependencies
    resolved = self.resolveDependenciesFor(object, objectInfo, context, locks, person)

    # We bail if we cannot resolve the dependencies
    if resolved is not True:
      raise DependencyUnresolvedError(resolved, "Cannot find dependency")

    # Shrink the process information down now that we have resolved everything
    self.reduceProcess(objectInfo)

    # Form the running object's init section from dependencies
    initSection = {}

    dependenciesIndex = context['dependenciesIndex']
    dependencies = objectInfo.get('dependencies')

    #for dependency in dependencies:
    #  print(dependency.get('name'), dependency.get('version'), dependency.get('revision'), [x.get('version') for x in dependenciesIndex[dependency['id']]['resolved']], dependency.get('version') == dependenciesIndex[dependency['id']]['resolved'][0].get('version'), dependency.get('version') in [x.get('version') for x in dependenciesIndex[dependency['id']]['resolved']]),

    dependencies = list(filter(lambda dependency:
      dependency.get('version') in [x.get('version') for x in dependenciesIndex[dependency['id']]['resolved']],
      #dependency.get('version') == dependenciesIndex[dependency['id']]['resolved'][0].get('version'),
      dependencies)
    )

    for dependency in dependencies:
      #print("ADDING", dependency.get('name'), dependency.get('revision'))
      dependencyInfo = {
        "id":       dependency.get('id'),
        "uid":      dependency.get('uid'),
        "name":     dependency.get('name'),
        "type":     dependency.get('type'),
        "revision": dependency.get('revision'),
      }

      # For direct dependencies, we copy certain metadata to the task
      for key in ['init', 'inject', 'version', 'subversion', 'build', 'owner',
                  'file', 'network', 'paths', 'lock', 'summary', 'install',
                  'provides', 'environment', 'architecture', 'requested',
                  'filter', 'requires', 'replace', 'mount']:
        if key in dependency:
          dependencyInfo[key] = dependency[key]

      # Add this object to the context of the running process
      objectIndex = context['objectIndex']
      while objectIndex in context['reservedIndices']:
        objectIndex += 1
      dependencyInfo['index'] = objectIndex
      objectIndex += 1
      context['objectIndex'] = objectIndex

      # Keep track of the dependencies in the current object
      inputInfo['dependencies'].append(dependencyInfo.copy())

      # Combine init section with main section, using the currentEnvironment
      self.parseInitSection(dependency, initSection, currentEnvironment)

      # Adds this object to the VM
      currentRunning['objects'].append(dependencyInfo)

    # Add prior processes' init sections
    processes = currentEnvironment["running"]
    for process in processes[:-1]:
      processInfo = process['objects'][-1]
      self.parseInitSection(processInfo, initSection, currentEnvironment)

    # Parse the {{ tags }} within the object
    self.parseTagsInTask(inputInfo)

    if 'dependencies' in inputInfo and len(inputInfo['dependencies']) == 0:
      del inputInfo['dependencies']

    # Add our own init section
    self.parseInitSection(inputInfo, initSection, currentEnvironment)
    self.completeInitSection(initSection, currentEnvironment)
    if 'run' in inputInfo:
      inputInfo['run'].update(initSection)

    currentRunning['objects'].append(inputInfo)

    return inputInfo

  def initializeTask(self, section, environmentGoal, architectureGoal, backendName, objectInfo, generator):
    """ Creates the basic task template for a new task manifest.

    Arguments:
      section (str): The phase being generated (run, build, etc)
      environmentGoal (str): The target environment of the task.
      architectureGoal (str): The target architecture for the task.
      backendName (str): The name of the backend for which this task will be deployed, if any.
      objectInfo (dict): The object metadata being used.
      generator (Object): The generator of this task, if necessary. None if not.

    Returns:
      dict: The initial and mostly empty task manifest.
    """

    # Create blank task
    from uuid import uuid1
    task = {
      "type": "task",
      "environment":  environmentGoal  or 'native',
      "architecture": architectureGoal or 'native'
    }

    # Preserve the task's name
    if backendName:
      task["backend"] = backendName
      task["name"] = f"Virtual Machine to {section} {objectInfo.get('name')} with {backendName}"
    else:
      task["name"] = f"Virtual Machine to {section} {objectInfo.get('name')} on {environmentGoal}/{architectureGoal}"

    # Keep track of the source of the task (workflow, etc)
    if generator:
      task['generator'] = self.objects.infoFor(generator)
      task['generator']['revision'] = generator.revision
      task['generator']['id']  = generator.id
      task['generator']['uid'] = generator.uid

    return task

  def prepareObject(self, object, section, person):
    """ Generates the initial boilerplate task for the given object.

    Arguments:
      object (Object): The object for which the task will be generated.
      section (str): The type of task (build or run).
      person (Person): The actor generating this task.

    Returns:
      dict: The simplified object metadata that serves as the basis of the task.
    """

    # Get the actual object metadata
    objectInfo = self.objects.infoFor(object)

    # Make a copy of the object metadata
    objectInfo = copy.deepcopy(objectInfo)

    # Ensure the task contains the object's id, uid, and revision
    objectInfo['id'] = object.id
    objectInfo['uid'] = object.uid
    objectInfo['revision'] = object.revision

    # Append 'owner' information. The owner revision is the same.
    ownerObj = self.objects.ownerFor(object, person=person)
    if ownerObj.id != object.id:
      objectInfo['owner'] = objectInfo.get('owner', {
        "id": ownerObj.id,
        "uid": ownerObj.uid
      })

    # Retain information about the local stage being used
    if not object.link is None:
      objectInfo['stage'] = { 'id': object.link }

    # Put build dependencies in with normal dependencies
    if section == "build":
      # We inherit 'init' sections from within build sections
      objectInfo['init'] = objectInfo['build'].get('init', {"dependencies": []})

    # Now, we override the main 'init' with the provided 'init', if it exists
    if section in objectInfo and 'init' in objectInfo[section]:
      objectInfo['init'] = objectInfo[section].get('init')

    # Ensure the object we are running/building is using the section asked for.
    objectInfo[section] = objectInfo.get(section, {})

    # Combine dependencies
    generalDependencies = objectInfo.get('dependencies', [])
    if not isinstance(objectInfo.get('init', {}), dict):
      raise MalformedInitError("Object metadata contains a malformed 'init' section.")

    # Reform the dependencies to a single list
    objectInfo['dependencies'] = generalDependencies + objectInfo.get('init', {}).get('dependencies', []) + objectInfo[section].get('dependencies', [])

    # Capture any other metadata.
    if 'metadata' in objectInfo[section]:
      objectInfo['metadata'] = objectInfo[section]['metadata']

    # Capture the network effects
    if 'network' in objectInfo[section]:
      objectInfo['network'] = objectInfo[section]['network']

    # Capture what file is selected to run
    if 'file' in objectInfo[section]:
      objectInfo['file'] = objectInfo[section]['file']

    # We, then, substitute the section requested with the 'run' section.
    if section != "run":
      objectInfo['run'] = objectInfo[section]
      del objectInfo[section]

    # Allow environment/architecture to bubble up from the requested section
    objectInfo['environment'] = objectInfo['run'].get('environment', objectInfo.get('environment'))
    objectInfo['architecture'] = objectInfo['run'].get('architecture', objectInfo.get('architecture'))

    return objectInfo

  def addInputsToTask(self, task, objectInfo, inputs, context, locks, person):
    """ Adds any forces inputs to an existing partial task.
    """

    # Retrieve the object index and reserved indices
    objectIndex = context.get('objectIndex', 0)
    reservedIndices = context.get('reservedIndices', [])

    # Ready the list of inputs
    objectInfo['inputs'] = objectInfo.get('inputs', [])

    # Make sure the inputs are a list
    if not isinstance(objectInfo['inputs'], list):
      objectInfo['inputs'] = [objectInfo['inputs']]

    # For each input requested
    for wireIndex, wire in enumerate(inputs):
      for input in wire:
        # Gather information about the input
        inputInfo = input
        if not isinstance(input, dict):
          inputInfo = self.objects.infoFor(input)
          inputInfo['id']       = input.id
          inputInfo['uid']      = input.uid
          inputInfo['revision'] = input.revision

          if not input.link is None:
            # Retain information about the local stage being used
            inputInfo['stage'] = { 'id': input.link }

          if input.file:
            inputInfo['file'] = input.file

          if input.buildId is not None:
            inputInfo['build']['id'] = input.buildId

        # Give the input a relative identifier
        while objectIndex in reservedIndices:
          objectIndex += 1
        inputInfo['index'] = objectIndex
        objectIndex += 1
        context['objectIndex'] = objectIndex

        resolved = self.resolveDependenciesFor(input, inputInfo, context, locks, person)

        # We bail if we cannot resolve the dependencies
        if resolved is not True:
          raise DependencyUnresolvedError(resolved, "Cannot find dependency")

        objectInfo['inputs'].extend([{}] * (wireIndex + 1 - len(objectInfo['inputs'])))
        objectInfo['inputs'][wireIndex]['connections'] = objectInfo['inputs'][wireIndex].get('connections', [])
        objectInfo['inputs'][wireIndex]['connections'].append({
          "id":       inputInfo.get('id'),
          "uid":      inputInfo.get('uid'),
          "type":     inputInfo.get('type'),
          "name":     inputInfo.get('name'),
          "revision": inputInfo.get('revision'),
          "index":    inputInfo.get('index'),
        })

        objectInfo['dependencies'] = objectInfo.get('dependencies', [])
        objectInfo['dependencies'].extend(inputInfo.get('dependencies', []))

        for key in ['stage', 'version', 'subversion', 'buildId', 'owner', 'file',
                    'network', 'dependencies', 'run', 'init', 'build', 'metadata',
                    'filter', 'requires', 'inputs', 'mount']:
          if key in inputInfo:
            objectInfo['inputs'][wireIndex]['connections'][-1][key] = inputInfo[key]

        if 'filter' in objectInfo['inputs'][wireIndex]:
          objectInfo['inputs'][wireIndex]['connections'][-1]['filter'] = objectInfo['inputs'][wireIndex]['filter']

  def updateIndexes(self, task, index):
    """ Updates the indexes in the task by adding the given value to each.

    Object indexes exist in tasks to assign a unique value to every instance of
    an object in the task. They are used to create file system environments for
    objects within the VM, such as directories on the local physical machine
    running the VM.

    Arguments:
      task (dict): The partial task manifest.
      index (int): The amount to add to every object index.
    """

    # Augment the index if we see it within the base object
    if 'index' in task:
      task['index'] += index

    # Augment the indexes of every dependency
    for dependency in task.get('dependencies', []):
      if 'index' in dependency:
        dependency['index'] += index

    # Augment the indexes of every input
    for wire in task.get('inputs', []):
      for input in wire.get('connections', []):
        input['index'] += index

    # Recursively call this for every process running within this sub-task.
    for running in task.get('running', []):
      for process in running.get('objects', []):
        self.updateIndexes(process, index)

  def contextFromTask(self, task):
    """ Returns the context block for an existing task.

    The context block is used to keep track of information about the generation
    of a task. It helps dependency version resolution by keeping track of what
    revisions were chosen for dependencies and what dependencies already exist
    in the task.

    When partial tasks are pulled from the cache, this context is not itself
    stored. It must be re-generated from the task outline. This function
    performs this function.

    Arguments:
      task (dict): The partial task manifest.

    Returns:
      dict: The context dictionary that contains generation context.
    """

    # Start with the basic context
    context = {'objectIndex': 0,
               'penalties': {},
               'dependencies': {},
               'dependenciesIndex': {},
               'reservedIndices': []}

    # Reference the dependencies index.
    # Our goal is to fill this with a hash of all known dependencies using data
    # from the existing task manifest.
    dependenciesIndex = context['dependenciesIndex']

    # Go through and build the context from the main object of the task.
    objectInfo = task.get('runs', task.get('builds'))

    # Go through every dependency
    # Tasks are generated such that the dependencies and sub-dependencies all
    # appear in the same list, as opposed to a tree. That is, all dependencies
    # of object A also place their sub-dependencies under A, and so on, in the
    # final task.
    for dependency in objectInfo.get('dependencies', []):
      if 'id' in dependency:
        # We want to know the largest object index so new objects are allocated
        # one higher as they are added to the partial task.
        if dependency.get('index'):
          context['objectIndex'] = max(context['objectIndex'], dependency['index'])

        # If the dependency has not yet been recorded, we will add it to the
        # ongoing record. Duplicate dependencies are not likely in a task
        # manifest. Even if they exist, they should have been resolved to the
        # same revision with their resulting metadata being cloned, so they are
        # safely ignored.
        if dependency['id'] not in dependenciesIndex:
          # Track a new dependency
          dependenciesIndex[dependency['id']] = {'version': []}

          dependencyInfo = dependenciesIndex[dependency['id']]
          dependencyInfo['resolved'] = [dependency]

          # We keep track of every requested version requirement as a list.
          # These are logically AND'd together when resolved. Any new
          # version requirements for objects augmented to this partial list
          # will have to also satisfy the existing resolved dependencies.
          if 'version' in dependency and dependency.get('replace') != 'never':
            dependencyVersion = dependency['version']
            for subVersion in dependencyVersion.split(','):
              if subVersion not in dependencyInfo['version']:
                dependencyInfo['version'].append(subVersion)

    # Go through every input. (We can ignore outputs... those are never part of
    # a partial task.) We only care to see the object index assigned to them.
    for wire in objectInfo.get('inputs', []):
      for input in wire.get('connections', []):
        context['objectIndex'] = max(context['objectIndex'], input['index'])

    # Ensure the index is the next available
    context['objectIndex'] = context.get('objectIndex', -1) + 1

    # Return the generated context
    return context

  def retrievePartialTaskFor(self, object, section="run", buildId=None, person=None):
    """ Retrieves the existing cached task for the given object and criteria.

    The task records were previously created or discovered and associated with
    the given object. They are identified by a combination of the given
    `section` and `buildId` fields along with the object id and its owner's
    public key (identity URI.)

    This also falls back to the provided `person` identity in case the given
    actor has generated their own task for the object.

    This assumes that the task records that are queryable have already been
    validated via their signatures. It makes no attempt to verify signatures
    upon retrieval.

    The value returned is the task metadata and is meant to be exactly aligned
    with the expected data returned from a call to the `generatePartialTaskFor`
    function. If no such task was stored or discovered on the local instance,
    this function returns None.

    Staged object tasks are never cached. When an `object` is passed with a
    `link` tag, this function will always return None. Retrieving a task for a
    staged object would be done through the LinkManager since such a task would
    be stored alongside other staged metadata.

    Arguments:
      object (Object): The object for which to retrieve a task.
      section (str): The type of task (run or build).
      buildTask (dict): The build task metadata to use.

    Returns:
      Object: The task object or None if such a task was not found locally.
      str: The build task identifier, if used in the partial task or None.
      TaskRecord: The task record which has signature information.
    """

    # Staged object tasks are never cached
    if object.link:
      return None, None, None

    # Presume and return None when the task is not found
    task = None

    # Negotiate the fallback identity from the provided person
    identity = None
    if person:
      identity = person.identity

    # Query for a prior task, preferably signed by the object owner
    taskRecord = None
    taskRecord = self.datastore.retrieveTask(id = object.id,
                                             revision = object.revision,
                                             section = section,
                                             buildId = buildId,
                                             identity = object.identity,
                                             secondaryIdentity = identity)

    # Pull out the task metadata and return it
    if taskRecord:
      task = self.objects.retrieve(id = taskRecord.task_id,
                                   revision = taskRecord.task_revision)

      buildId = taskRecord.build_id

    # Return the task (will be None if not found and overwritten)
    return task, buildId, taskRecord

  def pullPartialTaskFor(self, object, taskInfo, section="run", buildId=None):
    """ Stores a discovered task for the given object and criteria.

    The taskInfo must specify an existing object that represents the partial
    task manifest.
    """

    # Get a published time (when the task was created)

    id = object.id
    uid = object.uid
    revision = object.revision
    taskId = taskInfo.get('id')
    taskRevision = taskInfo.get('revision')
    identity = taskInfo.get('identity')
    published = taskInfo.get('published')
    published = Datetime.from8601(published)

    signatureInfo = taskInfo.get('signature', {})
    key = signatureInfo.get('key')
    signed = signatureInfo.get('signed')
    signed = Datetime.from8601(signed)
    signature = b''
    if signatureInfo.get('encoding') == "base64":
      signature = base64.b64decode(signatureInfo.get('data', '').encode('utf-8'))

    signature_type = signatureInfo.get('format') or "PKCS1_v1_5"
    signature_digest = signatureInfo.get('digest') or "SHA512"

    # Create the task record and store its signature
    return self.datastore.createTask(id = object.id,
                                     revision = object.revision,
                                     taskId = taskId,
                                     taskRevision = taskRevision,
                                     section = section,
                                     buildId = buildId,
                                     identity = identity,
                                     verifyKeyId = key,
                                     published = published,
                                     signed = signed,
                                     signature = signature,
                                     signature_type = signature_type,
                                     signature_digest = signature_digest)

  def writePartialTaskFor(self, object, task, person, section="run", buildId=None):
    """ Stores the generated task for the given object and criteria.

    This creates and stores the task as a normal object as the actor provided
    in the `person` field. The resulting record of the task is stored along with
    a signature also associated with that actor. This signature will be verified
    when the object given in `object` is discovered and pulled on another system
    along with this new task object.

    The `buildId` helps identify run tasks associated with particular builds of
    the provided object. It does not apply when writing a task for building said
    object (when the section is 'build'.) In that case, buildId should be None.

    Arguments:
      object (Object): The object for which the given task is associated.
      task (dict): The generated task metadata.
      person (Person): The actor generating this task.
      section (str): The type of task (run or build).
      buildId (str): The build task id used for this task.

    Returns:
      Object: The newly formed task as an Object.
    """

    # The buildId value is only useful with 'run' tasks
    if section == "build" and buildId:
      raise ValueError("Cannot write a build task associated with a build id.")

    # Create the proper task object
    taskObject = self.objects.write.create(name        = task.get('name'),
                                           object_type = "task",
                                           subtype     = section,
                                           info        = task)

    # Store the task object in the object repository
    self.objects.write.store(taskObject, person.identity)

    # Get a published time (when the task was created)
    import datetime
    published = datetime.datetime.now()

    # Sign the newly formed task object
    signInfo = self.keys.write.signTask(object, uri = person.identity,
                                                task = taskObject,
                                                section = section,
                                                buildId = buildId,
                                                published = published)

    # Unpack signature data
    signature_digest, signature_type, signature, signed, key = signInfo

    # Create the task record and store its signature
    self.datastore.createTask(id = object.id,
                              revision = object.revision,
                              taskId = taskObject.id,
                              taskRevision = taskObject.revision,
                              section = section,
                              buildId = buildId,
                              identity = person.identity,
                              verifyKeyId = key,
                              published = published,
                              signed = signed,
                              signature = signature,
                              signature_type = signature_type,
                              signature_digest = signature_digest)

    # Return the created task object
    return taskObject

  def generatePartialTaskFor(self, object, section="run", buildTask=None, person=None):
    """ Generates the partial task for the given object.

    Ignores any existing cached version of the partial task.

    When `BuildTask` is provided, that task metadata supplies which build is
    desired to be used to run the object. It does not apply when a build task
    is being generated (when section is 'build'.)

    Arguments:
      object (Object): The object for which to generate a task.
      section (str): The type of task to generate (run or build).
      buildTask (dict): The task metadata used to previously build the object.
      person (Person): The actor generating the task manifest.

    Returns:
      dict: The generated task manifest.
    """

    # The locks dict forces a particular version string for a dependency.
    locks = {}

    # The penalties dict contains the known failed versions of dependencies as
    # it resolves the dependency chains. This is to keep track of when
    # dependencies of dependencies clash.
    penalties = {}

    # Sanitize the object metadata returning a copy of it
    objectInfo = self.prepareObject(object, section = section,
                                            person = person)

    if objectInfo is None:
      return None

    # Generate a new task with the basics filled out
    task = self.initializeTask(environmentGoal = objectInfo.get('environment'),
                               architectureGoal = objectInfo.get('architecture'),
                               section = section,
                               backendName = None,
                               objectInfo = objectInfo,
                               generator = None)

    # Penalize resolving this object as its own dependency
    # (Unless it is locked in, which overrides this behavior)
    resolvedRevision = None
    if locks and objectInfo['id'] in locks:
      resolvedRevision, resolvedVersion = self.versions.resolve(object, version = locks[objectInfo['id']],
                                                                        penalties = penalties)

    # If the requested revision is not the locked revision (if it exists),
    # add it to the penalties list, if we are building.
    if object.revision != resolvedRevision and section == "build":
      penalties[objectInfo['id']] = penalties.get(objectInfo['id'], []) + [object.revision]

    # Determine if the object is interactive or not
    interactive = objectInfo.get('run', {}).get('interactive', False)
    if interactive:
      task['interactive'] = interactive

    # Keep track of all dependencies within this environment
    # We will throw away duplicates
    dependenciesIndex = {}

    # This contains all the state gathered while generating the task
    objectIndex = 0
    context = {'objectIndex': objectIndex, 'penalties': penalties, 'dependencies': {}, 'dependenciesIndex': dependenciesIndex, 'reservedIndices': []}

    # Determine the build task, if not provided and necessary
    isBuilding = section == 'build'
    if not isBuilding and not buildTask and self.isBuildable(object) and not objectInfo.get('build', {}).get('id') and not objectInfo.get('stage'):
      # Get a built copy
      try:
        buildTask = self.retrieveBuildTaskFor(object, preferred = None,
                                                      person = person)
      except BuildRequiredError as e:
        buildTask = None

    # Store the build task information into the object metadata as retrieved or
    # as provided by the `buildTask` argument.
    if buildTask:
      objectInfo['build'] = objectInfo.get('build', {})
      objectInfo['build']['id'] = buildTask.get('id')
      objectInfo['build']['revision'] = buildTask.get('revision')
      objectInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

    # Minimize the object metadata and add it to the partial task
    # This will resolve dependencies and fully realize the object within the task
    objectInfo = self.addProcessToTask(task, object, objectInfo, isBuilding = isBuilding,
                                                                 buildTask = buildTask,
                                                                 interactive = interactive,
                                                                 context = context,
                                                                 locks = locks,
                                                                 person = person)

    # Set the task reference, which quickly shows the object that is being run
    # It is either 'runs' or 'builds'
    taskReference = "runs"
    if section == "build":
      # The task tag is 'builds'
      taskReference = "builds"
    task[taskReference] = objectInfo

    # Return the generated task
    return task

  def partialTaskFor(self, object, section="run", running=None, inputs=None, services=None,
                                   arguments=None, buildTask=None, person=None,
                                   ignoreServices=False, force=False):
    """ Generates or retrieves the cached partial task for the given object.

    It is possible that such a task is cached. In that case, it will retrieve
    and return that existing partial task.

    This function does not resolve tags or mount paths. The main taskFor
    function will do this, instead, in order to preserve more complicated tasks
    in the future from an existing partial task.

    The `buildTask`, when given, should also include a `id` and `revision` keys
    that are filled with the appropriate id and revision for the stored task
    object. Partial tasks cannot be stored if a full build task is not also
    stored and accessible by the invoking actor.

    Arguments:
      object (Object): The object for which to generate a task.
      section (str): The type of task to generate (run or build).
      running (dict): The manifest for the subtask this task is invoking.
      inputs (list): A set of wires depicting objects connected to this object.
      services (list): A list of strings indicating desired services to run within the task.
      arguments (list): A list of arguments to append to the indicated object's command.
      buildTask (dict): The task metadata used to previously build the object.
      person (Person): The actor generating the task manifest.
      ignoreServices (bool): When True, the task will not resolve requested services.
      force (bool): When True, it will not search for a cached partial task.

    Returns:
      dict: The partial task manifest.
    """

    # When we are building the object, it might depend on itself. That's handled
    # here by penalizing the current revision during dependency resolution.
    taskReference = "runs"
    if section == "build":
      # The task tag is 'builds'
      taskReference = "builds"

    # Negotiate the build task id
    buildId = None
    if buildTask:
      # Duplicate the build task... it will be modified
      buildTask = copy.deepcopy(buildTask)
      buildId = buildTask.get('id')

    # Pull the cached task for the particular object and section/buildId
    taskFromCache = True
    taskObject = None
    if not force:
      taskObject, buildId, _ = self.retrievePartialTaskFor(object, section = section,
                                                                   buildId = buildId,
                                                                   person = person)

    # If there is no such task, generate one
    if taskObject is None:
      taskFromCache = False
      task = self.generatePartialTaskFor(object, section = section,
                                                 buildTask = buildTask,
                                                 person = person)
    else:
      task = self.objects.infoFor(taskObject)

    # Pull out the build task being used (if we are running the object)
    if section == "run":
      buildId = task.get('runs', {}).get('build', {}).get('id')

    # Conserve this partial task (if not already from the cache)
    if not taskFromCache and not object.link:
      self.writePartialTaskFor(object, task, person, section = section,
                                                     buildId = buildId)

    # Get the context from the partial task
    context = self.contextFromTask(task)

    # Get the base object information
    objectInfo = task[taskReference]

    # Find the referred object
    for process in task.get('running', []):
      for object in process.get('objects', []):
        if object.get('index') == objectInfo.get('index'):
          objectInfo = object
          break;

    # Add services
    if not ignoreServices:
      self.resolveServicesFor(objectInfo, section = section,
                                          services = services,
                                          context = context)

    # Add forced inputs to that object
    if inputs is not None and isinstance(inputs, list):
      ManifestManager.Log.write("Adding inputs to task")
      locks = {}
      self.addInputsToTask(task, objectInfo, inputs, context, locks, person)

    # Remove 'inputs' fields that are not needed
    for input in objectInfo.get('inputs', []):
      if 'filter' in input:
        del input['filter']

    # Update the command to run
    if arguments and 'run' in objectInfo:
      objectInfo['run']['command'] = objectInfo['run'].get('command', [])
      if not isinstance(objectInfo['run']['command'], list):
        objectInfo['run']['command'] = [objectInfo['run']['command']]
      objectInfo['run']['command'].extend(arguments)

    # Update the reference object
    task[taskReference] = objectInfo

    # Add the running partial task to this task
    # It also overwrites and inherits the reference object
    if running:
      providerObject = task.get('runs')

      # This means we augment the object indexes in the partial task by the
      # highest number in the partial task.
      self.updateIndexes(running, context['objectIndex'] + 1)

      # Add that updated partial task to the larger task
      task['running'][0]['objects'][0]['running'] = running['running']

      # Update the task name
      task['name'] = running['name']

      # Inherit the intention of the running sub-task
      # and destroy the intent of the current task.
      innerSection = "run"
      if 'runs' in running:
        if 'builds' in task:
          del task['builds']
        task['runs'] = running['runs']

      if 'builds' in running:
        innerSection = "build"
        if 'runs' in task:
          del task['runs']
        task['builds'] = running['builds']

      # Combine the init with the provider init
      runningObject = running.get('runs', running.get('builds'))
      if 'init' in runningObject:
        self.completeInitSection(runningObject['init'], providerObject)

      # Check for services that are yet-to-be-resolved in case the provider can
      # provide them for the object.
      unresolved = runningObject.get('requires', []) + runningObject.get(section, {}).get('requires', [])

      # For each service of the running object... attempt to resolve the service
      # using the provider.
      for service in unresolved:
        required = service.get('name')
        environment = runningObject.get('environment')
        architecture = runningObject.get('architecture')

        # We ignore any service that is marked 'if-needed' since that priority
        # would have been updated if it is seen to actually to have been needed.
        # That effect happens below when negotiating unresolved services of
        # inner objects of the provider.
        if service.get('priority') == 'if-needed':
          continue

        # We only have to resolve the service if it has not been resolved yet
        # So, skip to the next service if there is already a filled out 'using' section
        if service.get('using') is not None:
          continue

        ManifestManager.Log.noisy(f"Fulfilling service requirement '{required}' for {runningObject.get('name')} [{environment}/{architecture}] via provider {providerObject.get('type')} {providerObject.get('name')}")

        # Check the provider for fulfillment
        # The provider can fulfill a service with a 'if-needed' requirement priority.
        # An 'if-needed' requirement effectively punts the service to the higher level...
        # That is, if the Provider is offered the service, the running components will also
        # have it. Often via a pass-through or filesystem-level manner.
        serviceResolved = False
        for providerRequired in providerObject.get('requires', []):
          if providerRequired.get('name') == required:
            if providerRequired.get('priority') == 'if-needed':
              # Update the service requirement here to match the needs of the object
              providerRequired['priority'] = service.get('priority', 'required')
              ManifestManager.Log.noisy(f"Fulfilled service requirement '{required}' via upstream provider.")
              service['using'] = {
                  "name": providerObject.get('name'),
                  "type": providerObject.get('type'),
                  "summary": providerRequired.get('summary', "Service is provided by the containing environment.")
              }
              serviceResolved = True

        if not serviceResolved:
          raise Exception(f"Cannot resolve service '{required}'")

      # Preserve the task's name
      task["name"] = f"Virtual Machine to {innerSection}"
      task["name"] += f" {running[innerSection + 's'].get('name')}"
      task["name"] += f" on {task['environment']}/{task['architecture']}"

    # Return the resolved task
    return task

  def taskFor(self, object, section="run", running=None, inputs=None,
                            services=None, generator=None,
                            environment=None, architecture=None,
                            backend=None, ignoreServices=False, force=False,
                            arguments=None, buildTask=None, person=None):
    """ Generates a full task for the given object and the given conditions.

    Arguments:
      object (Object): The resolved object to create a task for. Can be a dict of metadata.
      section (str): The phase to generate a task for. Defaults to "run".
      running (dict): An existing task that will be running within the environment of this task.
      inputs (list): A list of inputs to give to this task.
      services (list): A list of strings indicating desired services to run within the task.
      generator (Object): The resolved object that has created this task.
      environment (str): The environment in which to run this task.
      architecture (str): The architecture on which to run this task.
      backend (str): The backend to specifically target for the task.
      ignoreServices (bool): When True, the task will not resolve requested services.
      force (bool): When True, a new partial task will be generated instead of the cached partial task.
      arguments (list): A list of arguments to append to the indicated object's command.
      buildTask (dict): The build task used to build the provided object, if running the object.
      person (Object): The "person" creating this task.

    Returns:
      dict: The complete task manifest or None if it could not be generated.
    """

    # Generate or retrieve the partial task of the desired object
    subTask = self.partialTaskFor(object, section = section,
                                          running = running,
                                          inputs = inputs,
                                          buildTask = buildTask,
                                          arguments = arguments,
                                          services = services,
                                          person = person,
                                          force = force)

    # If there was a problem, we bail
    if not subTask:
      return None

    # Get a backend path to the indicated environment and architecture from the object
    if subTask.get('environment') == environment and subTask.get('architecture') == architecture:
      backendPath = []
    else:
      backendPath = self.backends.providerPath(environment = subTask.get('environment'),
                                               architecture = subTask.get('architecture'),
                                               environmentGoal = environment,
                                               architectureGoal = architecture,
                                               backend = backend,
                                               person = person)

    if backendPath is None:
      # Cannot find a provider path
      raise ProviderRequiredError(f"No suitable way to {section} this object has been found on {environment} on {architecture}", environment, architecture)

    nativeBackend = None
    if len(backendPath) > 0:
      nativeBackend = backendPath[-1]
      if isinstance(nativeBackend, Object):
        nativeBackend = None

    for backendObject in reversed(backendPath):
      # Ignore the native backend, if it pops up
      if isinstance(backendObject, Object):
        subTask = self.partialTaskFor(backendObject, section = "run",
                                                     running = subTask,
                                                     person = person)

    # Finish up. The last known subTask is the root task
    task = subTask

    # Add the generator, if it exists
    if generator:
      generatorInfo = self.objects.infoFor(generator)
      task['generator'] = task.get('generator', [])
      task['generator'].append({
        'id': generator.id,
        'uid': generator.uid,
        'revision': generator.revision,
        'name': generatorInfo.get('name', 'Unknown'),
        'type': generatorInfo.get('type', 'object')
      })

    # Mark the task interactive if the object is marked interactive
    if task.get(section + 's', {}).get(section, {}).get('interactive', False):
      task['interactive'] = True

    # Establish the 'volume' and 'mounted' paths for every object so they
    # know where they, and their inputs, exist in their environments
    basepath = None
    if nativeBackend:
      # TODO: have the provider path yield the exact env/arch chosen
      # Re-negotiate the native environment/architecture pair for the task
      environment = environment or nativeBackend.provides()[0][0]
      architecture = architecture or nativeBackend.provides()[0][1]

      # Determine the basepath for the native layer for path determination
      task['backend'] = nativeBackend.__class__.name()
      backend = nativeBackend.__class__.name()
      basepath = {
        "id":           "backend",
        "environment":  environment,
        "architecture": architecture,
        "basepath":     nativeBackend.rootBasepath()
      }
    else:
      # The task itself provides the environment
      task['provides'] = {
        "environment": environment,
        "architecture": architecture
      }

    # Fill in the mount paths for each object in the task
    self.setVolumesInTask(task, rootBasepath = [], basepath = basepath)

    # Finalize the tags
    self.parseTagsInTask(task, removeUnknown = True)

    # The task cannot provide an environment... it *is* an environment.
    if 'provides' in task:
      del task['provides']

    # The task itself should not have any known pathing.
    if 'paths' in task:
      del task['paths']

    # Finally, the backend needs to resolve any pending services yet unresolved.
    if nativeBackend:
      # Look at the running task and ask the backend how to resolve the tasks.
      running = task['running'][0]['objects'][0]

      # Ignore services that are marked 'if-needed' since they are not needed.
      # If they were, that priority would have been updated to reflect that.
      if running.get('requires'):
        running['requires'] = list(filter(lambda x: x.get('priority') != 'if-needed',
                                          running.get('requires', [])))

      for service in running.get('requires', []):
        required = service.get('name', 'unknown')

        resolved = self.backends.resolveServiceFor(backend, service)

        if resolved is False:
          raise Exception(f"Cannot resolve service '{required}' using {backend}")

        resolved['backend'] = backend
        service['using'] = resolved

    # Return the formed task.
    return task

  def backendsFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run"):
    """ Returns a list of backends that can run the given object.
    """

    info = object
    if isinstance(object, Object):
      info = self.objects.infoFor(object)

    targetEnvironment  = info.get('environment')  or info.get(section, {}).get('environment')
    targetArchitecture = info.get('architecture') or info.get(section, {}).get('architecture')

    return self.backends.backendsFor(environment=targetEnvironment, architecture=targetArchitecture)

  def setVolumesInTask(self, task, rootBasepath=None, basepath=None, rootMountPathIndex=None, mountPathIndex=None):
    """ This method determines and completes the pathing metadata for a task.

    Arguments:
      task (dict): The task metadata.
      rootBasepath (dict): The path information for the initial VM environment.
      basepath (dict): The path information for the current sub-environment.
      rootMountPathIndex (list): Keeps track of allocated names for the root environment.
      mountPathIndex (list): Keeps track of allocated names for the sub-environment.
    """

    # Treat the incoming task object as the generic object
    objInfo = task

    # Start at the root of the virtual machine and go through recursively
    # to any leaves.
    mountId = objInfo.get('id')
    if 'owner' in objInfo:
      mountId = objInfo['owner'].get('id', objInfo.get('id'))

    # Keep track of which mount paths are used when the root basepath contains a list.
    if rootMountPathIndex is None:
      rootMountPathIndex = [0]

    # Keep track of which mount paths are used when the basepath contains a list.
    if mountPathIndex is None:
      mountPathIndex = [0]

    # Set the volume and mounted for this entry
    if rootBasepath is not None and basepath is not None:
      rootMountPaths  = {}
      localMountPaths = {}
      for subBasepath in rootBasepath + [basepath]:
        rootMountPath = subBasepath.get('basepath', {}).get('mount', '/')
        if not isinstance(rootMountPath, str) and isinstance(rootMountPath, list):
          index = rootMountPathIndex[0]
          if index < len(rootMountPath):
            rootMountPath = rootMountPath[index]
            rootMountPathIndex[0] += 1
          else:
            # TODO: cannot create this task... place errors somewhere
            rootMountPath = "/"

        baseArchitecture = subBasepath.get('architecture', "unknown")
        baseEnvironment  = subBasepath.get('environment',  "unknown")

        rootMountPaths[baseArchitecture] = rootMountPaths.get(baseArchitecture, {})
        rootMountPaths[baseArchitecture][baseEnvironment] = rootMountPath

        localMountPath = subBasepath.get('basepath', {}).get('local', '/home/occam/local')

        localMountPaths[baseArchitecture] = localMountPaths.get(baseArchitecture, {})
        localMountPaths[baseArchitecture][baseEnvironment] = localMountPath

      mountPath = basepath.get('basepath', {}).get('mount', '/')
      if not isinstance(mountPath, str) and isinstance(mountPath, list):
        index = mountPathIndex[0]
        if index < len(mountPath):
          mountPath = mountPath[index]
          mountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          mountPath = "/"

      objInfo['paths'] = {
        "volume": rootMountPaths,
        "mount":  mountPath,
        "separator": basepath.get('basepath', {}).get('separator', '/'),

        "local":  localMountPaths,
        "localMount": basepath.get('basepath', {}).get('local', '/home/occam/local'),

        "taskLocal":  basepath.get('basepath', {}).get('taskLocal', '/home/occam/task'),

        "cwd": basepath.get('basepath', {}).get('cwd', basepath.get('basepath', {}).get('local', '/home/occam/task'))
      }

    if 'provides' in objInfo:
      # Keep track of the basepath information in a stack
      if rootBasepath is None:
        rootBasepath = []

      # Push the latest basepath to the stack (each VM layer has one)
      if basepath is not None:
        rootBasepath = rootBasepath[:]
        rootBasepath.append(basepath)

      # Determine the new basepath structure to apply
      # This will be passed recursively to all objects with this new providing layer
      basepath = {
        "id": objInfo.get('id'),
        "basepath": objInfo.get('basepath', (basepath or {}).get('basepath', {})),
        "architecture": objInfo.get('provides', {}).get('architecture'),
        "environment":  objInfo.get('provides', {}).get('environment'),
      }

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('inputs', [])
    if isinstance(inputs, list):
      for wire in inputs:
        for input in wire.get('connections', []):
          self.setVolumesInTask(input, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Do the outputs, too
    outputs = objInfo.get('outputs', [])
    if isinstance(outputs, list):
      for wire in outputs:
        for output in wire.get('connections', []):
          self.setVolumesInTask(output, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Dependencies
    for dep in objInfo.get('dependencies', []):
      self.setVolumesInTask(dep, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Go through the running sub-tasks in this environment
    for subTask in objInfo.get('running', []):
      for inputObject in subTask.get('objects', []):
        self.setVolumesInTask(inputObject, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Update cwd
    if 'run' in objInfo and 'cwd' in objInfo['run']:
      objInfo['paths']['cwd'] = objInfo['run']['cwd']

    # Transplant the 'script' to 'command'
    # TODO: This generally needs to be done after tags are parsed...
    if 'run' in objInfo and 'script' in objInfo['run'] and 'command' not in objInfo['run']:
      objInfo['run']['command'] = objInfo['run']['script']

    if 'run' in objInfo and 'command' in objInfo['run']:
      if not isinstance(objInfo['run']['command'], list):
        objInfo['run']['command'] = [objInfo['run']['command']]

  def updateTag(self, value, originalKey, root):
    # Determine the root of the key
    # Determine the relative key
    # Update the tag with relative keys

    if originalKey == "":
      return value

    # For each tag, reflect the relative key
    try:
      index = 0
      start = value.index("{{", index)

      while start >= 0:
        end   = value.index("}}", start)
        key   = value[start+2:end].strip()

        if key == "{{":
          # Escaped curly brace section
          replacement = "{{"
        else:
          try:
            path = self.parser.path(originalKey)

            # Determine the root
            newRoot = root
            current = root
            keyPrefix = ""
            currentPrefix = ""
            for subKey in path:
              current = current[subKey]
              if isinstance(subKey, int):
                currentPrefix = currentPrefix + "[" + str(subKey) + "]"
              else:
                currentPrefix = currentPrefix + "." + subKey

              if isinstance(current, dict) and "id" in current:
                newRoot = current
                keyPrefix = currentPrefix

            if keyPrefix.startswith("."):
              keyPrefix = keyPrefix[1:]

            okey = key
            if keyPrefix:
              key = keyPrefix + "." + key

            replacement = "{{ " + key + " }}"
          except:
            replacement = "{{ %s }}" % (key)

        if replacement is None:
          replacement = ""

        replacement = str(replacement)

        try:
          value = value[0:start] + replacement + value[end+2:]
        except:
          value = value

        # Adjust where we look next
        index = start + len(replacement)

        # Pull the index of the next {{ section
        # If there isn't one, it will fire an exception
        start = value.index("{{", index)

    except ValueError as e:
      pass

    if value is None:
      value = ""
    return value

  def parseTagsInTask(self, value, root=None, parent=None, parentKey=None, environment=None, removeUnknown=False):
    """ This method goes through every entry in the task info and replaces curly
    brace tags with what they request.

    These replacements are completed to create the task manifest for creating
    a virtual machine. Each tag is based on the root of any object metadata.
    The root of an object is any section with an 'id' tag. The following shows
    how the changes made above are actually the same regardless of where the
    object is inserted into the task manifest as a whole.

    Examples:

      A section such as this::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample {{ paths.mount }}",
          "bar": "{{ paths.mount }}/usr/lib"
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
          "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
        }

      And, in a more complex example where some items are self referencing from their parent::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample {{ paths.mount }}",
              "bar": "{{ paths.mount }}/usr/lib"
            }
          ]
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
              "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
            }
          ]
        }

      As a special case, ``{{ {{ }}`` will escape double curly brackets,
      although double right curly brackets don't require it::

        {
          "foo": "./sample 'foo bar {{{{}}baz}}'"
        }

      Creates the task manifest with the curly brace intact::

        {
          "foo": "./sample 'foo bar {{baz}}'"
        }
    """

    if root is None:
      root = value

    if isinstance(value, dict):
      # Go through a chunk of metadata

      newRoot = root
      newParent = parent
      if 'id' in value:
        newRoot = value
        newParent = root

      # Handle complex objects first
      for k, v in value.items():
        if isinstance(v, dict):
          self.parseTagsInTask(v, root = newRoot,
                                  parent = newParent,
                                  parentKey = k,
                                  removeUnknown = removeUnknown)

      # Then lists
      for k, v in value.items():
        if isinstance(v, list):
          self.parseTagsInTask(v, root = newRoot,
                                  parent = newParent,
                                  parentKey = k,
                                  removeUnknown = removeUnknown)

      # Then strings
      for k, v in value.items():
        if isinstance(v, str):
          value[k] = self.parseTagsInTask(v, root = newRoot,
                                             parent = newParent,
                                             parentKey = parentKey,
                                             removeUnknown = removeUnknown)

    elif isinstance(value, list):
      # Go through a list of items

      i = 0
      for v in value:
        value[i] = self.parseTagsInTask(v, root = root,
                                           parent = parent,
                                           parentKey = parentKey,
                                           removeUnknown = removeUnknown)
        if value[i] == "" and v != "":
          # Delete empty strings in lists
          del value[i]
        else:
          i += 1

    elif isinstance(value, str):
      # This is a string, so we parse it for curly brace sections
      try:
        index = 0
        start = value.index("{{", index)

        while start >= 0:
          end   = value.index("}}", start)
          key   = value[start+2:end].strip()

          commands = []
          replaced = True
          replacement = None

          if key == "{{":
            # Escaped curly brace section
            replacement = "{{"
          else:
            originalKey = key
            key, *commands = [x.strip() for x in key.split('|')]
            if key != "" and root is not None:
              try:
                replacement = self.parser.get(root, key, allowExceptions = True)
              except:
                replaced = False
                if removeUnknown:
                  replacement = ""
                else:
                  replacement = "{{ %s }}" % (originalKey)

          if replacement is None:
            replacement = ""

          replacement = str(replacement)

          # Update keys *within* the replacement text
          replacement = self.updateTag(replacement, key, root)

          # If there are no more keys, then we're good to execute the commands
          if "{{" not in replacement:
            for command in commands:
              parts = [x.strip() for x in command.split(" ", 1)]
              func = parts[0]
              args = ""
              if len(parts) > 1:
                args = parts[1]

              import shlex

              # Parse args (splits on ' except when escaped)
              args = shlex.split(args)

              if func == "substring":
                if len(args) == 0:
                  args = [0]
                if len(args) == 1:
                  args.append(len(replacement))
                if len(args) > 2:
                  args = args[0:2]

                args = [int(x) for x in args]

                args[0] = min(args[0], len(replacement))
                args[1] = min(args[1], len(replacement))

                replacement = replacement[args[0]:args[1]]
              elif func == "basename":
                rmext = None
                if len(args) == 1:
                  import re
                  if args[0] == ".*":
                    rmext = re.compile(re.escape(".") + ".*$")
                  else:
                    rmext = re.compile(re.escape(args[0]) + "$")

                sep = root.get('basepath', {}).get('separator', "/")
                old = replacement

                if os.sep != sep:
                  repsep = "\\\\"
                  if os.sep == "/":
                    repsep = "\\/"
                  replacement = replacement.replace(os.sep, repsep).replace(sep, os.sep)

                replacement = os.path.basename(replacement)

                if rmext:
                  replacement = rmext.sub("", replacement)
              elif func == "extname":
                replacement = os.path.splitext(replacement)[1]
              elif func == "replace":
                replacement = replacement.replace(args[0], args[1])
              elif func == "dospath":
                # str -> dospath -> str

                # Converts the posix path to a DOS path
                # Convert '/' separators to '\'
                replacement = replacement.replace("/", "\\")

                # Shorten all directories and filenames when appropriate
                tmppath = replacement.split("\\")
                newpath = []
                for subpath in tmppath:
                  subpath = subpath.replace(" ", "")
                  name, ext = os.path.splitext(subpath)
                  if len(name) > 8 or len(ext) > 4:
                    subpath = name[0:6] + "~1" + ext[0:4]
                  newpath.append(subpath)

                replacement = "\\".join(newpath)

          try:
            value = value[0:start] + replacement + value[end+2:]
          except:
            value = value

          # Adjust where we look next
          index = start
          if not replaced:
            index = start + len(replacement)

          # Pull the index of the next {{ section
          # If there isn't one, it will fire an exception
          start = value.index("{{", index)

      except ValueError as e:
        pass

    if value is None:
      value = ""
    return value

  def rakeTaskObjects(self, objInfo):
    """ Returns an array of every object metadata for the given task metadata.
    """

    ret = []

    for wire in objInfo.get('inputs', []):
      for input in wire.get('connections', []):
        ret.append(input)
        ret += self.rakeTaskObjects(input)

    return ret

class ManifestError(Exception):
  """ Base class for all manifest errors.
  """

  def __init__(self, message):
    self.message = message

  def __str__(self):
    return self.message

class MetadataError(ManifestError):
  """ Error for issues with a malformed `object.json` manifest.
  """

  def __init__(self, message):
    super().__init__(message)

class BuildInformationMissingError(MetadataError):
  """ Error when a `build` section does not exist when needed.
  """

  def __init__(self, message):
    super().__init__(message)

class MalformedInitError(MetadataError):
  """ Error when an `init` section contains an issue.
  """

  def __init__(self, message):
    super().__init__(message)

class ProviderRequiredError(ManifestError):
  """ Error is generated when a provider path cannot be resolved.
  """

  def __init__(self, message, environment, architecture):
    super().__init__(message)
    self.environment = environment
    self.architecture = environment

class BackendRequiredError(ManifestError):
  """ Error is generated when a backend cannot be resolved.
  """

  def __init__(self, message, environment, architecture):
    super().__init__(message)
    self.environment = environment
    self.architecture = environment

class DependencyUnresolvedError(ManifestError):
  """ Error is generated when a dependency cannot be resolved.
  """

  def __init__(self, objectInfo, message):
    super().__init__(message)
    self.objectInfo = objectInfo

class BuildRequiredError(ManifestError):
  """ Error is generated when a build is required.
  """

  def __init__(self, objectInfo, requiredObject, message):
    super().__init__(message)
    self.objectInfo = objectInfo
    self.objectInfo['id'] = requiredObject.id
    self.objectInfo['uid'] = requiredObject.uid
    self.objectInfo['revision'] = requiredObject.revision
    self.objectInfo['version'] = requiredObject.version
    self.requiredObject = requiredObject
