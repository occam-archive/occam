# The Manifests Component

This component manages VM task generation and caching.

## Commands

### `manifests` `run`

### `manifests` `build`

### `manifests` `new`

### `manifests` `view`

### `manifests` `route`

## Philosophy

Software preservation encompasses the ability not just to run a software object,
but also to interact with that object. More traditional methods of preserving
software take a similar approach to museums. That is, objects go in and are
maintained as they are. Nothing can be touched. Nothing can be changed.

This traditional approach is seen in software archival projects that wrap
software and data into isolated virtual machines or containers. Then, these
repositories only truly manage the storage and retrieval of said containers,
which are large and abstruse. That is, it is rather difficult to understand
what is contained within and interact with any particular file or tool wrapped
tightly within. This is that 'do not touch' mentality seeping into what should
be highly interactive and versatile archive platforms.

We find this inappropriate for software objects since these are often tools,
interactive art, and often derivable and/or composable with other artifacts.
It serves the community and the goal of preservation the most if the mechanism
providing the execution of a software object is highly flexible, inspectable,
and, prehaps most importantly, repeatable.

It is the question of repeatability that is the most ironic aspect of
traditional software preservation schemes. An archive platform that simply wraps
software objects within, say, a Docker container inherits issues that may not be
immediately apparent that should be avoided. In this case, the software is
preserved, in a way, but only to the point that Docker, as a piece of software,
is preserved. If the technology that makes it possible to run the wrapped
software itself decays, then you lose all artifacts that had targetted it as a
platform. It resembles the effect of platform decay we see in more conventional
places such as the discontinuance of Adobe (formerly Macromedia) Flash, and it
is, ultimately, a catastrophe waiting to happen. If not dealt with carefully, it
becomes a game of whack-a-mole; constantly preserving yet another platform to
keep another platform up and running in order to support level upon level of
aging software archives.

Therefore, Occam is designed around task manifests. All objects are assumed to
run directly in their intended environment. A DOS application is assumed, in our
system, to be able to run directly within a DOS environment. Of course, such an
environment is not readily available. In this case, Occam finds a 'provider',
which is a software artifact, itself archived the same way as any other
software, that reports it provides such an environment. It repeats this step
recursively until the resulting environment matches one directly available. For
instance, some type of x86-64 Linux system.

The lack of presumption about how to run an object is the novel aspect of the
design. It allows for flexibility by allowing different decisions to be made at
any future moment. If an x86-64 system becomes rare, an emulator for that system
can be imported into the archive that 'provides' what we now think of as our
traditonal environment that itself runs on whatever future systems exist.

At no point are we storing the end result of the process. We continuously
negotiate how to run a software artifact based on what resources we have
available. We do not store Docker containers or VM images. We do not need to do
so. Therefore, we avoid the problems associated with storing the redundant data
associated with duplicating libraries and resources within multiple containers.
Furthermore, we have full information about what is within the eventual VM
and can inspect the software within, add new software or data as inputs to such
software objects, and change parameters on how that software is invoked.

## What Constitutes a Task Manifest

A software object is comprised of, of course, its data: source code, scripts,
data files, etc. It is also accompanied by metadata in the form of its
`object.json` file. This metadata provides details of the artifact's
dependencies, external resources, installation paths, and other descriptive
information. By itself, the object's metadata is not sufficient to run the
software artifact. It needs to have certain knowledge gaps filled. In the end,
a full task manifest is produced. It describes every piece of software that is
to be used to run the given software artifact. That includes dependencies,
emulators, and any inputs or data objects being used.

The Manifests Component has the responsibility of creating such a manifest.
It takes a software object and, from its metadata, resolves the dependencies
(and their dependencies) while building environmental layers as needed to
bridge the artifact to the available resources.

The resulting task contains a complete description of the VM starting from the
native environment. The environment that would be common today, an x86-64 Linux
machine, would generally be the presumed environment. Yet, we often do not want
to run random software artifacts directly on the machine. Often, then, we will
employ some type of virtualization technology. This is itself a unique
environment.

For instance, using the Singularity container engine (similar to Docker), the
Occam system is configured such that it says the native environment is actually
an x86-64 Singularity machine. Now, software has to find a mapping from its
own native environment to the Singularity platform. To do so, it needs a
Singularity container image to act as the base image. This is, itself, realized
as an artifact in the system the 'provides' a normal x86-64 Linux environment
that tells the system it requires an x86-64 Singularity environment.

So from that, let's consider a complicated artifact: the original DOS release
of Doom. This software was intended to run on a DOS environment. Particularly,
one typical around its release in 1993. We cannot run that on our modern compute
clusters. Instead, let's say, we need to run it on a machine configured with
Singularity. (or Docker, or VirtualBox, etc)

To do so, it needs to find a way to map the Doom software artifact to that
Singularity environment. Let's say there is the
[DOSBox](https://www.dosbox.com/) emulator as a software artifact in our system.
This is built to run on an x86-64 Linux machine and provides the x86 DOS
environment required by our original object. Now there is a known path.

The Manifest Component takes that path: the Singularity base environment,
DOSBox, and Doom; and combines it into a full-fledged task manifest. For each
layer, it resolves the dependencies of that object. DOSBox has its own
dependencies: a graphics library, sound library, etc. Doom might have its own.
In then end, the task more or less has the following structure:

```
{
  "name": "Task to run Doom on x86-64/singularity",
  "type": "task",
  "backend": "singularity",
  "running": [
    {
      "objects": [
        {
          "name": "Linux",
          "type": "singularity-environment",
          "architecture": "x86-64",
          "environment": "singularity",
          "provides": {
            "architecture": "x86-64",
            "environment": "linux",
          },
          "file": "occam-base.sif",
          "running": [
            {
              "objects": [
                {
                  "name": "DOSBox",
                  "type": "emulator",
                  "architecture": "x86-64",
                  "environment": "linux",
                  "provides": {
                    "architecture": "x86",
                    "environment": "dos",
                  },
                  "run": {
                    "command": "/usr/bin/dosbox"
                  },
                  "running": [
                    {
                      "objects": [
                        {
                          "name": "Doom",
                          "type": "game",
                          "architecture": "x86",
                          "environment": "dos",
                          "run": {
                            "command": "DOOM.EXE"
                          },
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
```

Of course, the real task has much more information about the dependencies and
some summary and descriptive information omitted for clarity. As you can see,
each layer is nested within another and builds several bridges from certain
history environments to modern ones. At each point, the 'provider' of the
environment has an array of processes in the `running` section that each have a
set of software `objects` that are responsible for providing what is need to run
the software corresponding to that individual process. One of the objects in the
list will have a `run` section with a `command` to invoke that the provider
(both the Singularity base image and DOSBox) will interpret. The provider is
responsible for invoking the software and handling its output and potential
errors.

So, why is this so powerful? Now, Doom has a long and historied modding
community. Folks have been making levels and augmenting the game for its entire
decades long existence. If we have created a container to run Doom, and simply
stored it, we would lose much of the ability to update or interact with the
software and data kept within it. It would be difficult to make difference
decisions on how the container was constructed. It is desirable to be able to
build these containers on-the-fly not just because you might want a different
emulator as technology changes, but also to provide new and interesting inputs
to the software in question.

In this case, we might want to throw a different user-created mod or level to
the game. We would want to attach the level's data (itself an object in the
system) to the VM and augment the command of the game itself to point it to
this data. Doom allows us to load a level by attaching the filename to a
command-line argument. Another thing we could do is augment the Doom object to
notice an input was provided to it and optionally, when it sees this input,
change the arguments it passes to the command. That is, the command invoked
by the Doom software artifact is some kind of batch script. Either way, this is
made possible by the flexibility of generating these tasks on-demand.

You can imagine the simple change we have to make to the full task above. We
simply add the metadata to the manifest as part of the Doom section:

```
{
  ....
        {
          "name": "Doom",
          "type": "game",
          "architecture": "x86",
          "environment": "dos",
          "inputs": [
            {
              "connections": [
                {
                  "name": "My Doom Level",
                  "type": "WAD",
                  "subtype": "Doom",
                  "file": "MYLEVEL.WAD"
                }
              ]
            }
          ]
        }
  ....
}
```

The rest of the task manifest stays the same. It simply, when the task is
deployed, loads the extra content into VM or container and runs the software.
What is important is that there is no complicated process to augment an
existing container. We can throw away all containers and VMs once they are
used.

## Paths and Mounts

Tasks also establish the mount paths for every object within the overall
manifest. Since there is only a single VM that actually runs with individual
software objects, such as emulators, running things as needed, every object is
available and loaded into the VM. That also means that individual objects will
have multiple paths that they are known by depending on the environment in which
they are considered.

For instance, a DOS game might be running within an emulated environment and,
therefore, has a DOS path. Let's say it is `D:\`. That would be its mount path
within the DOS environment. Yet it also has a path within the container or
virtual machine itself. If that environment is a Linux system because that is
the environment the emulator expects, that path may be `/home/occam/objects/0`.

These paths are determined based on the `basepath` given by the provider
environment or backend plugin. Everything is placed in a `paths` key off of the
object metadata within the task manifest. Within this, the `volume` key yields
the path in which the object data is found. Generally the `volume` path is not
writable. It is a read-only reference to the snapshot of the object referenced
by the manifest. The `local` mount, on the contrary, is a writable path that
references the local data for the object. This includes the `inputs` and
`outputs` directories, if possible in that environment and defined by that
environment. For instance, the DOS environment might place the `volume` on a
secondary drive letter that is read-only. The `local` environment might allow
for the inputs and outputs in subdirectories or off of secondary drive letters.

Each of these keys, `volume` and `local`, contain a set of paths that differ by
the environment. They are indexed by the architecture and then the environment.
For instance, a DOS object might yield these paths:

```
"paths": {
  "volume": {
    "javascript": {
      "html": "/"
    },
    "x86": {
      "dos": "D:"
    }
  },
  "mount": "D:",
  "separator": "\\",
  "local": {
    "javascript": {
      "html": "/home/occam/local"
    },
    "x86": {
      "dos": "C:"
    }
  },
  "localMount": "C:",
  "taskLocal": "Z:",
  "cwd": "C:"
}
```

Here, you can find the general `volume` and `local`. Then, for convenience,
the `localMount` gives the `local` path for the intended environment. The
`mount` path is the `volume` path for the intended environment. Finally,
the `taskLocal` is a path that contains the task manifest and other metadata
governing the runtime of the virtual machine, including events and logs.

The `cwd` gives you the intended initial directory that the software runs
within. This is generally the same as the `localMount`. The `separator`
yields the character that serves as a path separator. For this DOS program, the
`separator` is a backslash: (`\`). For a UNIX system, it would be a normal
slash: (`/`).

## Caching, Discovery, and Signing

To facilitate the creation of task manifests, partial tasks that resolve just
one software artifact are generated and stored when such objects are pulled
into the system for the first time or built. Resolving dependencies is a
complex task that requires a lot of forethought into what versions may work
or not work for that artifact. Therefore, the partial tasks serve as a hint for
the system to a known 'good' state. The environment of the virtual machine, at
least around that particular software artifact, will be deterministic with
regard to what supporting artifacts are included. This enforces repeatability
of that artifact even as dependencies change or update around it, but still
offers the flexibility to change that task in the future, when desired, to
include those updates.

When an object is discovered, we also need to pull into our system the set of
dependencies required to run that software. Without a known task, it would not
be clear what should be pulled. The task, therefore, serves as guidance when
discovering new software on what other supporting software or data must also
be present in order to invoke it.

This means that repeatability in invocation of software is preserved through
the task even if the backend (Docker, Singularity) or even the use of
emulation is employed. The is regardless of the fact such tehcnologies were
used or even existed at the time the task is generated.

This the task designates the resolution of dependencies, there is some issue
of trust. If a server gives you a task that is complete nonsense, the object
will no longer run. Therefore, the tasks are objects that are signed by an actor
that can convey trust. Generally, the author of the object is a good authority
on how to run that object, so such a signature from that actor is preferred.

The signing works by using a key from that actor to digitally sign a token
referencing the task and its association with the object. It is signed outside
the scope of other signatures that mark the built binaries or version tags for
the object. These signatures are generally only verified on discovery. That is,
when the object is pulled from one server to another.
