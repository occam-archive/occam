# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("tasks")
class TaskRecord:
  schema = {

    # Foreign keys

    # The object id that the task represents
    "id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    # The task object id
    "task_id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    # The identifier of the build being used (also a task)
    "build_id": {
      "foreign": {
        "table": "objects",
        "key": "id"
      }
    },

    # Attributes

    # The revision of the object that was built
    "revision": {
      "type": "string",
      "length": 128
    },

    # The task revision
    "task_revision": {
      "type": "string",
      "length": 128
    },

    # The type of task (build or run)
    "section": {
      "type": "string",
      "length": 5
    },

    # A timestamp for its creation (can be whatever, honestly)
    "published": {
      "type": "datetime",
    },
    
    # The signing Person
    "identity_uri": {
      "type": "string",
      "length": 256
    },

    # The verify key
    "verify_key_id": {
      "type": "string",
      "length": 256
    },

    # A timestamp for the signature
    "signed": {
      "type": "datetime",
    },

    # A signature for this build
    "signature": {
      "type": "binary",
      "length": 512,
    },

    # The signature algorithm
    "signature_type": {
      "type": "string",
      "length": 16,
      "default": ""
    },

    # The hash digest for the token message
    "signature_digest": {
      "type": "string",
      "length": 10,
      "default": ""
    },
  }
