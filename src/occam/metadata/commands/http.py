# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.log    import Log
from occam.object import Object
from occam.semver import Semver

from occam.commands.manager import command, option, argument

from occam.objects.manager  import ObjectManager
from occam.metadata.manager import MetadataManager

from occam.manager import uses

@command('metadata', 'http',
  category      = 'Metadata Management',
  documentation = "Responds to an HTTP request.")
@argument("format", type=str)
@argument("method", type=str)
@argument("path", type=str)
@option("-q", "--query",  action = "append",
                          dest   = "query",
                          nargs  = 2,
                          help   = "a query parameter of the form key followed by value")
@option("-d", "--header", action = "append",
                          dest   = "headers",
                          nargs  = 2,
                          help   = "adds a header string")
@uses(ObjectManager)
@uses(MetadataManager)
class MetadataHTTPCommand:
  def do(self):
    # Build up headers dict with upper case keys
    headers = {}
    for item in (self.options.headers or []):
      headers[item[0].upper()] = item[1]

    # Build query dict
    query = {}
    for item in (self.options.query or []):
      query[item[0]] = item[1]

    response = self.metadata.http(self.options.format, method  = self.options.method,
                                                       path    = self.options.path,
                                                       query   = query,
                                                       headers = headers,
                                                       data    = None,
                                                       person  = self.person)

    Log.output(str(response[0]), end="")
    Log.output("\n", end="")
    Log.output(json.dumps(response[1]), end="")
    Log.output("\n", end="")
    Log.pipe(response[2])
    return 0
