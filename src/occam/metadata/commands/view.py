# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object
from occam.semver import Semver

from occam.commands.manager import command, option, argument

from occam.objects.manager  import ObjectManager
from occam.metadata.manager import MetadataManager

from occam.manager import uses

import json

@command('metadata', 'view',
  category      = 'Metadata Management',
  documentation = "Views metadata about an object.")
@argument("format", type=str)
@argument("object", type="object")
@uses(ObjectManager)
@uses(MetadataManager)
class MetadataViewCommand:
  def do(self):
    Log.header("Generating metadata")

    obj = self.objects.resolve(self.options.object,
                               allowNone = True,
                               person = self.person)

    if obj is None:
      Log.error("Cannot find object")
      return -1

    # Get the object info
    objectInfo = self.objects.infoFor(obj)

    response = self.metadata.view(self.options.format, object = obj,
                                                       objectInfo = objectInfo,
                                                       person = self.person)

    Log.output(str(response[0]), end="")
    Log.output("\n", end="")
    Log.pipe(response[1])
    return 0
