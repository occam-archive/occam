# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class KeyParser:
  @staticmethod
  def keySnakeToLowerCamelCase(key):
    """ Converts a snake case key to a lower camel-case key.

    Converts a key of the form `something_like_this` to one in the form of
    `somethingLikeThis`, which is our conventional JSON form.

    Arguments:
      key (str): The key to convert.

    Returns:
      str: The conforming lower camel-case key.
    """

    key = key.replace("_", " ").title().replace(" ", "")
    return key[0:1].lower() + key[1:]

  def splitKeys(self, key):
    keys = re.split(r"([^\\])\.", key)

    new_keys = []
    for i in range(0, len(keys)-1, 2):
      new_keys.append((keys[i] + keys[i+1]).replace('\\', ''))

    new_keys.append(keys[len(keys)-1].replace('\\', ''))

    return new_keys

  def parseKeyParts(self, key):
    import re
    # Parse key (splits on '.' except when escaped)
    return [x.replace("\\.", ".") for x in re.split(r"(?<!\\)\.", key)]

  def parseArrayParts(self, key):
    import re
    # Parse key (splits on '[' except when escaped)
    key, *indices = re.split(r"(?<!\\)\[", key, 1)
    if len(indices) == 0:
      indices = ""
    else:
      indices = indices[0]

    return key, [int(x) for x in re.findall(re.escape("[") + "([^]]+)" + re.escape("]"), "[" + indices)]

  def path(self, key):
    """ Retrieves an array of keys that represent a path from the given key.

    Examples:

      A complex dictionary key::

        "foo.bar[0][1].baz"

      Becomes::

        ["foo", 0, 1, "bar", "baz"]
    """

    parts = self.parseKeyParts(key)

    ret = []

    for i, subKey in enumerate(parts):
      subKey, arrayParts = self.parseArrayParts(subKey)

      ret.append(subKey)
      ret.extend(arrayParts)

    return ret

  def getParent(self, document, key, create = False, asList = False):
    """ Retrieves the containing hash for the given key.
    """

    parts = self.parseKeyParts(key)

    for i, subKey in enumerate(parts):
      subKey, arrayParts = self.parseArrayParts(subKey)

      if i == len(parts) - 1 and len(arrayParts) == 0:
        return document, subKey

      if isinstance(document, list):
        subKey = int(subKey)

        document = document[subKey]
      else:
        # If we need to set a subkey but the parent is not a dict or list, we
        # coerce it to be so
        if arrayParts:
          if create and (not subKey in document or not isinstance(document[subKey], list)):
            document[subKey] = []
        else:
          if create and (not subKey in document or not isinstance(document[subKey], dict)):
            document[subKey] = {}

        document = document[subKey]

      for j, indexKey in enumerate(arrayParts):
        indexKey = int(indexKey)
        if i == len(parts) - 1 and j == len(arrayParts) - 1:
          if not isinstance(document, list) and create:
            parent = []

          return document, indexKey

        document = document[indexKey]

      if document is None:
        return None, None

    return document, None

  def get(self, document, key, default="__none__", allowExceptions=False):
    """ Retrieves the value for the given key.
    """

    try:
      parent, subKey = self.getParent(document, key)
    except Exception as e:
      if default == "__none__":
        raise e
      return default

    if isinstance(parent, list):
      return parent[subKey]

    if default == "__none__" and allowExceptions:
      parent[subKey]

    if not isinstance(parent, dict):
      return default

    return parent.get(subKey, default)

  def append(self, document, key, value, create = False):
    """ Appends the given value to the list via the given key in the given document.

    When create is True, the path through the document will also be created.
    """

    parent, subKey = self.getParent(document, key, create = create)
    if not subKey in parent and create:
      parent[subKey] = []
    parent[subKey].append(value)

    return document

  def set(self, document, key, value, create = False):
    """ Sets the given key to the given value within the given document.

    When create is True, the path through the document will also be created.
    """

    parent, subKey = self.getParent(document, key, create = create)

    if isinstance(subKey, int) and isinstance(parent, list) and subKey >= len(parent) and create:
      # Create enough entries to cover the span (give up after a few tho)
      iterations = 50
      while iterations >= 0 and subKey >= len(parent):
        iterations -= 1
        parent.append(None)

    parent[subKey] = value
    return document

  def delete(self, document, key):
    """ Deletes the given key from the given document.
    """

    parent, subKey = self.getParent(document, key)

    if isinstance(parent, list):
      del parent[int(subKey)]
    elif subKey:
      del parent[subKey]

    return document
