# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.accounts.manager import AccountManager
from occam.commands.manager import command, option, argument
from occam.messages.manager import MessageManager

from occam.manager import uses

@command('accounts', 'sendmail',
         category      = 'Account Management',
         documentation = 'Sends email to the address associated with a given account')
@argument("body",         type = str,
                          help = "The body text of the message to send")
@argument("subject",      type = str,
                          help = "The subject text of the message to send")
@option("-i", "--identity", action = "store", 
                            dest   = "identity",
                            help   = "The identity to message.")
@option("-a", "--all",    action  = "store_true",
                          dest    = "all",
                          default = False,
                          help    = "Sends to all registered email addresses")
@uses(MessageManager)
@uses(AccountManager)
class SendMailCommand:
  """ Sends email with given body and subject to given identity. Optionally
      sends message to all registered addresses in system.
  """
  def do(self):
    # Require administrator role to send messages
    if 'administrator' not in self.person.roles:
      Log.error("Only an administrator can send messages.")
      return -1

    if not (self.options.identity or self.options.all):
      Log.error("No recipients specified.")
      return -1
      
    accounts = []
    # If an identity was specified we want to get the account associated with
    # it.
    if self.options.identity:
      account = self.accounts.retrieveAccount(identity = self.options.identity)
      if account is None:
        Log.error(f"No account for identity {self.options.identity}")
        return -1

      accounts = [account]

    # Send all accounts the message if instructed to do so.
    if self.options.all:
      accounts = self.accounts.retrieveAllAccounts()

    # As messages are sent, be sure to track who all was unable to be reached
    # through any means of contact. For certain events it may be necessary to
    # reach out to these people manually in order to notify them of problems
    # with the system.
    uncontactedAccounts = []
    for account in accounts:
      if not self.messages.sendMessage(self.options.subject,
                                       self.options.body,
                                       account):
        uncontactedAccounts.append(account.username)

    if uncontactedAccounts:
      Log.warning(f"The owners of the following accounts could not be "
                  f"contacted: {uncontactedAccounts}")

    Log.done("Messages sent!")
    return 0
