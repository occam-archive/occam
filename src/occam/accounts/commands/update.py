# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.accounts.manager import AccountManager
from occam.objects.manager  import ObjectManager

from occam.commands.manager import command, option, argument

@command('accounts', 'update',
  category      = 'Account Management',
  documentation = "Updates an existing account on the local system.")
@option("-u", "--username",    dest    = "username",
                               action  = "store",
                               help    = "look up account by username")
@option("-i", "--identity_uri", dest    = "identity_uri",
                                action  = "store",
                                help    = "look up account by identity URI")
@option("-o", "--add-role",    dest    = "roles",
                               action  = "append",
                               default = [],
                               type    = str,
                               help    = "adds the given role to the account (requires administrator role)")
@option("-r", "--remove-role", dest    = "remove_roles",
                               action  = "append",
                               default = [],
                               type    = str,
                               help    = "removes the given role from the account (requires administrator role)")
@option("-s", "--add-subscription",    dest    = "subscriptions",
                                       action  = "append",
                                       default = [],
                                       type    = str,
                                       help    = "adds the given subscription to the account")
@option("-n", "--remove-subscription", dest    = "remove_subscriptions",
                                       action  = "append",
                                       default = [],
                                       type    = str,
                                       help    = "removes the given subscription from the account")
@option("-d", "--update-subscription", dest    = "update_subscriptions",
                                       action  = "append",
                                       default = [],
                                       type    = str,
                                       help    = "updates subscription list to arguments in list passed, if non-empty")
@option("-p", "--person", dest = "person",
                          action = "store",
                          type = "object",
                          help = "updates the Person associated with this account.")
@option("-e", "--email",  dest = "email",
                          action = "store",
                          type = str,
                          help = "adds email address to the account")
@option("--set-email-public", dest    = "email_public",
                              action  = "store_true",
                              default = False,
                              help    = "sets the email address as publically viewable")
@option("--set-email-private", dest    = "email_private",
                               action  = "store_true",
                               default = False,
                               help    = "sets the email address as private")
@uses(ObjectManager)
@uses(AccountManager)
class UpdateCommand:
  def do(self):
    account = None

    if len(self.options.roles) > 0 or len(self.options.remove_roles) > 0:
      # Only administrators can perform this action
      if 'administrator' not in self.person.roles:
        Log.error("Only an administrator can add roles to an account.")
        return -1

    if self.options.username is None and self.options.identity_uri is None:
      Log.error("Must specify username or identity_uri")
      return -1

    if self.options.email_public and self.options.email_private:
      Log.error("Please specify only one of the email privacy flags.")
      return -1

    Log.header("Updating account %s" % (self.options.username or self.options.identity_uri))

    account = self.accounts.retrieveAccount(username = self.options.username,
                                            identity = self.options.identity_uri)

    if account is None and self.options.identity_uri is None:
      Log.error("No account found")
      return -1

    if self.options.person:
      identity = self.options.identity_uri or account.identity_key_id

      # Retrieve person
      person = self.objects.resolve(self.options.person, person = self.person)
      if person is None:
        Log.error("Cannot retrieve the provided Person")
        return -1
      
      if person.identity != identity:
        Log.error("Person must be owned by the updated account")
        return -1

      Log.write(f"Setting Person to {person.id}")
      self.accounts.updatePerson(identity, person)

      Log.done("Successfully updated %s" % (identity))
      return 0

    new_roles = self.accounts.addRoles(account, self.options.roles)
    new_roles = self.accounts.removeRoles(account, self.options.remove_roles)

    new_subscriptions = self.accounts.addSubscriptions(account, self.options.subscriptions)
    new_subscriptions = self.accounts.removeSubscriptions(account, self.options.remove_subscriptions)
    
    if self.options.email:
      # Adds email to record and sends validation message if system is email
      # enabled and if email not yet in account record.
      if not self.accounts.isEmailAddressValid(self.options.email):
        Log.error("Please enter a valid email address.")
        return -1

      if not account.email:
        self.accounts.addEmail(account, self.options.email)
        Log.write(f"Added email address {self.options.email}")

      elif account.email == self.options.email:
        Log.write(f"Email address already recorded as {account.email}")
      
      else:
        self.accounts.addEmail(account, self.options.email)
        Log.write(f"Updated email address to {self.options.email}")
    
    # Update the privacy of the email if prompted by the user.
    emailPublic = bool(account.email_public)
    if self.options.email_public or self.options.email_private:
      emailPublic = self.options.email_public and not self.options.email_private
      self.accounts.setEmailVisibility(account, emailPublic)

    Log.write(f"Email is public: {emailPublic}")

    if self.options.update_subscriptions:
      # Replaces current subscriptions with those passed in options
      new_subscriptions = self.accounts.commitSubscriptions(account, self.options.update_subscriptions)

    Log.write("Current roles: %s" % (', '.join(new_roles)))
    Log.write("Current subscriptions: %s" % (', '.join(new_subscriptions)))

    Log.done("Successfully updated %s" % (account.username))
    return 0
