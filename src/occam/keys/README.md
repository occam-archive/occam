# The Keys Component

This component handles object signing and encryption keys for accounts.

When objects are published or tagged, they can be signed. When you request a
particular version or revision of an object, they can be requested with a
particular signature. The object can then be retrieved only when verified
against the signature that represents that identity.

Servers can also be a signing authority.

Server private keys verify Account private keys.

A SignatureRecord holds the signed content of a particular object revision.

The database records consist of the Identity, which represents an actor within
the system. The Identity contains a public key. There is an IdentityKey which
represents the private key. This could be lost (perhaps purposely) and
recovered by gathering a set of KeyShard records. There is, associated with
the Identity, a SigningKey (private) which has an associated VerifyKey
(public).

The Identity is referred by its URI. Considering Identity is decentralized in
the best of cases, the URI itself is based on something that does not refer
directly to the source (such as a domain). Instead, the URI refers to the
identifying key, which is the private key used to sign the signing keys for
that identity. The URI is the hash of the public key that corresponds to this
private key. The URI itself is signed by the private key upon its creation.

If this private identifying key is lost, then the identity is also lost and
must be abandoned by its owner. This person must then create a new identity
and resign their past work under their new identity and, through some social
mechanism, tell others of this circumstance.
