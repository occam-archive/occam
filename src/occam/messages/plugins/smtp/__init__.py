# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.config import Config
from occam.log import loggable
from occam.manager import uses

from occam.messages.manager import messenger

from occam.network.manager import NetworkManager

@loggable
@uses(NetworkManager)
@messenger("smtp")
class SMTPMessenger:
  def __init__(self, configuration):
    """ Creates an SMTP messenger with the provided configuration.
    """

    # Retain configuration options given to us by the parent component.
    self._configuration = configuration
    self.domain = configuration.get("webUri", "http://localhost:9292")

  def sendAccountConfirmation(self, account):
    """ Sends email verification email.

    Skips verification if the email has already been verified.

    Arguments:
      account (AccountRecord): The account whose email is being confirmed.
    """

    if account.email_verified:
      return

    recipient = account.email
    confirmationUrl = f"people/{account.identity_key_id}/validate-email?token={account.email_token}"

    textBody = (f"Please click the link below to verify your account\n"
                f"{self.domain}/{confirmationUrl}")

    self.sendMessage('Occam Account Verification', textBody, account)

  def sendPasswordReset(self, account):
    """ Sends email with password reset token.

    Arguments:
      account (AccountRecord): The account whose password will be reset
    """

    resetUrl = f"people/{account.identity_key_id}/reset-password/{account.reset_token}"

    textBody = (f"Please click the link below to reset your password for the "
                f"account '{account.username}'\n"
                f"{self.domain}/{resetUrl}")

    self.sendMessage('Occam Update Password', textBody, account)

  def sendMessage(self, subject, body, account):
    """ Sends text email message.

    Arguments:
      subject (str): Subject of the message.
      body (str): Body of the message.
      account (AccountRecord): The account belonging to whom the message should
        be sent.
    Returns:
      bool: True if a message was sent. False if a message was not sent.
    """

    import smtplib
    import ssl

    from email.mime.text      import MIMEText
    from email.mime.multipart import MIMEMultipart

    # Accounts without email addresses set cannot be sent email.
    recipient = account.email
    if recipient is None:
      return False

    debug = self._configuration.get('debug', False)
    sender = self._configuration.get('sender', None)
    smtpService = self._configuration.get('smtpService', None)
    # Per https://kinsta.com/blog/smtp-port/, we want to default to 587.
    port = self._configuration.get('port', 587)

    # It is possible SMTP verification is enabled. If so we need to provide for
    # a way to supply the password for the sender. We already store passwords
    # for Postgres in the config, so "in for a penny" I suppose. Our docs
    # should remind the user to secure their Occam config for this reason.
    password = self._configuration.get('senderPassword')

    if not debug:
      if sender is None:
        SMTPMessenger.Log.write('Could not retrieve sender information')
        return False

      if smtpService is None:
        SMTPMessenger.Log.write('Configuration does not specify SMTP service information.')
        return False

    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['From'] = sender
    message['To'] = recipient

    htmlBody = self.formatHTMLMessage(body)

    plainText = MIMEText(body, 'plain')
    htmlFormat = MIMEText(htmlBody, 'html')
    message.attach(plainText)
    message.attach(htmlFormat)

    if debug:
      SMTPMessenger.Log.write(message.as_string())
      return True

    # SMTP connections seem to come in a few flavors:
    #   * Connections that require SSL from the start.
    #   * Connections that require TLS upgrade via STARTTLS.
    #   * Connections that don't support TLS.
    #
    # We should, at a minimum, discourage anything below TLS 1.2.
    context = self.network.getSSLContext()
    context.options |= ssl.OP_NO_SSLv2
    context.options |= ssl.OP_NO_SSLv3
    context.options |= ssl.OP_NO_TLSv1
    context.options |= ssl.OP_NO_TLSv1_1

    # Try to start with SSL.
    errorMsg = ""
    try:
      with smtplib.SMTP_SSL(smtpService, port, context=context) as server:
        if password is not None:
          server.login(sender, password)
        server.sendmail(sender, recipient, message.as_string())
        return True
    except (ssl.SSLError, smtplib.SMTPException) as e:
      # Record this detail just in case it helps debugging an error in the
      # STARTTLS case.
      errorMsg = (f"Failed to connect to {smtpService}:{port} via SSL: {e}\n")

    # Try STARTTLS.
    try:
      with smtplib.SMTP(smtpService, port) as server:
        server.starttls(context=context)
        if password is not None:
          server.login(sender, password)
        server.sendmail(sender, recipient, message.as_string())
        return True
    except (ssl.SSLError, smtplib.SMTPException) as e:
      errorMsg = f"{errorMsg}Failed to connect to {smtpService}:{port} via STARTTLS: {e}"
      SMTPMessenger.Log.warning(errorMsg)

    SMTPMessenger.Log.error(f"Failed to send message via {smtpService}:{port}.")
    return False

  def formatHTMLMessage(self, message):
    """ Formats message text in html.
    """

    return f"""\
      <html>
        <head></head>
        <body>
          <p>
            {message}
          </p>
        </body>
      </html>
      """
