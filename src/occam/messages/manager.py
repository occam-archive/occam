# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log                    import loggable
from occam.manager                import manager, uses
from occam.config                 import Config

from occam.network.manager       import NetworkManager

@loggable
@uses(NetworkManager)
@manager("messages")
class MessageManager:
  """ This Occam manager sends event-driven email messages to users.
  """

  drivers = {}

  # TODO: Shamelessly stolen from the jobs manager. Deduplicate this behavior
  # into the manager decorator.
  @staticmethod
  def register(adapter, driverClass):
    """ Adds a new messenger driver to the possible drivers.
    """
    MessageManager.drivers[adapter] = driverClass

  # TODO: Shamelessly stolen from the jobs manager. Deduplicate this behavior
  # into the manager decorator.
  def driverFor(self, messenger):
    """ Returns an instance of a driver for the given messenger backend.

    Arguments:
      messenger (String): The name of the messenger.
    """

    if not messenger in MessageManager.drivers:
      # Attempt to find the driver plugin
      import importlib

      try:
        importlib.import_module("occam.messages.plugins.%s" % (messenger))
      except ImportError as e:
        # On error, we return None
        MessageManager.Log.warning(f"Failed to import messenger plugin for {messenger}: {e}")
        return None

      if not messenger in MessageManager.drivers:
        # If the import does not register the driver correctly, we also error out.
        return None

    return MessageManager.drivers[messenger](self.configurationFor(messenger))

  # TODO: Shamelessly stolen from the jobs manager. Deduplicate this behavior
  # into the manager decorator.
  def configurationFor(self, messengers):
    """ Returns the configuration section for the given messenger.

    Returns the configuration for the given messenger that is found within the
    occam configuration (config.yml) under the "messages" section under the
    "messengers" key and then under the given messenger plugin name.
    """

    config = self.configuration
    subConfig = config.get('messengers', {}).get(messengers, {})

    return subConfig

  def __init__(self):
    """ Initializes the manager.
    """

    config = self.configuration
    self.adapters = config.get("activeAdapters", []) or []

  def sendAccountConfirmation(self, account, adapter):
    """ Sends confirmation message to through the given adapter.

    This is generally done either on account creation or when contact info is
    added or replaced.

    Arguments:
      account (AccountRecord): The account whose contact method is being
        confirmed.
    """

    if adapter in self.adapters:
      driver = self.driverFor(adapter)
      if driver:
        driver.sendAccountConfirmation(account)

  def sendPasswordReset(self, account):
    """ Sends a message with the password reset token.

    Arguments:
      account (AccountRecord): The account whose password will be reset
    """

    for adapter in self.adapters:
      driver = self.driverFor(adapter)
      if driver:
        driver.sendPasswordReset(account)

  def sendMessage(self, subject, body, account):
    """ Pushes a notification to the given recipient.
    """

    # TODO: Conditionalize this based on account subscription preferences.
    #
    # Send a message through each active messages adapter.
    #
    # Note that due to the nature of this module it is possible some of these
    # message send attempts will encounter transient errors and others will
    # succeed. Here we choose to indicate if any of the methods managed to make
    # it out to the user. It is up to the message driver to log errors.
    success = False
    for adapter in self.adapters:
      driver = self.driverFor(adapter)
      if driver:
        success = driver.sendMessage(subject, body, account) or success

    return success

def messenger(name: str):
  """ This decorator will register a possible messenger backend.
  """

  def _messenger(cls):
    MessageManager.register(name, cls)
    return cls

  return _messenger
