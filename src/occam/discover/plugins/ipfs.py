# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import atexit
import os

from occam.config import Config

from occam.manager import uses

from occam.discover.manager import network
from occam.network.manager  import NetworkManager

import subprocess

@network('ipfs')
@uses(NetworkManager)
class IPFS:
  """ Implements the logic to make use of IPFS distributed/federated storage.
  """

  daemon = None
  mount = False

  def detect(self):
    """ Returns True when IPFS is available.
    """
    
    try:
      p = self.invokeIPFS([])
      code = p.wait()
    except:
      code = -1

    return code == 0

  @staticmethod
  def path():
    """ Returns the path to the IPFS binary.
    """

    return (os.path.realpath("%s/../../../../../go-ipfs" % (os.path.realpath(__file__))))

  def ipfsMountPath(self):
    return os.path.join(self.storePath(), "ipfs-mount")

  def ipnsMountPath(self):
    return os.path.join(self.storePath(), "ipns-mount")

  def IPFSIsDaemonRunning(self):
    if IPFS.daemon:
      return True

    ipfsPath = self.storePath()
    p = self.invokeIPFS(["log", "ls"], ipfsPath=ipfsPath)
    p.communicate()

    if p.returncode == 0:
      return True

    return False

  def IPFSStartDaemon(self):
    try:
      if IPFS.daemon:
        return IPFS.daemon
    except:
      pass

    ipfsMountPath = self.ipfsMountPath()
    ipnsMountPath = self.ipnsMountPath()

    if self.IPFSIsDaemonRunning():
      return

    if not os.path.exists(ipfsMountPath):
      try:
        os.mkdir(ipfsMountPath)
      except:
        pass

    if not os.path.exists(ipnsMountPath):
      try:
        os.mkdir(ipnsMountPath)
      except:
        pass

    IPFS.Log.noisy("Starting IPFS Daemon (%s, %s)" % (ipfsMountPath, ipnsMountPath))
    command = ["daemon"]
    if IPFS.mount:
      command.extend(["--mount", "--mount-ipfs", ipfsMountPath, "--mount-ipns", ipnsMountPath])

    # Start process
    daemon = self.invokeIPFS(command, ipfsPath=self.storePath())

    # Wait until it says "Daemon is ready"
    readout = ""
    while(True):
      output = daemon.stdout.read(1).decode('utf-8')
      if not output:
        break

      readout += output
      if "Daemon is ready" in readout:
        break

    def cleanup():
      self.IPFSStopDaemon()

    atexit.register(cleanup)

    IPFS.daemon = daemon
    return IPFS.daemon

  def IPFSStopDaemon(self):
    if IPFS.daemon:
      import signal
      try:
        IPFS.daemon.send_signal(signal.SIGINT)
      except:
        pass
      IPFS.daemon = None

  def IPFSNodeHashToSocketAddresses(self, nodeHash):
    """ Yields a set of socket addresses for the given node hash.
    """

    ipfsPath = self.storePath()

    p = self.invokeIPFS(["dht", "findpeer", nodeHash], ipfsPath=ipfsPath)
    multiaddresses = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()

    # Go through and pull out IP
    # They are in the form:
    # /ip4/<ip addr>/tcp/<port>
    ret = []
    for multiaddress in multiaddresses:
      if multiaddress.startswith("/ip4/"):
        # Pull out IP address and port
        splitAddress = multiaddress.split('/')
        ip = splitAddress[2]
        port = splitAddress[4]

        # Ignore loopback and 0 ip addresses.
        if ip != "127.0.0.1" and ip != "0.0.0.0":
          ret.append('%s:%s' % (ip, port))

    return ret

  def storePath(self):
    """ Returns the path to the ipfs instance.
    """

    return self.configuration.get('path', os.path.join(Config.root(), "ipfs"))

  @staticmethod
  def popen(command, stdout=subprocess.DEVNULL, stdin=None, stderr=subprocess.DEVNULL, cwd=None, env=None):
    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def invokeIPFS(self, args, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.DEVNULL, ipfsPath=None, env=None):
    """ Internal method to invoke IPFS with the given arguments
    """

    if env is None:
      env = {}
    if not 'IPFS_PATH' in env and not ipfsPath is None:
      env['IPFS_PATH'] = ipfsPath

    env['PATH'] = os.getenv('PATH')

    command = ['%s/ipfs' % (IPFS.path())] + args
    return IPFS.popen(command, stdout=stdout, stdin=stdin, stderr=stderr, env=env)

  def announce(self, id, token):
    """ Announces a token.
    """

    IPFS.Log.noisy("Pushing token for object %s" % (id))

    # Write an advertisement token
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    result = p.communicate(input=token)

    # Pin the hash maybe
    return id

  def announceClient(self, name, port):
    """ Announces a service.
    """

    IPFS.Log.noisy(f"Announcing client service {name}:{port}")

    # Open a port to the webserver (running on the default port...
    # However, we will want the client to tell the occam daemon what port it is on.
    p = self.invokeIPFS(["p2p", "listen", f"/x/{name}/http", f"/ip4/127.0.0.1/tcp/{port}"], ipfsPath=self.storePath())
    result = p.communicate()

    # Return a 0 exit code
    return 0

  def announceIdentity(self, id, publicKey):
    """ Announces a token.
    """

    # This is technically the same as announcing anything in IPFS
    # Since the identity uris are IPFS hashes.

    IPFS.Log.write("Pushing public key for identity %s" % (id))

    # Write an advertisement
    p = self.invokeIPFS(["block", "put"], stdin=subprocess.PIPE, ipfsPath=self.storePath())
    p.communicate(input=publicKey.encode('utf-8'))

    # Pin the hash maybe
    return id

  def editorIdFor(self, type, subtype):
    """ Returns a discoverable key for the given editor type/subtype.
    """

    token = self.editorTokenFor(type, subtype)

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def editorTokenFor(self, type, subtype):
    """ Returns a discoverable token for the given editor type/subtype.
    """

    return ("occam-editor\ntype=%s\nsubtype=%s\n" % (type, subtype)).encode('utf-8')

  def viewerIdFor(self, type, subtype):
    """ Returns a discoverable key for the given viewer type/subtype.
    """

    token = self.viewerTokenFor(type, subtype)

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def viewerTokenFor(self, type, subtype):
    """ Returns a discoverable token for the given viewer type/subtype.
    """

    return ("occam-viewer\ntype=%s\nsubtype=%s\n" % (type, subtype)).encode('utf-8')

  def providerTokenFor(self, environment, architecture):
    """ Returns a discoverable token for the given provider env/arch.
    """

    return ("occam-provider\nenvironment=%s\narchitecture=%s" % (
      environment,
      architecture
    )).encode('utf-8')

  def providerIdFor(self, environment, architecture):
    token = self.providerTokenFor(environment, architecture)

    # Hash the token

    from occam.storage.plugins.ipfs_vendor.multihash import multihash
    hashedBytes = multihash.encode(token, multihash.SHA2_256)

    from occam.storage.plugins.ipfs_vendor.base58 import b58encode
    return b58encode(bytes(hashedBytes))

  def taskTokenFor(self, id, environment, architecture):
    """
    """

  def retrieveToken(self, id):
    """ Returns the value associated with the given id.
    """

    IPFS.Log.noisy("attempting to retrieve %s" % (id))

    daemon = self.IPFSStartDaemon()

    # TODO: add timeout to configuration
    p = self.invokeIPFS(["block", "get", id, "--timeout", "1s"], ipfsPath=self.storePath())
    ret = p.stdout.read()
    p.communicate()

    return ret

  def search(self, id):
    """ Returns information about the location of an ID on the network.

    Returns a list of metadata containing socket addresses where the given
    object can be found on the network. Also, it yields services.
    """

    IPFS.Log.noisy("attempting to discover %s" % (id))

    daemon = self.IPFSStartDaemon()

    # TODO: add timeout to configuration
    p = self.invokeIPFS(["dht", "findprovs", id, "--timeout", "1s"], ipfsPath=self.storePath())
    hosts = p.stdout.read().decode('utf-8').strip().split("\n")
    p.wait()
    if hosts[0] == "":
      hosts = hosts[1:]

    ret = []
    for peerID in hosts:
      socketAddresses = self.IPFSNodeHashToSocketAddresses(peerID)
      if len(socketAddresses) > 0:
        IPFS.Log.noisy("Found %s at %s" % (id, socketAddresses))

      ret.append({
        "id": peerID,
        "ip": list(map(lambda x: x.split(':')[0], socketAddresses)),
        "ipfs": socketAddresses,
        "web": f"http://127.0.0.1:8080/p2p/{peerID}/x/occam-web-client/http"
      })

    if len(hosts) == 0:
      IPFS.Log.noisy("%s not found" % (id))

    return ret

  def status(self, id):
    """
    """

    # Look up nodes
    addresses = self.search(id)

    # TODO: sort by trust
    for address in addresses:
      # Query each node
      print(address)

    return {}
