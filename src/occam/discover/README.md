# The Discover Component

This component manages mechanisms for finding objects in the world at large.
This gives the discovery capability to any module that needs it.

Discovery is sometimes passed into routines that might require it. For
instance, routines that might lookup subobjects that are linked might
want a discovery agent to use to do that lookup. You can provide this
manager as that agent.

It is possible that down the line we will allow alternative agents or
extensions that will enable different modes of discovery through
services or various archive or publication venues that are outside of
the federation.
