# The Backends Component

This Occam manager handles backend invocations. A backend is a service that
Occam can use to build and run objects. For instance, a backend can wrap a
virtual machine such that objects can be built and run within that
environment. Some objects may be able to run on most backends, some on just
a few, and some backends are decidedly relegated to run older archived
programs.

The [ManifestManager](../manifests/README.md) creates the tasks it wishes to
run and the BackendManager receives those tasks and invokes the backend system
to build or run the objects represented by those tasks. The ManifestManager
creates tasks by using the BackendManager to determine which objects can be
executed and which need to be used together to run something. It is a two step
process:

1. Determine the task (container environment) to run something.
2. Create and run that container (or reuse one that already exists)

The Environment Map is a data structure that normalizes the relationships
among environmental objects. That is, for objects that maintain an environment
for which other objects can be executed (emulators, operating systems, etc,)
the Environment Map contains a mapping between each environment/architecture
and every possible environment/architecture it maps to. It also maps every
known environment/architecture to known backends.

That is, the Environment Map can tell you when given "dos" and "x86" all of
the possible environments ("linux" on "x86-64") that this object can
potentially run upon. Or any backends ("docker", "kvm") that it can run
upon.

The system uses this Environment Map to determine how to best create
virtual machines to run objects:

```
occam manifest run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c
```

This command will use this ManifestManager and BackendManager, making use of
the Environment Map, to generate a possible task that will run the given
object at the given revision:

```
occam manifest run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c --targetEnvironment javascript --targetArchitecture html
```

This will generate a task manifest that will tell a browser component how to
run this object within an HTML/javascript environment. Again, it uses the
Environment Map to quickly know how to map environments such as "dos/x86" to
"javascript/html" and whether or not it is possible. It can make that
decision almost immediately:

```
occam objects run bd4e25a0-5389-11e6-8a01-f23c910a26c8@2ed1d22cb3bd3f6dd40da2765e31a18f161e966c
```

This command will run that object using the task it generates.

To query backends some object can run use the `manifest` command with the
`--backends`/`-b` flag:

```
occam manifest run --environment dos --architecture x86 --backends
```

This will list the backends that are able to run an object originally meant
to be executed in a DOS environment.

See the [Manifest Manager](../manifests/README.md) for more examples and information.
