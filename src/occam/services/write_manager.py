# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log    import loggable
from occam.object import Object
from occam.config import Config

from occam.manager import uses, manager

from occam.services.manager import ServiceManager
from occam.objects.manager  import ObjectManager

@loggable
@manager("services.write", reader=ServiceManager)
@uses(ObjectManager)
class ServiceWriteManager:
  """ This Occam manager organizes objects that can provide particular services.
  """

  def __init__(self):
    """ Initialize the service manager.
    """

  def store(self, id, revision, objectInfo):
    """ Records the given object as a possible service provider.

    We will associate the environment and architecture of the object to the
    provided service name. That way, we can pull out possible objects to
    resolve services for other objects within the same environment.

    Otherwise, we have to rely on backends that provide the given service.

    Args:
      id (str): The object's id.
      revision (str): The object's revision.
      objectInfo (dict): The object's metadata record.
    """

    services = objectInfo.get('services', [])

    environment  = objectInfo.get('environment',
        objectInfo.get('run', {}).get('environment', ''))
    architecture = objectInfo.get('architecture',
        objectInfo.get('run', {}).get('architecture', ''))

    for service in services:
      ServiceWriteManager.Log.write("Discovered service %s." % (service))

    self.datastore.write.updateServices(id = id,
                                        revision = revision,
                                        environment = environment,
                                        architecture = architecture,
                                        services = services,
                                        backend = None)

  def storeBackend(self, backend, service):
    """ Records that the given backend provides the given service.

    Args:
      backend (str): The name of the native backend (if any)
    """

    # Just store it
    self.datastore.write.updateServices(id = None,
                                        revision = None,
                                        environment = None,
                                        architecture = None,
                                        backend = backend,
                                        services = [service])
